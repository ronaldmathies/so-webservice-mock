package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.webservice.mock.fixture.util.WebserviceMockClient;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Delete All")
public class DeleteAll {

    @Start(name="DeleteAll", arguments = {},
            example =
                "!*> DeleteAll all the data in the mock\n" +
                "!|script              |\n" +
                "|start   |Delete All  |\n" +
                "|submit;               |\n" +
                "*!")
    public DeleteAll() {}

    public boolean submit() {
        return WebserviceMockClient.delete("/mock/all");
    }

}
