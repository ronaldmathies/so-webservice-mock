package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.webservice.mock.fixture.util.WebserviceMockClient;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Delete All Responses For JsEndpoint")
public class DeleteAllResponsesForEndpoint {

    private String endpointName = null;

    @Start(name="Delete All Responses For JsEndpoint", arguments = {},
            example =
                "!*> Deletes all responses for an endpoint\n" +
                "!|script                                             |\n" +
                "|start            |Delete All Responses For JsEndpoint |\n" +
                "|setEndpointName; |myservice                         |\n" +
                "|submit;                                             |\n" +
                "*!")
    public DeleteAllResponsesForEndpoint() {}

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public boolean submit() {
        return WebserviceMockClient.delete("/mock/responses/%s", endpointName);
    }

}
