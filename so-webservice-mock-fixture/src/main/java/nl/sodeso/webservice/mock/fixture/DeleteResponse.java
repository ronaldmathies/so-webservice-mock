package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.webservice.mock.fixture.util.WebserviceMockClient;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Delete JsResponseAction")
public class    DeleteResponse {

    private int id = 0;
    private String endpointName = null;

    @Start(name="Delete JsResponseAction", arguments = {},
            example =
                "!*> Delete a specific response for a specific endpoint\n" +
                "!|script                           |\n" +
                "|start            |Delete JsResponseAction |\n" +
                "|setEndpointName; |myservice       |\n" +
                "|setName;           |10              |\n" +
                "|submit;                           |\n" +
                "*!")
    public DeleteResponse() {}

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean submit() {
        return WebserviceMockClient.delete("/mock/response/%s/%s", endpointName, String.valueOf(id));
    }

}
