package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Add JsEndpoint")
public interface AddEndpointSamples {

    @Command(name="Add Basic JsEndpoint", arguments = {},
        example =
            "!*> Add an endpoint\n" +
            "!|script                                 |\n" +
            "|start            |Add JsEndpoint JsEndpoint |\n" +
            "|setEndpointName; |myservice             |\n" +
            "|setPath;         |/service/2016/01/     |\n" +
            "#\n# Add when neccesary additional logging capability here.\n#\n" +
            "|submit;                                 |\n" +
            "*!")
    void addBasicEndpoint();

    @Command(name="Add logging with id based on body using xpath", arguments = {"numberOfRequestsToStore", "xpath"},
        example =
            "|addLoggingWithBodyXpath; |50 |//*[local-name()='element'] |\n")
    void addLoggingWithBodyXpath(int numberOfRequestsToStore, String xpath);

    @Command(name="Add logging with id based on body using a regulair expression", arguments = {"numberOfRequestsToStore", "regexp"},
            example =
                    "|addLoggingWithBodyRegExp; |50 |<reference>(.*?)</reference> |\n")
    void addLoggingWithBodyRegExp(int numberOfRequestsToStore, String regexp);

    @Command(name="Add logging with id based on body using json path", arguments = {"numberOfRequestsToStore", "jsonpath"},
            example =
                    "|addLoggingWithBodyJsonPath; |50 |$.request.reference |\n")
    void addLoggingWithBodyJsonPath(int numberOfRequestsToStore, String jsonpath);

    @Command(name="Add logging with id based on a cookie value", arguments = {"numberOfRequestsToStore", "name"},
            example =
                    "|addLoggingWithCookie; |50 |mycookie |\n")
    void addLoggingWithCookie(int numberOfRequestsToStore, String name);

    @Command(name="Add logging with id based on a cookie value using xpath", arguments = {"numberOfRequestsToStore", "name", "xpath"},
            example =
                    "|addLoggingWithCookieXpath; |50 |mycookie |//*[local-name()='element'] |\n")
    void addLoggingWithCookieXpath(int numberOfRequestsToStore, String name, String xpath);

    @Command(name="Add logging with id based on a cookie value using a regulair expression", arguments = {"numberOfRequestsToStore", "name", "regexp"},
            example =
                    "|addLoggingWithCookieRegExp; |50 |mycookie |<reference>(.*?)</reference> |\n")
    void addLoggingWithCookieRegExp(int numberOfRequestsToStore, String name, String regexp);

    @Command(name="Add logging with id based on a cookie value using jsonpath", arguments = {"numberOfRequestsToStore", "name", "jsonpath"},
            example =
                    "|addLoggingWithCookieJsonPath; |50 |mycookie |$.request.reference |\n")
    void addLoggingWithCookieJsonPath(int numberOfRequestsToStore, String name, String jsonpath);

    @Command(name="Add logging with id based on a header", arguments = {"numberOfRequestsToStore", "name"},
            example =
                    "|addLoggingWithHeader; |50 |myheader |\n")
    void addLoggingWithHeader(int numberOfRequestsToStore, String name);

    @Command(name="Add logging with id based on a header body using xpath", arguments = {"numberOfRequestsToStore", "name", "xpath"},
            example =
                    "|addLoggingWithHeaderXpath; |50 |myheader |//*[local-name()='element'] |\n")
    void addLoggingWithHeaderXpath(int numberOfRequestsToStore, String name, String xpath);

    @Command(name="Add logging with id based on a header body using a regulair expression", arguments = {"numberOfRequestsToStore", "name", "regexp"},
            example =
                    "|addLoggingWithHeaderRegExp; |50 |myheader |<reference>(.*?)</reference> |\n")
    void addLoggingWithHeaderRegExp(int numberOfRequestsToStore, String name, String regexp);

    @Command(name="Add logging with id based on a header body using jsonpath", arguments = {"numberOfRequestsToStore", "name", "jsonpath"},
            example =
                    "|addLoggingWithHeaderJsonPath; |50 |myheader |$.request.reference |\n")
    void addLoggingWithHeaderJsonPath(int numberOfRequestsToStore, String name, String jsonpath);

}
