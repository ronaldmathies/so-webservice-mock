package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Command;
import fitnesse.responders.editing.fixture.Fixture;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Add JsResponseAction")
public interface AddResponseSamples {

    @Command(name="Add Basic Construction: Inline Body", arguments = {},
            example =
                "!*> Add a response.\n" +
                "!|script|\n" +
                "|start                |Add JsResponseAction To Webservice Mock                               |\n" +
                "|setName;               |10                                                            |\n" +
                "|setEndpointName;     |myservice                                                     |\n" +
                "|setDeleteAfterUse;   |false                                                         |\n" +
                "#\n# Choose any of the two methods below to add a response body.\n#\n" +
                "|setMessageUsingFile; |200      |application/xml | body.xml                          |\n" +
                "|setMessage;          |200      |application/xml | <response></response>             |\n" +
                "|addHeader;           |myheader |value                                               |\n" +
                "|addCookie;           |mycookie |value           |sodeso.nl              |/ |1 |3600 |\n" +
                "#\n# Add additional conditions here.\n#\n" +
                "#\n# Add additional actions here.\n#\n" +
                "|submit;                                                                             |\n" +
                "*!")
    void addBasicConstructionWithBody();

}
