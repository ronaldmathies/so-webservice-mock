package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.webservice.mock.fixture.util.WebServiceMockProperties;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Setup Properties")
public class SetupMockProperties {

    @Start(name="Setup Mock Properties", arguments = {},
            example =
                "!*> Set mock properties\n" +
                "!|script                         |\n" +
                "|start    |Setup Mock Properties |\n" +
                "|setHost; |mock.sodeso.nl        |\n" +
                "|setPort; |80                    |\n" +
                "*!")
    public SetupMockProperties() {}

    public boolean setHost(String host) {
        WebServiceMockProperties.getInstance().setHost(host);
        return true;
    }

    public boolean setPort(int port) {
        WebServiceMockProperties.getInstance().setPort(port);
        return true;
    }

}
