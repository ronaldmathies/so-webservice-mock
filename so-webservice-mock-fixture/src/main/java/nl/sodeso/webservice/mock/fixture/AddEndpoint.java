package nl.sodeso.webservice.mock.fixture;

import nl.sodeso.webservice.mock.fixture.util.WebserviceMockClient;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.settings.JsLogging;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsBodyExpression;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsCookieExpression;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsHeaderExpression;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

/**
 * @author Ronald Mathies
 */
public class AddEndpoint {

    private JsEndpoint jsEndpoint = new JsEndpoint();

    public AddEndpoint() {}

    public void setEndpointName(String endpointName) {
        jsEndpoint.setName(endpointName);
    }

    public void setPath(String path) {
        jsEndpoint.setPath(path);
    }

    public void addLoggingWithBodyXpath(int numberOfRequestsToStore, String xpath) {
        addLoggingWithBody(numberOfRequestsToStore, xpath, null, null);
    }

    public void addLoggingWithBodyRegExp(int numberOfRequestsToStore, String regexp) {
        addLoggingWithBody(numberOfRequestsToStore, null, regexp, null);
    }

    public void addLoggingWithBodyJsonPath(int numberOfRequestsToStore, String jsonpath) {
        addLoggingWithBody(numberOfRequestsToStore, null, null, jsonpath);
    }

    private void addLoggingWithBody(int numberOfRequestsToStore, String xpath, String regexp, String jsonpath) {
        JsBodyExpression expression = new JsBodyExpression();
        expression.setXpath(xpath);
        expression.setRegexp(regexp);
        expression.setJsonpath(jsonpath);
        addLogging(numberOfRequestsToStore, expression);
    }

    public void addLoggingWithCookie(int numberOfRequestsToStore, String name) {
        addLoggingWithCookie(numberOfRequestsToStore, name, null, null, null);
    }

    public void addLoggingWithCookieXpath(int numberOfRequestsToStore, String name, String xpath) {
        addLoggingWithCookie(numberOfRequestsToStore, name, xpath, null, null);
    }

    public void addLoggingWithCookieRegExp(int numberOfRequestsToStore, String name, String regexp) {
        addLoggingWithCookie(numberOfRequestsToStore, name, null, regexp, null);
    }

    public void addLoggingWithCookieJsonPath(int numberOfRequestsToStore, String name, String jsonpath) {
        addLoggingWithCookie(numberOfRequestsToStore, name, null, null, jsonpath);
    }

    private void addLoggingWithCookie(int numberOfRequestsToStore, String name, String xpath, String regexp, String jsonpath) {
        JsCookieExpression expression = new JsCookieExpression();
        expression.setName(name);
        expression.setXpath(xpath);
        expression.setRegexp(regexp);
        expression.setJsonpath(jsonpath);
        addLogging(numberOfRequestsToStore, expression);
    }

    public void addLoggingWithHeader(int numberOfRequestsToStore, String name) {
        addLoggingWithHeader(numberOfRequestsToStore, name, null, null, null);
    }

    public void addLoggingWithHeaderXpath(int numberOfRequestsToStore, String name, String xpath) {
        addLoggingWithHeader(numberOfRequestsToStore, name, xpath, null, null);
    }

    public void addLoggingWithHeaderRegExp(int numberOfRequestsToStore, String name, String regexp) {
        addLoggingWithHeader(numberOfRequestsToStore, name, null, regexp, null);
    }

    public void addLoggingWithHeaderJsonPath(int numberOfRequestsToStore, String name, String jsonpath) {
        addLoggingWithHeader(numberOfRequestsToStore, name, null, null, jsonpath);
    }

    private void addLoggingWithHeader(int numberOfRequestsToStore, String name, String xpath, String regexp, String jsonpath) {
        JsHeaderExpression expression = new JsHeaderExpression();
        expression.setName(name);
        expression.setXpath(xpath);
        expression.setRegexp(regexp);
        expression.setJsonpath(jsonpath);
        addLogging(numberOfRequestsToStore, expression);
    }

    private void addLogging(int numberOfRequestsToStore, JsExpression jsExpression) {
        JsLogging jsLogging = new JsLogging();
        jsLogging.setNumberOfRequestsToStore(numberOfRequestsToStore);
        jsLogging.setJsExpression(jsExpression);

        jsEndpoint.setJsLogging(jsLogging);
    }

    public boolean submit() {
        return WebserviceMockClient.post("/mock/endpoint", Entity.entity(jsEndpoint, MediaType.APPLICATION_JSON_TYPE));
    }

}
