package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Command;
import nl.sodeso.commons.fileutils.FileFinder;
import nl.sodeso.fitnesse.CommandException;
import nl.sodeso.webservice.mock.fixture.util.WebserviceMockClient;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.model.generic.JsHeader;
import nl.sodeso.webservice.mock.model.response.JsResponse;
import nl.sodeso.webservice.mock.model.response.condition.conditions.JsBodyCondition;
import nl.sodeso.webservice.mock.model.response.condition.conditions.JsCookieCondition;
import nl.sodeso.webservice.mock.model.response.condition.conditions.JsHeaderCondition;
import nl.sodeso.webservice.mock.model.response.condition.conditions.JsMethodCondition;
import nl.sodeso.webservice.mock.model.response.response.responses.JsMessageResponseAction;
import nl.sodeso.webservice.mock.model.response.response.responses.JsRedirectResponseAction;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author Ronald Mathies
 */
public class AddResponse {

    private String endpointName = null;
    private JsResponse jsResponse = new JsResponse();

    public AddResponse() {
    }

    public void setId(String id) {
        this.jsResponse.setName(id);
    }

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public void setDeleteAfterUse(boolean deleteAfterUse) {
        this.jsResponse.getJsSettings().setDeleteAfterUse(deleteAfterUse);
    }

//    public void setHttpStatus(int httpStatus) {
//        response.getMessageResponse().setHttpStatus(httpStatus);
//    }

//    public void setContentType(String contentType) {
//        response.getMessageResponse().setContentType(contentType);
//    }
//
//    public void setBody(String body) {
//        response.getMessageResponse().setBody(body);
//    }
//
//    public void setBodyUsingFile(String filename) {
//        boolean[] success = {false};
//        FileFinder.findFileInUserPath(filename, found -> {
//            try {
//                response.getMessageResponse().setBody(new String(Files.readAllBytes(found.toPath()), "UTF-8"));
//                success[0] = true;
//            } catch (IOException e) {
//                throw new CommandException("Cannot load file '%s'.", filename);
//            }
//        });
//
//        if (success[0] == false) {
//            throw new CommandException("Cannot find file '%s', please make sure the file exists and is on the classpath.", filename);
//        }
//    }

    public void setMessageUsingFile(int httpStatus, String contentType, String filename) {
        boolean[] success = {false};
        FileFinder.findFileInUserPath(filename, found -> {
            try {
                setMessage(httpStatus, contentType, new String(Files.readAllBytes(found.toPath()), "UTF-8"));
                success[0] = true;
            } catch (IOException e) {
                throw new CommandException("Cannot load file '%s'.", filename);
            }
        });

        if (success[0] == false) {
            throw new CommandException("Cannot find file '%s', please make sure the file exists and is on the classpath.", filename);
        }
    }

    public void setMessage(int httpStatus, String contentType, String body) {
        JsMessageResponseAction jsMessageResponse = new JsMessageResponseAction();
        jsMessageResponse.setHttpStatus(httpStatus);
        jsMessageResponse.setContentType(contentType);
        jsMessageResponse.setBody(body);
        jsResponse.setJsResponseAction(jsMessageResponse);
    }

    public void setProxy() {

    }

    public void setRedirect(int httpStatus, String url) {
        JsRedirectResponseAction jsRedirectResponse = new JsRedirectResponseAction();
        jsRedirectResponse.setHttpStatus(httpStatus);
        jsRedirectResponse.setUrl(url);
        jsResponse.setJsResponseAction(jsRedirectResponse);
    }

    @Command(name="Add JsHeader", arguments = {"key", "value"},
        example =
            "|addHeader; |myheader |value |\n")
    public void addHeader(String key, String value) {
        jsResponse.getJsResponseAction().addHeader(new JsHeader(key, value));
    }

    @Command(name="Add JsCookie", arguments = {"name", "value", "domain", "path", "version", "maxAge"},
        example =
            "|addCookie; |mycookie |value |sodeso.nl |/ |1 |3600 |\n")
    public void addCookie(String name, String value, String domain, String path, String version, String maxAge) {
        jsResponse.getJsResponseAction().addCookie(
                new JsCookie.Builder("create", name, value)
                    .withDomain(domain)
                    .withPath(path)
                    .withVersion(version)
                    .withMaxAge(maxAge)
                    .build());
    }

    @Command(name="Delete JsCookie", arguments = {"name"},
        example =
            "|deleteCookie; |mycookie |\n")
    public void deleteCookie(String name) {
        jsResponse.getJsResponseAction().addCookie(new JsCookie.Builder("delete", name, null).build());
    }

    @Command(name="JsCondition: Body using XPath", arguments = {"xpath", "matches"},
        example =
            "|addBodyConditionUsingXpath; |//*[local-name()='element'] |value |\n")
    public void addBodyConditionUsingXpath(String xpath, String matches) {
        jsResponse.addCondition(new JsBodyCondition.Builder(matches).withXpath(xpath).build());
    }

    @Command(name="JsCondition: Body using Json JsPath", arguments = {"jsonpath", "matches"},
        example =
            "|addBodyConditionUsingJsonPath; |$.request.reference |value |\n")
    public void addBodyConditionUsingJsonPath(String jsonPath, String matches) {
        jsResponse.addCondition(new JsBodyCondition.Builder(matches).withJsonpath(jsonPath).build());
    }

    @Command(name="JsCondition: Body using Regulair JsExpression", arguments = {"regexp", "matches"},
        example =
            "|addBodyConditionUsingRegexp;   |<reference>(.*?)</reference> |value |\n")
    public void addBodyConditionUsingRegexp(String regexp, String matches) {
        jsResponse.addCondition(new JsBodyCondition.Builder(matches).withRegexp(regexp).build());
    }

    @Command(name="JsCondition: Body (comparing one-on-one)", arguments = {"matches"},
            example =
                    "|addBodyCondition; |value |\n")
    public void addBodyCondition(String matches) {
        jsResponse.addCondition(new JsBodyCondition.Builder(matches).build());
    }

    @Command(name="JsCondition: JsCookie using XPath", arguments = {"name", "xpath", "matches"},
        example =
            "|addCookieConditionUsingXpath; |mycookie |//*[local-name()='element'] |value |\n")
    public void addCookieConditionUsingXpath(String name, String xpath, String matches) {
        jsResponse.addCondition(new JsCookieCondition.Builder(name, matches).withXpath(xpath).build());
    }

    @Command(name="JsCondition: JsCookie using Json JsPath", arguments = {"name", "jsonpath", "matches"},
        example =
            "|addCookieConditionUsingJsonPath; |mycookie |$.request.reference |value |\n")
    public void addCookieConditionUsingJsonPath(String name, String jsonPath, String matches) {
        jsResponse.addCondition(new JsCookieCondition.Builder(name, matches).withJsonpath(jsonPath).build());
    }

    @Command(name="JsCondition: JsCookie using Regulair JsExpression", arguments = {"name", "regexp", "matches"},
        example =
            "|addCookieConditionUsingRegexpPath; |mycookie |<reference>(.*?)</reference> |value |\n")
    public void addCookieConditionUsingRegexpPath(String name, String regexp, String matches) {
        jsResponse.addCondition(new JsCookieCondition.Builder(name, matches).withRegexp(regexp).build());
    }

    @Command(name="JsCondition: JsHeader using XPath", arguments = {"name", "xpath", "matches"},
        example =
            "|addHeaderConditionUsingXpath; |myheader |//*[local-name()='element'] |value |\n")
    public void addHeaderConditionUsingXpath(String name, String xpath, String matches) {
        jsResponse.addCondition(new JsHeaderCondition.Builder(name, matches).withXpath(xpath).build());
    }

    @Command(name="JsCondition: JsHeader using Json JsPath", arguments = {"name", "jsonpath", "matches"},
        example =
            "|addHeaderConditionUsingJsonPath; |myheader |$.request.reference |value |\n")
    public void addHeaderConditionUsingJsonPath(String name, String jsonPath, String matches) {
        jsResponse.addCondition(new JsHeaderCondition.Builder(name, matches).withJsonpath(jsonPath).build());
    }

    @Command(name="JsCondition: JsHeader using Regulair JsExpression", arguments = {"name", "regexp", "matches"},
        example =
            "|addHeaderConditionUsingRegexpPath; |myheader |<reference>(.*?)</reference> |value |\n")
    public void addHeaderConditionUsingRegexpPath(String name, String regexp, String matches) {
        jsResponse.addCondition(new JsHeaderCondition.Builder(name, matches).withRegexp(regexp).build());
    }

    @Command(name="JsCondition: Method (GET, PUT, POST, etc..)", arguments = {"type"},
        example =
            "|addMethodCondition; |GET |\n")
    public void addMethodCondition(String type) {
        jsResponse.addCondition(new JsMethodCondition.Builder(type).build());
    }

    public boolean submit() {
        return WebserviceMockClient.post("/mock/response/" + endpointName, Entity.entity(jsResponse, MediaType.APPLICATION_JSON_TYPE));
    }

}
