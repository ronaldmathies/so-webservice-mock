package nl.sodeso.webservice.mock.fixture.util;

import nl.sodeso.fitnesse.CommandException;
import nl.sodeso.webservice.mock.model.generic.JsStatus;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Ronald Mathies
 */
public class WebserviceMockClient {

    private enum Method {
        GET,
        DELETE
    }

    public static boolean post(String path, Entity entity) {
        if (WebServiceMockProperties.getInstance().checkMockPresentOnThisConfiguration()) {
            try {

                Client client = ClientBuilder.newBuilder().build();

                URI http = new URL("http", WebServiceMockProperties.getInstance().getHost(), WebServiceMockProperties.getInstance().getPort(), path).toURI();
                WebTarget webTarget = client.target(http);

                javax.ws.rs.core.Response response = webTarget.request().post(entity);
                if (response.getStatus() == javax.ws.rs.core.Response.Status.OK.getStatusCode()) {
                    return true;
                } else {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    throw new CommandException("Failed to post information to the webservice mock, jsStatus: %s - %s", jsStatus.getCode(), jsStatus.getMessage());
                }

            } catch (MalformedURLException | URISyntaxException e) {
                throw new CommandException("Failed to post information to the webservice mock due to an exception: %s", e.getMessage());
            }
        }

        return false;
    }

    public static boolean delete(String path, Object ... args) {
        return execute(Method.DELETE, path, args);
    }

    public static boolean get(String path, Object ... args) {
        return execute(Method.GET, path, args);
    }

    public static boolean execute(Method method, String path, Object... args) {
        if (WebServiceMockProperties.getInstance().checkMockPresentOnThisConfiguration()) {
            try {
                Client client = ClientBuilder.newBuilder().build();

                WebTarget webTarget = client.target(new URL("http", WebServiceMockProperties.getInstance().getHost(), WebServiceMockProperties.getInstance().getPort(), args != null && args.length > 0 ? String.format(path, args) : path).toURI());

                javax.ws.rs.core.Response response = null;
                if (method.equals(Method.GET)) {
                    response = webTarget.request().get();
                } else {
                    response = webTarget.request().delete();
                }

                if (response.getStatus() == javax.ws.rs.core.Response.Status.OK.getStatusCode()) {
                    return true;
                } else {
                    JsStatus jsStatus = response.readEntity(JsStatus.class);
                    throw new CommandException("Failed to post information to the webservice mock, jsStatus: %s - %s", jsStatus.getCode(), jsStatus.getMessage());
                }
            } catch (MalformedURLException | URISyntaxException e) {
                throw new CommandException("Failed to post information to the webservice mock: %s", e.getMessage());
            }
        }

        return false;
    }

}
