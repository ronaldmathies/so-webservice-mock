package nl.sodeso.webservice.mock.fixture.util;

import nl.sodeso.fitnesse.CommandException;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Ronald Mathies
 */
public class WebServiceMockProperties {

    private String host;
    private int port;

    boolean isOk = false;

    private static WebServiceMockProperties INSTANCE = null;

    private WebServiceMockProperties() {}

    public static WebServiceMockProperties getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WebServiceMockProperties();
        }

        return INSTANCE;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return this.host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return this.port;
    }

    public boolean checkMockPresentOnThisConfiguration() {
        if (isOk) {
            return isOk;
        } else {
            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection connection = (HttpURLConnection) new URL("HTTP", this.host, this.port, "/mock/version").openConnection();
                connection.setRequestMethod("GET");
                isOk = (connection.getResponseCode() == HttpURLConnection.HTTP_OK);
            } catch (Exception e) {
                System.out.println(e);
                // Ignore...
            }

            if (!isOk) {
                throw new CommandException("There is no webservice mock present on http://%s:%d ", this.host, this.port);
            }
        }

        return isOk;
    }
}
