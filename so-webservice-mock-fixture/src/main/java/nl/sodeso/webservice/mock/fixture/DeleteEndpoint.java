package nl.sodeso.webservice.mock.fixture;

import fitnesse.responders.editing.fixture.Fixture;
import fitnesse.responders.editing.fixture.Start;
import nl.sodeso.webservice.mock.fixture.util.WebserviceMockClient;

/**
 * @author Ronald Mathies
 */
@Fixture(name="Webservice Mock: Delete JsEndpoint")
public class DeleteEndpoint {

    private String endpointName = null;

    @Start(name="Delete JsEndpoint", arguments = {},
            example =
                    "!*> Delete an endpoint\n" +
                    "!|script                           |\n" +
                    "|start            |Delete JsEndpoint |\n" +
                    "|setEndpointName; |hrdomein        |\n" +
                    "|submit;                           |\n" +
                    "*!")
    public DeleteEndpoint() {}

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public boolean submit() {
        return WebserviceMockClient.delete("/mock/endpoint/%s", endpointName);
    }

}
