## 1. What is so-webservice-mock

This image is used to run te backend of the webservice mock application. It consists of a Wildfly 10 image with
the application itself.

## 2. Using so-webservice-mock

To use this image you must first make sure that you have this
image on the docker environment where you want to run it.

To achieve this execute the following two commands:

`docker login registry.gitlab.com`

Use your GitLab account as the credentials to login.

`docker pull registry.gitlab.com/ronaldmathies/so-webservice-mock:latest`

This will download and install the image in your local Docker image cache.

## 3. Running so-webservice-mock

Once the download has finished and the image is stored inside the Docker
environment we can run the image. There are some configuration parameters
involved while running the image:

`SERVICE_ID`: 

the name of the Webservice Mock service instance, this should ideally be a name 
which explains what this is instance is used for. This is also the name that is
shown in the Webservice Mock consoles.

'EXPOSED_PORT':

The exposed port should be the same as the port specified through the -P option
in the command line. This is the port that will be used by the Webservice Mock
Console applications to communicate with this specific instance of the
Webservice Mock service.

`MULTICAST_ENABLED`: 

Enable this option to use multicast as the mechanism for discovery between the 
Webservice Mock and Webservice Mock Console. Enabling this option also
requires the other MULTICAST_XXX options to be set.

`MULTICAST_GROUP`:

The multicast group is used to create a sub-group on the network where all 
Webservice Mock service and Consoles can discover each other. For example: `224.0.0.3`.

`MULTICAST_PORT`:

The port number on which the Webservice Mock service will send the multicast
messages. For example: `50727`.

`MULTICAST_INTERVAL`:

The interval (in milliseconds) in which the Webservice Mock service will send
out a message, the lower the interval the quicker the Webservice Mock Console
applications will discover the service. Setting it to low will create a lot of 
noice on the network. For example: `5000` (5 seconds).

`BROADCAST_ENABLED`:

Enable this option to use broadcast as the mechanism for discovery between the
Webservice Mock service and Webservice Mock Console application. Enabling
this option also requires the other BROADCAST_XXX options to be set. Using
broadcast is generally not a very good idea since the noice it creates on a
network is heavy (all computers on the network receive broadcast messages). It
is adviceable to use MULTICAST.

`BROADCAST_ADDRESS`:

The broadcast address is used to send out the broadcast messages on. This
should be the same address as used by the Webservice Mock Console applications.

`BROADCAST_PORT`:

The broadcast port on wich the broadcast messages should be send on.

`BROADCAST_INTERVAL`:

The interval (in milliseconds) in which the Webservice Mock service will send
out a message, the lower the interval the quicker the Webservice Mock Console
applications will discover the service. Setting it to low will create a lot of 
noice on the network. For example: `5000` (5 seconds).

Example:

`
docker run -d
    --name webservice-mock-1 \
    -p 9080:8080 \
    -e SERVICE_ID=MY-MOCK
    -e EXPOSED_PORT=9080
    -e MULTICAST_ENABLED=true
    -e MULTICAST_GROUP=224.0.0.3
    -e MULTICAST_PORT=50727
    -e MULTICAST_INTERVAL=5000
    registry.gitlab.com/ronaldmathies/so-webservice-mock:latest
`

In this case the 9080 (exposed) port is the default HTTP port for incomming
requests. So we will use this port as our exposed port.

In this configuration we don't expose the management console port, if you would
like to have access to the management console then add `-p 9990:9990` to the
run configuration.