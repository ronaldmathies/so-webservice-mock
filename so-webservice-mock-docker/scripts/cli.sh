#!/bin/bash

echo "Resolving application to deploy"
cd ${WEBSERVICE_MOCK_HOME}/bin/application

war=$(ls -t -U *.* | grep -m 1 so-webservice-mock-web-*.war)
if [ -z ${war+x} ]; \
    then \
        echo "Application to deploy not found."; \
        exit -1; \
fi

echo "Deploying '$war'"

${JBOSS_HOME}/bin/jboss-cli.sh -c --command="deploy --force $war"

