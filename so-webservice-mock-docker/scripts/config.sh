#!/bin/bash

if [ -z "$SERVICE_ID"]; then
    echo >&2 'error: service identification not specified. '
    echo >&2 '  You need to specify SERVICE_ID which will be used as the human-readable name for the mock service.'
    exit 1
fi

if [ -z "$EXPOSED_PORT"]; then
    echo >&2 'error: exposed port not specified. '
    echo >&2 '  You need to specify EXPOSED_PORT, this port number will be used by the clients to communicate with this mock service (this would often be the exposed port of the docker container).'
    exit 1
fi

if [ -z "$MULTICAST_ENABLED" -a -z "$BROADCAST_ENABLED" ]; then
    echo >&2 'error: no broadcasting technique specified. '
    echo >&2 '  You need to specify either MULTICAST_ENABLED or BROADCAST_ENABLED'
    exit 1
fi

if [ -z "$MULTICAST_ENABLED" ]; then
    if [ -z "$MULTICAST_GROUP" -a -z "$MULTICAST_PORT" -a -z "$MULTICAST_INTERVAL" ]; then
        echo >&2 'error: multicast was set to enabled but none or some of the settings are missing. '
        echo >&2 '  You need to specify MULTICAST_GROUP, MULTICAST_PORT and MULTICAST_INTERVAL.'
        exit 1
    fi
fi

if [ -z "$BROADCAST_ENABLED" ]; then
    if [ -z "$BROADCAST_ADDRESS" -a -z "$BROADCAST_PORT" -a -z "$BROADCAST_INTERVAL" ]; then
        echo >&2 'error: broadcast was set to enabled but none or some of the settings are missing. '
        echo >&2 '  You need to specify BROADCAST_ADDRESS, BROADCAST_PORT and BROADCAST_INTERVAL.'
        exit 1
    fi
fi

sed -e "s/\${SERVICE_ID}/$SERVICE_ID/" \
    -e "s/\${EXPOSED_PORT}/$EXPOSED_PORT/" so-webservice-mock-configuration.properties


if [ ! -z "$MULTICAST_ENABLED" ]; then
    sed -e "s/\${MULTICAST_ENABLED}/true/" \
        -e "s/\${BROADCAST_ENABLED}/false/" \
        -e "s/\${MULTICAST_GROUP}/$MULTICAST_GROUP/" \
        -e "s/\${MULTICAST_PORT}/$MULTICAST_PORT/" \
        -e "s/\${MULTICAST_INTERVAL}/$MULTICAST_INTERVAL/" so-webservice-mock-configuration.properties
fi

if [ ! -z "$BROADCAST_ENABLED" ]; then
    sed -e "s/\${BROADCAST_ENABLED}/true/" \
        -e "s/\${MULTICAST_ENABLED}/false/" \
        -e "s/\${BROADCAST_ADDRESS}/$BROADCAST_ADDRESS/" \
        -e "s/\${BROADCAST_PORT}/$BROADCAST_PORT/" \
        -e "s/\${BROADCAST_INTERVAL}/$BROADCAST_INTERVAL/" so-webservice-mock-configuration.properties
fi