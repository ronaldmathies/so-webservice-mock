docker stop webservice-mock
docker rm webservice-mock
docker rmi webservice/mock

docker build -t webservice/mock .

docker run -d
    --name webservice-mock-1 \
    -p 9080:8080 \
    -e SERVICE_ID=MY-MOCK
    -e EXPOSED_PORT=9080
    -e MULTICAST_ENABLED=true
    -e MULTICAST_GROUP=224.0.0.3
    -e MULTICAST_PORT=50727
    -e MULTICAST_INTERVAL=5000
    webservice/mock

docker exec -t -i webservice-mock bash