package nl.sodeso.webservice.mock.model.endpoint.response.action;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsExceptionAction;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsTimeoutAction;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsVariableAction;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "exception", value = JsExceptionAction.class),
        @JsonSubTypes.Type(name = "variable", value = JsVariableAction.class),
        @JsonSubTypes.Type(name = "timeout", value = JsTimeoutAction.class),
})
public interface JsAction extends Serializable {

}
