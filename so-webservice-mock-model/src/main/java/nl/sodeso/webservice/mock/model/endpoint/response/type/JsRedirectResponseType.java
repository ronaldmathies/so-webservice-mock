package nl.sodeso.webservice.mock.model.endpoint.response.type;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "redirect")
public class JsRedirectResponseType extends JsResponseType {

    @NotNull
    private String url = null;

    public JsRedirectResponseType() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String redirectUrl) {
        this.url = url;
    }

    @Override
    public int getHttpStatus() {
        return 0;
    }

}
