package nl.sodeso.webservice.mock.model.endpoint.response.condition;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.*;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "body", value = JsBodyCondition.class),
        @JsonSubTypes.Type(name = "cookie", value = JsCookieCondition.class),
        @JsonSubTypes.Type(name = "header", value = JsHeaderCondition.class),
        @JsonSubTypes.Type(name = "method", value = JsMethodCondition.class),
        @JsonSubTypes.Type(name = "parameter", value = JsParameterCondition.class),
        @JsonSubTypes.Type(name = "path", value = JsPathCondition.class),
        @JsonSubTypes.Type(name = "query", value = JsQueryCondition.class)
})
public interface JsCondition extends Serializable {

}
