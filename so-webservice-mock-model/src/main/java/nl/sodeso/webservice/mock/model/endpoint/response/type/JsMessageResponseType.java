package nl.sodeso.webservice.mock.model.endpoint.response.type;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "message")
public class JsMessageResponseType extends JsResponseType {

    private String contentType = "application/xml";

    private String body = null;

    public JsMessageResponseType() {}

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
