package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "method")
public class JsMethodCondition implements JsCondition {

    @NotNull
    private String type;

    public JsMethodCondition() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class Builder {

        private JsMethodCondition condition;

        public Builder(String type) {
            this.condition = new JsMethodCondition();
            this.condition.setType(type);
        }

        public JsMethodCondition build() {
            return this.condition;
        }

    }
}
