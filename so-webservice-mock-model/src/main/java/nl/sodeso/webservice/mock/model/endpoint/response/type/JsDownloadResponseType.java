package nl.sodeso.webservice.mock.model.endpoint.response.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "download")
public class JsDownloadResponseType extends JsResponseType {

    private String contentType = "application/pdf";

    @NotNull
    private String data = null;

    private long size = 0L;

    @NotNull
    private String filename = null;

    @JsonIgnore
    private String fileUuid = null;

    public JsDownloadResponseType() {}

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileUuid() {
        return fileUuid;
    }

    public void setFileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
    }
}
