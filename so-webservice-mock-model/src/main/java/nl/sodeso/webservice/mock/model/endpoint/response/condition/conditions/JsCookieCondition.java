package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.generic.JsPath;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "cookie")
public class JsCookieCondition extends JsPath implements JsCondition {

    @NotNull
    private String name;

    @NotNull
    private String matches;

    public JsCookieCondition() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatches() {
        return matches;
    }

    public void setMatches(String matches) {
        this.matches = matches;
    }

    public static class Builder {

        private JsCookieCondition condition;

        public Builder(String name, String matches) {
            this.condition = new JsCookieCondition();
            this.condition.setName(name);
            this.condition.setMatches(matches);
        }

        public JsCookieCondition.Builder withXpath(String xpath) {
            this.condition.setXpath(xpath);
            return this;
        }

        public JsCookieCondition.Builder withJsonpath(String jsonpath) {
            this.condition.setJsonpath(jsonpath);
            return this;
        }

        public JsCookieCondition.Builder withRegexp(String regexp) {
            this.condition.setRegexp(regexp);
            return this;
        }

        public JsCookieCondition build() {
            return this.condition;
        }

    }
}
