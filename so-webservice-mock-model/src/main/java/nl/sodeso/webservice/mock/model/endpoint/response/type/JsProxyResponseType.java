package nl.sodeso.webservice.mock.model.endpoint.response.type;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "proxy")
public class JsProxyResponseType extends JsResponseType {

    private String domain;

    private boolean ignoreHeaders;
    private boolean ignoreCookies;

    private boolean overrideBody;
    private String body = null;

    private int connectionTimeoutInSeconds;
    private int readTimeoutInSeconds;

    public JsProxyResponseType() {}

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isIgnoreHeaders() {
        return ignoreHeaders;
    }

    public void setIgnoreHeaders(boolean ignoreHeaders) {
        this.ignoreHeaders = ignoreHeaders;
    }

    public boolean isIgnoreCookies() {
        return ignoreCookies;
    }

    public void setIgnoreCookies(boolean ignoreCookies) {
        this.ignoreCookies = ignoreCookies;
    }

    public boolean isOverrideBody() {
        return overrideBody;
    }

    public void setOverrideBody(boolean overrideBody) {
        this.overrideBody = overrideBody;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getConnectionTimeoutInSeconds() {
        return connectionTimeoutInSeconds;
    }

    public void setConnectionTimeoutInSeconds(int connectionTimeoutInSeconds) {
        this.connectionTimeoutInSeconds = connectionTimeoutInSeconds;
    }

    public int getReadTimeoutInSeconds() {
        return readTimeoutInSeconds;
    }

    public void setReadTimeoutInSeconds(int readTimeoutInSeconds) {
        this.readTimeoutInSeconds = readTimeoutInSeconds;
    }
}
