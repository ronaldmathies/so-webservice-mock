package nl.sodeso.webservice.mock.model.endpoint.response.settings;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("settings")
public class JsSettings implements Serializable {

    private boolean isDeleteAfterUse = false;

    public JsSettings() {}

    public boolean isDeleteAfterUse() {
        return isDeleteAfterUse;
    }

    public void setDeleteAfterUse(boolean deleteAfterUse) {
        this.isDeleteAfterUse = deleteAfterUse;
    }

}
