package nl.sodeso.webservice.mock.model.endpoint.settings;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class JsLogging implements Serializable {

    private boolean enableLogging = false;
    private int numberOfRequestsToStore = 20;

    @NotNull
    @JsonProperty("expression")
    private JsExpression jsExpression;

    public JsLogging() {}

    public boolean isEnableLogging() {
        return enableLogging;
    }

    public void setEnableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
    }

    public int getNumberOfRequestsToStore() {
        return numberOfRequestsToStore;
    }

    public void setNumberOfRequestsToStore(int numberOfRequestsToStore) {
        this.numberOfRequestsToStore = numberOfRequestsToStore;
    }

    public JsExpression getJsExpression() {
        return jsExpression;
    }

    public void setJsExpression(JsExpression jsExpression) {
        this.jsExpression = jsExpression;
    }
}
