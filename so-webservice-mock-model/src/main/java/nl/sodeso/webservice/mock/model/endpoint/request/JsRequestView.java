package nl.sodeso.webservice.mock.model.endpoint.request;

/**
 * @author Ronald Mathies
 */
public class JsRequestView {

    public static class Summary {}

    public static class Detailed extends Summary {}

}
