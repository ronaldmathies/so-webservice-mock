package nl.sodeso.webservice.mock.model.endpoint.response;

/**
 * @author Ronald Mathies
 */
public class JsResponseView {

    public static class Summary {}

    public static class Detailed extends Summary {}

    public static class Requests extends Detailed {}

}
