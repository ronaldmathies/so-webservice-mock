package nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;

/**
 * @author Ronald Mathies
 */
public class JsUuidExpression implements JsExpression {

    public static final String TYPE = "uuid";

    public JsUuidExpression() {}

}
