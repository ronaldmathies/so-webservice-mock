package nl.sodeso.webservice.mock.model.endpoint.response.action.actions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "timeout")
public class JsTimeoutAction implements JsAction {

    @NotNull
    private String seconds;

    public JsTimeoutAction() {}

    public String getSeconds() {
        return this.seconds;
    }

    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }
}
