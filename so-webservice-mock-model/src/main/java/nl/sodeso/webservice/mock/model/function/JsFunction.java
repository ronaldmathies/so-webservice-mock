package nl.sodeso.webservice.mock.model.function;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("function")
public class JsFunction {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String documentation;

    @NotNull
    private String format;

    @JsonProperty("parameters")
    private List<JsFunctionParameter> jsFunctionParameters = new ArrayList<>();

    @JsonProperty("samples")
    private List<JsSample> jsSamples = new ArrayList<>();

    public JsFunction() {}

    public JsFunction(String name, String description, String documentation, String format) {
        this.name = name;
        this.description = description;
        this.documentation = documentation;
        this.format = format;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public List<JsFunctionParameter> getJsFunctionParameters() {
        return jsFunctionParameters;
    }

    public void setJsFunctionParameters(List<JsFunctionParameter> jsFunctionParameters) {
        this.jsFunctionParameters = jsFunctionParameters;
    }

    public void addJsParameter(JsFunctionParameter jsFunctionParameter) {
        this.jsFunctionParameters.add(jsFunctionParameter);
    }

    public List<JsSample> getJsSamples() {
        return jsSamples;
    }

    public void setJsSamples(List<JsSample> jsSamples) {
        this.jsSamples = jsSamples;
    }

    public void addJsSample(JsSample jsSample) {
        this.jsSamples.add(jsSample);
    }

}
