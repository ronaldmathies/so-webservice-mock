package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.generic.JsParameter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "parameter")
public class JsParameterCondition implements JsCondition {

    private String path;
    private List<JsParameter> parameters = new ArrayList<>();

    public JsParameterCondition() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<JsParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<JsParameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(JsParameter parameter) {
        this.parameters.add(parameter);
    }

    public JsParameter parameterWithName(String name) {
        for (JsParameter parameter : parameters) {
            if (parameter.getName().equals(name)) {
                return parameter;
            }
        }

        return null;
    }

}
