package nl.sodeso.webservice.mock.model.endpoint.settings.expression;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.*;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
    @JsonSubTypes.Type(name = JsBodyExpression.TYPE, value = JsBodyExpression.class),
    @JsonSubTypes.Type(name = JsCookieExpression.TYPE, value = JsCookieExpression.class),
    @JsonSubTypes.Type(name = JsHeaderExpression.TYPE, value = JsHeaderExpression.class),
    @JsonSubTypes.Type(name = JsQueryExpression.TYPE, value = JsQueryExpression.class),
    @JsonSubTypes.Type(name = JsUuidExpression.TYPE, value = JsUuidExpression.class)
})
public interface JsExpression extends Serializable {

}
