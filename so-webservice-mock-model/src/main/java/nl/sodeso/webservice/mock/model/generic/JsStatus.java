package nl.sodeso.webservice.mock.model.generic;


/**
 * @author Ronald Mathies
 */
public class JsStatus {

    public static final String OK = "OK";
    public static final String ERR = "ERR";

    private String code;
    private String message;

    public JsStatus() {}

    public JsStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
