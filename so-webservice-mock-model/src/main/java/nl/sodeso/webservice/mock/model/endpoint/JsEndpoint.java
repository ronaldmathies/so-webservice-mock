package nl.sodeso.webservice.mock.model.endpoint;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import nl.sodeso.webservice.mock.model.endpoint.settings.JsLogging;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Ronald Mathies
 */

@JsonTypeName("endpoint")
public class JsEndpoint {

    @JsonView({JsEndpointView.Summary.class})
    @NotNull
    private String name;

    @JsonView({JsEndpointView.Detailed.class})
    private String path;

    @JsonView({JsEndpointView.Detailed.class})
    @JsonProperty("logging")
    private JsLogging jsLogging;

    @JsonView({JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    @JsonProperty("responses")
    private List<JsResponse> jsResponses = new ArrayList<>();

    @JsonView({JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class})
    @JsonProperty("requests")
    private List<JsRequest> jsRequests = new ArrayList<>();

    public JsEndpoint() {}

    public JsEndpoint(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public JsLogging getJsLogging() {
        return jsLogging;
    }

    public void setJsLogging(JsLogging jsLogging) {
        this.jsLogging = jsLogging;
    }

    public List<JsResponse> getJsResponses() {
        return jsResponses;
    }

    public void setJsResponses(List<JsResponse> jsResponses) {
        this.jsResponses = jsResponses;
    }

    public void addJsResponse(JsResponse jsResponse) {
        this.jsResponses.add(jsResponse);
    }

    public void clearJsResponses() {
        this.jsResponses.clear();
    }

    public boolean removeJsRequest(String id) {
        return jsRequests.removeIf(request -> request.getId().equals(id));
    }

    public void clearJsRequests() {
        jsRequests.clear();
    }

    public boolean removeJsResponse(String name) {
        return jsResponses.removeIf(response -> response.getName().equals(name));
    }

    public Optional<JsResponse> getJsResponseByName(String name) {
        return this.jsResponses.stream().filter(response -> response.getName().equals(name)).findFirst();
    }

    public List<JsRequest> getJsRequests() {
        return jsRequests;
    }

    public void setJsRequests(List<JsRequest> jsRequests) {
        this.jsRequests = jsRequests;
    }

    public void addJsRequest(JsRequest jsRequest) {
        this.jsRequests.add(jsRequest);
    }

    public Optional<JsRequest> getRequestById(String id) {
        return jsRequests.stream().filter(jsRequest -> jsRequest.getId().equals(id)).findFirst();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JsEndpoint that = (JsEndpoint) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
