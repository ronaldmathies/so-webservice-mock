package nl.sodeso.webservice.mock.model.endpoint.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonView;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpointView;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsResponseType;
import nl.sodeso.webservice.mock.model.endpoint.response.settings.JsSettings;
import nl.sodeso.webservice.mock.model.endpoint.settings.JsLogging;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("response")
public class JsResponse implements Serializable {

    @NotNull
    @JsonView({JsResponseView.Summary.class, JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    private String name = null;

    @NotNull
    @JsonProperty("settings")
    @JsonView({JsResponseView.Detailed.class, JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    private JsSettings jsSettings = new JsSettings();

    @JsonView({JsResponseView.Detailed.class, JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    @JsonProperty("logging")
    private JsLogging jsLogging;

    @NotNull
    @JsonProperty("response")
    @JsonView({JsResponseView.Detailed.class, JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    private JsResponseType jsResponseType = null;

    @NotNull
    @JsonProperty("conditions")
    @JsonView({JsResponseView.Detailed.class, JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    private List<JsCondition> jsConditions = new ArrayList<>();

    @JsonProperty("actions")
    @JsonView({JsResponseView.Detailed.class, JsEndpointView.Responses.class, JsEndpointView.ResponsesAndRequests.class})
    private List<JsAction> jsActions = new ArrayList<>();


    @JsonView({JsResponseView.Requests.class})
    @JsonProperty("requests")
    private transient List<JsRequest> jsRequests = new ArrayList<>();

    public JsResponse() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsLogging getJsLogging() {
        return this.jsLogging;
    }

    public void setJsLogging(JsLogging jsLogging) {
        this.jsLogging = jsLogging;
    }

    public JsResponseType getJsResponseType() {
        return jsResponseType;
    }

    public void setJsResponseType(JsResponseType jsResponseType) {
        this.jsResponseType = jsResponseType;
    }

    public JsSettings getJsSettings() {
        return jsSettings;
    }

    public void setJsSettings(JsSettings jsSettings) {
        this.jsSettings = jsSettings;
    }

    public List<JsCondition> getJsConditions() {
        return jsConditions;
    }

    public void setJsConditions(List<JsCondition> jsConditions) {
        this.jsConditions = jsConditions;
    }

    public void addCondition(JsCondition jsCondition) {
        this.jsConditions.add(jsCondition);
    }

    public List<JsAction> getJsActions() {
        return jsActions;
    }

    public void setJsActions(List<JsAction> jsActions) {
        this.jsActions = jsActions;
    }

    public void addAction(JsAction jsAction) {
        this.jsActions.add(jsAction);
    }

    public List<JsRequest> getJsRequests() {
        return jsRequests;
    }

    public void setJsRequests(List<JsRequest> jsRequests) {
        this.jsRequests = jsRequests;
    }

    public void addJsRequest(JsRequest jsRequest) {
        this.jsRequests.add(jsRequest);
    }

    public boolean removeJsRequest(String id) {
        return jsRequests.removeIf(request -> request.getId().equals(id));
    }

    public void clearJsRequests() {
        jsRequests.clear();
    }

    public Optional<JsRequest> getJsRequestById(String id) {
        return jsRequests.stream().filter(jsRequest -> jsRequest.getId().equals(id)).findFirst();
    }

}
