package nl.sodeso.webservice.mock.model.service;


import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("service")
public class JsService {

    @NotNull
    private String name;

    @NotNull
    private String version;

    public JsService() {}

    public JsService(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
