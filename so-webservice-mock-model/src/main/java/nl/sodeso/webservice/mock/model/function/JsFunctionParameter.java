package nl.sodeso.webservice.mock.model.function;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("parameter")
public class JsFunctionParameter {

    public String name;
    public String type;

    public JsFunctionParameter() {}

    public JsFunctionParameter(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
