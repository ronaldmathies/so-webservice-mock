package nl.sodeso.webservice.mock.model.endpoint;

/**
 * @author Ronald Mathies
 */
public class JsEndpointView {

    public static class Summary {}

    public static class Detailed extends Summary {}

    public static class Responses extends Detailed {}

    public static class Requests extends Detailed {}

    public static class ResponsesAndRequests extends Detailed {}
}
