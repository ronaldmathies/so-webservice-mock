package nl.sodeso.webservice.mock.model.endpoint.response.action.actions;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class JsVariableValue implements Serializable {

    private String scope;
    private String name;
    private String value;

    public JsVariableValue() {}

    public JsVariableValue(String scope, String name, String value) {
        this.scope = scope;
        this.name = name;
        this.value = value;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
