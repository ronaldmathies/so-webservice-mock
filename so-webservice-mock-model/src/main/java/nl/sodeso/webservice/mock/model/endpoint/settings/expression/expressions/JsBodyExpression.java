package nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.model.generic.JsPath;

/**
 * @author Ronald Mathies
 */
public class JsBodyExpression extends JsPath implements JsExpression {

    public static final String TYPE = "body";

    public JsBodyExpression() {}

}
