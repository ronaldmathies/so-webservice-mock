package nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.model.generic.JsPath;

/**
 * @author Ronald Mathies
 */
public class JsCookieExpression extends JsPath implements JsExpression {

    public static final String TYPE = "cookie";

    private String name;

    public JsCookieExpression() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
