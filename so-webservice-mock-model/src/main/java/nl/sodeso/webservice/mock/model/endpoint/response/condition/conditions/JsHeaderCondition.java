package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.generic.JsPath;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "header")
public class JsHeaderCondition extends JsPath implements JsCondition {

    @NotNull
    private String name;

    @NotNull
    private String matches;

    public JsHeaderCondition() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatches() {
        return matches;
    }

    public void setMatches(String matches) {
        this.matches = matches;
    }

    public static class Builder {

        private JsHeaderCondition condition;

        public Builder(String name, String matches) {
            this.condition = new JsHeaderCondition();
            this.condition.setName(name);
            this.condition.setMatches(matches);
        }

        public JsHeaderCondition.Builder withXpath(String xpath) {
            this.condition.setXpath(xpath);
            return this;
        }

        public JsHeaderCondition.Builder withJsonpath(String jsonpath) {
            this.condition.setJsonpath(jsonpath);
            return this;
        }

        public JsHeaderCondition.Builder withRegexp(String regexp) {
            this.condition.setRegexp(regexp);
            return this;
        }

        public JsHeaderCondition build() {
            return this.condition;
        }

    }
}
