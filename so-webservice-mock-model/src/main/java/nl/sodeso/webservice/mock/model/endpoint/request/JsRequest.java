package nl.sodeso.webservice.mock.model.endpoint.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonView;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpointView;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponseView;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.model.generic.JsHeader;
import nl.sodeso.webservice.mock.model.generic.JsQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("request")
public class JsRequest {

    @JsonView({JsRequestView.Summary.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private String id;

    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private long time;

    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private String body;

    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private String requestUri;

    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private String requestUrl;

    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private String method;

    @JsonProperty("headers")
    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private List<JsHeader> jsHeaders = new ArrayList<>();

    @JsonProperty("queries")
    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private List<JsQuery> jsQueries = new ArrayList<>();

    @JsonProperty("cookies")
    @JsonView({JsRequestView.Detailed.class, JsEndpointView.Requests.class, JsEndpointView.ResponsesAndRequests.class, JsResponseView.Requests.class})
    private List<JsCookie> jsCookies = new ArrayList<>();

    public JsRequest() {
        this.time = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return this.time;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestUrl() {
        return this.requestUrl;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestUri() {
        return this.requestUri;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return this.method;
    }

    public List<JsQuery> getJsQueries() {
        return this.jsQueries;
    }

    public void addJsQuery(String name, String value) {
        this.jsQueries.add(new JsQuery(name, value));
    }

    public String getFullJsQuery() {
        StringBuilder fullJsQuey = new StringBuilder("?");

        jsQueries.forEach(jsQuery -> {
            fullJsQuey.append(jsQuery.getName()).append("=").append(jsQuery.getValue());
        });

        return fullJsQuey.toString();
    }

    public Optional<JsQuery> getJsQuery(String name) {
        return this.jsQueries.stream().filter(jsQuery -> jsQuery.getName().equals(name)).findFirst();
    }

    public List<JsHeader> getJsHeaders() {
        return this.jsHeaders;
    }

    public void addJsHeader(String name, String value) {
        this.jsHeaders.add(new JsHeader(name, value));
    }

    public Optional<JsHeader> getJsHeader(String name) {
        return this.jsHeaders.stream().filter(jsHeader -> jsHeader.getName().equals(name)).findFirst();
    }

    public List<JsCookie> getJsCookies() {
        return this.jsCookies;
    }

    public void addJsCookie(JsCookie jsCookie) {
        this.jsCookies.add(jsCookie);
    }

//    public void addJsCookie(String name, String value, String domain, String path, String version, String maxAge) {
//        JsCookie jsCookie = new JsCookie("", name, value);
//        jsCookie.setDomain(domain);
//        jsCookie.setPath(path);
//        jsCookie.setVersion(version);
//        jsCookie.setMaxAge(maxAge);
//        this.jsCookies.add(jsCookie);
//    }

    public Optional<JsCookie> getJsCookie(String name) {
        return this.jsCookies.stream().filter(jsCookie -> jsCookie.getName().equals(name)).findFirst();
    }

    public static class Builder {

        private JsRequest jsRequest;

        public Builder() {
            this.jsRequest = new JsRequest();
        }

        public Builder withRequestUrl(String requestUrl) {
            jsRequest.setRequestUrl(requestUrl);
            return this;
        }

        public Builder withRequestUri(String requestUri) {
            jsRequest.setRequestUri(requestUri);
            return this;
        }

        public Builder withMethod(String method) {
            jsRequest.setMethod(method);
            return this;
        }

        public Builder withBody(String body) {
            jsRequest.setBody(body);
            return this;
        }

        public Builder withHeader(String name, String value) {
            jsRequest.addJsHeader(name, value);
            return this;
        }

        public Builder withCookie(JsCookie jsCookie) {
            jsRequest.addJsCookie(jsCookie);
            return this;
        }

        public Builder withQuery(String name, String value) {
            jsRequest.addJsQuery(name, value);
            return this;
        }

        public JsRequest build() {
            return this.jsRequest;
        }

    }
}
