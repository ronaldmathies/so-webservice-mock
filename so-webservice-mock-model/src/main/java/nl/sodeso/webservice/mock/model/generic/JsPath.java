package nl.sodeso.webservice.mock.model.generic;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class JsPath implements Serializable {

    private String xpath;
    private String jsonpath;
    private String regexp;

    public JsPath() {}

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public String getJsonpath() {
        return jsonpath;
    }

    public void setJsonpath(String jsonpath) {
        this.jsonpath = jsonpath;
    }

    public String getRegexp() {
        return regexp;
    }

    public void setRegexp(String regexp) {
        this.regexp = regexp;
    }

}
