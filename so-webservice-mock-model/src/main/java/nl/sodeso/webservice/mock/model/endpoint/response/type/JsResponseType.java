package nl.sodeso.webservice.mock.model.endpoint.response.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.model.generic.JsHeader;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "message", value = JsMessageResponseType.class),
        @JsonSubTypes.Type(name = "proxy", value = JsProxyResponseType.class),
        @JsonSubTypes.Type(name = "redirect", value = JsRedirectResponseType.class),
        @JsonSubTypes.Type(name = "download", value = JsDownloadResponseType.class)
})
public abstract class JsResponseType implements Serializable {

    private int httpStatus = 200;

    @JsonProperty("cookies")
    private List<JsCookie> jsCookies = new ArrayList<>();

    @JsonProperty("headers")
    private List<JsHeader> jsHeaders = new ArrayList<>();

    public JsResponseType() {}

    public int getHttpStatus() {
        return this.httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public List<JsCookie> getJsCookies() {
        return jsCookies;
    }

    public void setJsCookies(List<JsCookie> jsCookies) {
        this.jsCookies = jsCookies;
    }

    public void addCookie(JsCookie jsCookie) {
        this.jsCookies.add(jsCookie);
    }

    public List<JsHeader> getJsHeaders() {
        return jsHeaders;
    }

    public void setJsHeaders(List<JsHeader> jsHeaders) {
        this.jsHeaders = jsHeaders;
    }

    public void addHeader(JsHeader jsHeader) {
        this.jsHeaders.add(jsHeader);
    }

}
