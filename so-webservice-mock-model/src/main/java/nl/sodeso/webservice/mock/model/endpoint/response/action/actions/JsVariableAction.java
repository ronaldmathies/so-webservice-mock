package nl.sodeso.webservice.mock.model.endpoint.response.action.actions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "variable")
public class JsVariableAction implements JsAction {

    private List<JsVariableValue> variables = new ArrayList<>();

    public JsVariableAction() {}

    public List<JsVariableValue> getVariables() {
        return variables;
    }

    public void setVariables(List<JsVariableValue> variables) {
        this.variables = variables;
    }

    public void addVariable(JsVariableValue variable) {
        this.variables.add(variable);
    }

}

