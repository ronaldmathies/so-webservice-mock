package nl.sodeso.webservice.mock.model.generic;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class JsQuery implements Serializable {

    private String name;
    private String value;

    public JsQuery() {}

    public JsQuery(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
