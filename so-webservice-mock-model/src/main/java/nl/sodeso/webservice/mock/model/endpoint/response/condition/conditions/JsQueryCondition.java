package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.generic.JsQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "query")
public class JsQueryCondition implements JsCondition {

    private List<JsQuery> queries = new ArrayList<>();

    public JsQueryCondition() {
    }

    public List<JsQuery> getQueries() {
        return queries;
    }

    public void setQueries(List<JsQuery> queries) {
        this.queries = queries;
    }

    public void addQuery(JsQuery jsQuery) {
        this.queries.add(jsQuery);
    }
}
