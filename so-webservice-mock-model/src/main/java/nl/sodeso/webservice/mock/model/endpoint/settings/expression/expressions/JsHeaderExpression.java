package nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.model.generic.JsPath;

/**
 * @author Ronald Mathies
 */
public class JsHeaderExpression extends JsPath implements JsExpression {

    public static final String TYPE = "header";

    private String name;

    public JsHeaderExpression() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
