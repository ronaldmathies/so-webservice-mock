package nl.sodeso.webservice.mock.model.function;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("parameter")
public class JsSampleParameter {

    private String name;
    private String value;

    public JsSampleParameter() {}

    public JsSampleParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
