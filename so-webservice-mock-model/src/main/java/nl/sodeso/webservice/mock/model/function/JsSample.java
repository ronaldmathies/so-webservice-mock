package nl.sodeso.webservice.mock.model.function;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@JsonTypeName("sample")
public class JsSample {

    private String name;

    private List<JsSampleParameter> jsSampleParameters = new ArrayList<>();

    public JsSample() {}

    public JsSample(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addJsSampleParameter(JsSampleParameter jsSampleParameter) {
        this.jsSampleParameters.add(jsSampleParameter);
    }

    public List<JsSampleParameter> getJsSampleParameters() {
        return jsSampleParameters;
    }

    public void setJsSampleParameters(List<JsSampleParameter> jsSampleParameters) {
        this.jsSampleParameters = jsSampleParameters;
    }
}
