package nl.sodeso.webservice.mock.model.generic;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class JsHeader implements Serializable {

    @NotNull
    private String name;

    @NotNull
    private String value;

    public JsHeader() {}

    public JsHeader(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
