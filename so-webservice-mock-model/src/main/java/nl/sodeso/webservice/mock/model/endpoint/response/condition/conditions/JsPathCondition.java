package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "path")
public class JsPathCondition implements JsCondition {

    private String path;
    private boolean isRegexp = false;

    public JsPathCondition() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isRegexp() {
        return isRegexp;
    }

    public void setRegexp(boolean regexp) {
        isRegexp = regexp;
    }
}
