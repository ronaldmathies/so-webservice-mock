package nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.model.generic.JsPath;

import javax.validation.constraints.NotNull;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "body")
public class JsBodyCondition extends JsPath implements JsCondition {

    @NotNull
    private String matches;

    public JsBodyCondition() {}

    public String getMatches() {
        return matches;
    }

    public void setMatches(String matches) {
        this.matches = matches;
    }

    public static class Builder {

        private JsBodyCondition condition;

        public Builder(String matches) {
            this.condition = new JsBodyCondition();
            this.condition.setMatches(matches);
        }

        public Builder withXpath(String xpath) {
            this.condition.setXpath(xpath);
            return this;
        }

        public Builder withJsonpath(String jsonpath) {
            this.condition.setJsonpath(jsonpath);
            return this;
        }

        public Builder withRegexp(String regexp) {
            this.condition.setRegexp(regexp);
            return this;
        }

        public JsBodyCondition build() {
            return this.condition;
        }

    }
}
