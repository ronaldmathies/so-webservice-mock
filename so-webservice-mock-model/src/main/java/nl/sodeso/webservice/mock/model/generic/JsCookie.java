package nl.sodeso.webservice.mock.model.generic;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.net.HttpCookie;

/**
 * @author Ronald Mathies
 */
public class JsCookie implements Serializable {

    @NotNull
    private String action;

    @NotNull
    private String name;

    private String value;
    private String comment;

    private String domain;
    private String path;
    private String version;
    private String maxAge;

    private boolean isSecure;
    private boolean isHttpOnly;

    public JsCookie() {}

    public JsCookie(String action, String name, String value) {
        this.action = action;
        this.name = name;
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean hasComment() {
        return comment != null && !comment.isEmpty();
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean hasDomain() {
        return domain != null && !domain.isEmpty();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean hasPath() {
        return path != null && !path.isEmpty();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean hasVersion() {
        return version != null && !version.isEmpty();
    }

    public String getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public boolean hasMaxAge() {
        return maxAge != null && !maxAge.isEmpty();
    }

    public boolean isSecure() {
        return isSecure;
    }

    public void setSecure(boolean secure) {
        isSecure = secure;
    }

    public boolean isHttpOnly() {
        return isHttpOnly;
    }

    public void setHttpOnly(boolean httpOnly) {
        isHttpOnly = httpOnly;
    }

    public static class Builder {

        private JsCookie jsCookie;

        public Builder(String name, String value) {
            this.jsCookie = new JsCookie(null, name, value);
            this.jsCookie.setMaxAge("1");
        }

        public Builder(String action, String name, String value) {
            this.jsCookie = new JsCookie(action, name, value);
            this.jsCookie.setMaxAge("1");
        }

        public JsCookie.Builder withDomain(String domain) {
            this.jsCookie.setDomain(domain);
            return this;
        }

        public JsCookie.Builder withPath(String path) {
            this.jsCookie.setPath(path);
            return this;
        }

        public JsCookie.Builder withVersion(String version) {
            this.jsCookie.setVersion(version);
            return this;
        }

        public JsCookie.Builder withMaxAge(String maxAge) {
            this.jsCookie.setMaxAge(maxAge);
            return this;
        }

        public JsCookie.Builder withSecure(boolean isSecure) {
            this.jsCookie.isSecure = isSecure;
            return this;
        }

        public JsCookie.Builder withHttpOnly(boolean isHttpOnly) {
            this.jsCookie.isHttpOnly = isHttpOnly;
            return this;
        }

        public JsCookie build() {
            return this.jsCookie;
        }

    }
}
