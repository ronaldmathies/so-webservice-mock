package nl.sodeso.webservice.mock.model.endpoint.response.action.actions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;

/**
 * @author Ronald Mathies
 */
@JsonTypeName(value = "exception")
public class JsExceptionAction implements JsAction {

    public JsExceptionAction() {}

}
