package nl.sodeso.webservice.mock.service.factory.response;

import nl.sodeso.webservice.mock.model.endpoint.response.type.JsDownloadResponseType;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsMessageResponseType;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsProxyResponseType;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsRedirectResponseType;
import nl.sodeso.webservice.mock.service.factory.ResponseFactory;
import nl.sodeso.webservice.mock.service.factory.response.responses.DownloadResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.responses.MessageResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.responses.ProxyResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.responses.RedirectResponseExecutor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class JsResponseTypeMetaFactoryTest {

    @Test
    public void getInstance() throws Exception {
        ResponseFactory instance = ResponseFactory.getInstance();
        assertNotNull(instance);
    }

    @Test
    public void getMessageResponseExecutor() throws Exception {
        ResponseExecutor executor = ResponseFactory.getInstance().getExecutor(new JsMessageResponseType());
        assertEquals(executor.getClass(), MessageResponseExecutor.class);
    }

    @Test
    public void getProxyResponseExecutor() throws Exception {
        ResponseExecutor executor = ResponseFactory.getInstance().getExecutor(new JsProxyResponseType());
        assertEquals(executor.getClass(), ProxyResponseExecutor.class);
    }

    @Test
    public void getRedirectResponseExecutor() throws Exception {
        ResponseExecutor executor = ResponseFactory.getInstance().getExecutor(new JsRedirectResponseType());
        assertEquals(executor.getClass(), RedirectResponseExecutor.class);
    }

    @Test
    public void getDownloadResponseExecutor() throws Exception {
        ResponseExecutor executor = ResponseFactory.getInstance().getExecutor(new JsDownloadResponseType());
        assertEquals(executor.getClass(), DownloadResponseExecutor.class);
    }
}