package nl.sodeso.webservice.mock.service.factory.action.actions;

import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsTimeoutAction;
import nl.sodeso.webservice.mock.service.factory.action.BaseActionExecutorTest;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsTimeoutJsActionExecutorTest extends BaseActionExecutorTest {

    @Test
    public void standard() throws Exception {
        JsTimeoutAction jsTimeoutAction = new JsTimeoutAction();
        jsTimeoutAction.setSeconds("2");

        long start = System.currentTimeMillis();
        executeAction(null, jsTimeoutAction);
        long duration = System.currentTimeMillis() - start;

        assertTrue(duration > 1999L);
    }

    @Test
    public void withFunctions() throws Exception {
        JsTimeoutAction jsTimeoutAction = new JsTimeoutAction();
        jsTimeoutAction.setSeconds("_random(2, 3)");

        long start = System.currentTimeMillis();
        executeAction(null, jsTimeoutAction);
        long duration = System.currentTimeMillis() - start;

        assertTrue(duration > 1999L);
        assertTrue(duration < 3200L); // 3200 is much higher then 3000, this is due to the overhead en inprecise results.
    }

}