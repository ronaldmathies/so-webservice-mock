package nl.sodeso.webservice.mock.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * @author Ronald Mathies
 */
public class CheckJsonMessages {

    @Test
    public void standard() throws IOException {
        pathToCheck("actions", JsResponse.class);
        pathToCheck("conditions", JsResponse.class);
        pathToCheck("cookies", JsResponse.class);
        pathToCheck("functions", JsResponse.class);
        pathToCheck("endpoint", JsEndpoint.class);
    }

    @SuppressWarnings("unchecked")
    private void pathToCheck(String path, Class clazz) throws IOException {
        Files.walkFileTree(new File(System.getProperty("user.dir") + "/src/test/resources/samples/" + path).toPath(), new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                File file = path.toFile();

                if (file.getName().endsWith("json")) {

                    try {
                        ObjectMapper mapper = new ObjectMapper();
                        Object object = mapper.readValue(file, clazz);
                        assertNotNull(object);
                    } catch (Exception e) {
                        fail(String.format("Failed to precess file '%s' due to error '%s'.", file.getName(), e.getMessage()));
                    }
                }

                return FileVisitResult.CONTINUE;
            }
        });
    }

}
