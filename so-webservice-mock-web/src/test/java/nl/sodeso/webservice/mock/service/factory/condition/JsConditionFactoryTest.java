package nl.sodeso.webservice.mock.service.factory.condition;

import nl.sodeso.webservice.mock.service.factory.ConditionFactory;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class JsConditionFactoryTest {

    @Test
    public void getInstance() throws Exception {
        ConditionFactory conditionFactory = ConditionFactory.getInstance();
        assertNotNull(conditionFactory);
    }

}