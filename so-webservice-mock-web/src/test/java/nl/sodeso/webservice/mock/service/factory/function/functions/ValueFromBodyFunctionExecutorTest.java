package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class ValueFromBodyFunctionExecutorTest {

    @Test
    public void valueFromBodyUsingJsonPath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("{ \"request\": { \"reference\": \"value\" }}")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromBodyUsingJsonPath($.request.reference)");
        assertEquals("value", result);
    }

    @Test
    public void valueFromBodyUsingRegExp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("<element>value</element>")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromBodyUsingRegExp(<element>(.*?)</element>)");
        assertEquals("value", result);
    }

    @Test
    public void valueFromBodyUsingXpath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("<ns2:element>value</ns2:element>")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromBodyUsingXpath(//*[local-name()=\\\'element\\\'])");
        assertEquals("value", result);
    }
}