package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsPathCondition;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.condition.BaseConditionExecutorTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsPathConditionExecutorTest extends BaseConditionExecutorTest {

    @Test
    public void standard() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withRequestUri("/service/company/10/customer/all")
                .build());

        JsPathCondition condition = new JsPathCondition();
        condition.setPath("/company/10/customer/all");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void regexp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withRequestUri("/service/company/10/customer/all")
                .build());

        JsPathCondition condition = new JsPathCondition();
        condition.setPath("/company.*");
        condition.setRegexp(true);

        assertTrue(executeCondition(context, condition));
    }
}