package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsQueryCondition;
import nl.sodeso.webservice.mock.model.generic.JsQuery;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.condition.BaseConditionExecutorTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsQueryConditionExecutorTest extends BaseConditionExecutorTest {

    @Test
    public void standard() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withQuery("query", "value")
                .build());

        JsQueryCondition condition = new JsQueryCondition();
        condition.addQuery(new JsQuery("query", "value"));

        assertTrue(executeCondition(context, condition));
    }
}