package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class CurrentTimeFunctionExecutorTest {

    @org.junit.Test
    public void currentTime() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_currentTime('dd-MM-yyyy')");
        String expectedResult = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        assertEquals(result, expectedResult);
    }

    @org.junit.Test
    public void millisToTime() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_millisToTime(" + System.currentTimeMillis() + ", dd-MM-yyyy)");
        String expectedResult = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        assertEquals(result, expectedResult);
    }

    @org.junit.Test
    public void millisToTimeWithFunctions() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_millisToTime(_timeToMillis(_currentTime(dd-MM-yyyy), dd-MM-yyyy), dd-MM-yyyy)");
        String expectedResult = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        assertEquals("Result does not match expected value.", result, expectedResult);
    }

    @org.junit.Test
    public void addTime() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_addTime(01-01-2010, dd-MM-yyyy, 1, 1, 1)");
        assertEquals(result, "02-02-2011");
    }

    @org.junit.Test
    public void addTimeWithFunctions() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_addTime(_currentTime(dd-MM-yyyy), dd-MM-yyyy, 1, 1, 1)");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.roll(Calendar.DAY_OF_YEAR, 1);
        calendar.roll(Calendar.MONTH, 1);
        calendar.roll(Calendar.YEAR, 1);

        String expectedResult = new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime());
        assertEquals(result, expectedResult);
    }

    @org.junit.Test
    public void timeToMillis() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_timeToMillis(01-01-2016, dd-MM-yyyy)");
        long expectedResult = new SimpleDateFormat("dd-MM-yyyy").parse("01-01-2016").getTime();
        assertEquals(Long.parseLong(result), expectedResult);
    }

    @org.junit.Test
    public void timeToMillisWithFunctions() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_timeToMillis(_currentTime(dd-MM-yyyy), dd-MM-yyyy)");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        long expectedResult = simpleDateFormat.parse(simpleDateFormat.format(new Date())).getTime();
        assertEquals(Long.parseLong(result), expectedResult);
    }
}