package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsCookieCondition;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.condition.BaseConditionExecutorTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsCookieConditionExecutorTest extends BaseConditionExecutorTest {

    @Test
    public void standard() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withCookie(new JsCookie.Builder("mycookie", "value").build())
                .build());

        JsCookieCondition condition = new JsCookieCondition();
        condition.setName("mycookie");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void json() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withCookie(new JsCookie.Builder("mycookie", "{ \"request\": { \"reference\": \"value\" }}").build())
                .build());

        JsCookieCondition condition = new JsCookieCondition();
        condition.setName("mycookie");
        condition.setJsonpath("$.request.reference");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void regexp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withCookie(new JsCookie.Builder("mycookie", "<element>value</element>").build())
                .build());

        JsCookieCondition condition = new JsCookieCondition();
        condition.setName("mycookie");
        condition.setRegexp("<element>(.*?)</element>");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void xpath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withCookie(new JsCookie.Builder("mycookie", "<ns2:element>value</ns2:element>").build())
                .build());

        JsCookieCondition condition = new JsCookieCondition();
        condition.setName("mycookie");
        condition.setXpath("//*[local-name()='element'][text() = 'value']");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }
}