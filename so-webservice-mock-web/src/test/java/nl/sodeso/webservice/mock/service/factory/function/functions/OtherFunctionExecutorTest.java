package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class OtherFunctionExecutorTest {

    @Test
    public void random() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_random(100, 200)");
        int _value = Integer.parseInt(result);

        assertTrue(_value >= 100 && _value <= 200);
    }

    @Test
    public void randomWithFunctions() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_random(_random(100, 200), _random(201, 300))");
        int _value = Integer.parseInt(result);

        assertTrue(_value >= 100 && _value <= 300);
    }

    @org.junit.Test
    public void uuid() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_uuid()");
        assertNotNull(result);
    }

}