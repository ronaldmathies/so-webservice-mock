package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class ValueFromHeaderFunctionExecutorTest {

    @Test
    public void valueFromHeader() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("myheader", "value")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromHeader(myheader)");
        assertEquals("value", result);
    }
    @Test
    public void valueFromHeaderUsingXpath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("myheader", "<ns2:element>value</ns2:element>")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromHeaderUsingXpath(myheader, //*[local-name()=\\\'element\\\'])");
        assertEquals("value", result);
    }

    @Test
    public void valueFromHeaderUsingJsonPath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("myheader", "{ \"request\": { \"reference\": \"value\" }}")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromHeaderUsingJsonPath(myheader, $.request.reference)");
        assertEquals("value", result);
    }

    @Test
    public void valueFromHeaderUsingRegExp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("myheader", "<element>value</element>")
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromHeaderUsingRegExp(myheader, <element>(.*?)</element>)");
        assertEquals("value", result);
    }
}