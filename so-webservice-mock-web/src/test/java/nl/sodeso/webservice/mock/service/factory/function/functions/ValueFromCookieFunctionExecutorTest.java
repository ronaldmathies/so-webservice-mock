package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class ValueFromCookieFunctionExecutorTest {

    @Test
    public void valueFromCookie() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withCookie(new JsCookie.Builder("mycookie", "value").build())
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromCookie(mycookie)");
        assertEquals("value", result);
    }

    @Test
    public void valueFromCookieUsingXpath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withCookie(new JsCookie.Builder("mycookie", "<ns2:element>value</ns2:element>").build())
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromCookieUsingXpath(mycookie, //*[local-name()=\\\'element\\\'])");
        assertEquals("value", result);
    }

    @Test
    public void valueFromCookieUsingJsonPath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withCookie(new JsCookie.Builder("mycookie", "{ \"request\": { \"reference\": \"value\" }}").build())
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromCookieUsingJsonPath(mycookie, $.request.reference)");
        assertEquals("value", result);
    }

    @Test
    public void valueFromCookieUsingRegExp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withCookie(new JsCookie.Builder("mycookie", "<element>value</element>").build())
                .build());

        String result = FunctionUtil.evaluateAndExecuteFunctions(context, "_valueFromCookieUsingRegExp(mycookie, <element>(.*?)</element>)");
        assertEquals("value", result);
    }
}