package nl.sodeso.webservice.mock.service.factory.action.actions;

import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsExceptionAction;
import nl.sodeso.webservice.mock.service.factory.action.BaseActionExecutorTest;

import javax.servlet.ServletException;

import static org.junit.Assert.fail;

/**
 * @author Ronald Mathies
 */
public class JsExceptionJsActionExecutorTest extends BaseActionExecutorTest {

    @org.junit.Test
    public void standard() throws Exception {
        boolean isExceptionThrown = false;
        try {
            executeAction(null, new JsExceptionAction());
        } catch (ServletException e) {
            isExceptionThrown = true;
        }

        if (!isExceptionThrown) {
            fail("Exception is not thrown while we excepted the exception to be thrown.");
        }
    }

}