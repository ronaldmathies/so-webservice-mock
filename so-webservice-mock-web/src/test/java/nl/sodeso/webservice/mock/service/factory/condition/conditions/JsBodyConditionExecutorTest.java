package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsBodyCondition;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.condition.BaseConditionExecutorTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsBodyConditionExecutorTest extends BaseConditionExecutorTest {

    @Test
    public void standard() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("value")
                .build());

        JsBodyCondition condition = new JsBodyCondition();
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void json() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("{ \"request\": { \"reference\": \"value\" }}")
                .build());

        JsBodyCondition condition = new JsBodyCondition();
        condition.setJsonpath("$.request.reference");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void regexp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("<element>value</element>")
                .build());

        JsBodyCondition condition = new JsBodyCondition();
        condition.setRegexp("<element>(.*?)</element>");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void xpath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withBody("<ns2:element>value</ns2:element>")
                .build());

        JsBodyCondition condition = new JsBodyCondition();
        condition.setXpath("//*[local-name()='element'][text() = 'value']");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }
}