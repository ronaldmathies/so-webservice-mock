package nl.sodeso.webservice.mock.service.factory.action;

import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.ActionFactory;

/**
 * @author Ronald Mathies
 */
public class BaseActionExecutorTest {

    @SuppressWarnings("unchecked")
    protected void executeAction(Context context, JsAction jsAction) throws Exception {
        ActionExecutor executor = ActionFactory.getInstance().getAction(jsAction);
        executor.setContext(context);
        executor.execute(jsAction);
    }

}
