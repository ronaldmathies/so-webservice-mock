package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class StringFunctionExecutorTest {

    @org.junit.Test
    public void append() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_append('val\\\'ue1 ', value2)");
        assertEquals(result, "val'ue1 value2");
    }

    @org.junit.Test
    public void substring() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_substring('Hello World!', 0, 5)");
        assertEquals(result, "Hello");
    }

    @org.junit.Test
    public void indexOf() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_indexOf('Hello World!', World)");
        assertEquals(result, "6");

        result = FunctionUtil.evaluateAndExecuteFunctions(null, "_indexOf('Hello World!', X)");
        assertEquals(result, "-1");
    }

    @org.junit.Test
    public void indexOfFromIndex() throws Exception {
        String result = FunctionUtil.evaluateAndExecuteFunctions(null, "_indexOfFromIndex('ABC ABC', A, 1)");
        assertEquals(result, "3");
    }

}