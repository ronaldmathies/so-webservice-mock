package nl.sodeso.webservice.mock.service.factory.action;

import nl.sodeso.webservice.mock.service.factory.ActionFactory;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class JsActionFactoryTest {
    @Test
    public void getInstance() throws Exception {
        ActionFactory instance = ActionFactory.getInstance();
        assertNotNull(instance);
    }

}