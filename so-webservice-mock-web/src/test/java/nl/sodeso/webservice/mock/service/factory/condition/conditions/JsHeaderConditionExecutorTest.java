package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsHeaderCondition;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.condition.BaseConditionExecutorTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsHeaderConditionExecutorTest extends BaseConditionExecutorTest {

    @Test
    public void standard() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("header", "value")
                .build());

        JsHeaderCondition condition = new JsHeaderCondition();
        condition.setName("header");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void json() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("header", "{ \"request\": { \"reference\": \"value\" }}")
                .build());

        JsHeaderCondition condition = new JsHeaderCondition();
        condition.setName("header");
        condition.setJsonpath("$.request.reference");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void regexp() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("header", "<element>value</element>")
                .build());

        JsHeaderCondition condition = new JsHeaderCondition();
        condition.setName("header");
        condition.setRegexp("<element>(.*?)</element>");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }

    @Test
    public void xpath() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("GET")
                .withHeader("header", "<ns2:element>value</ns2:element>")
                .build());

        JsHeaderCondition condition = new JsHeaderCondition();
        condition.setName("header");
        condition.setXpath("//*[local-name()='element'][text() = 'value']");
        condition.setMatches("value");

        assertTrue(executeCondition(context, condition));
    }
}