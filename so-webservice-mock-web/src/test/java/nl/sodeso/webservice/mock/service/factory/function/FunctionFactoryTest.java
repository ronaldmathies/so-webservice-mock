package nl.sodeso.webservice.mock.service.factory.function;

import nl.sodeso.webservice.mock.service.factory.FunctionFactory;
import nl.sodeso.webservice.mock.service.factory.function.functions.ValueFromBodyFunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.functions.ValueFromCookieFunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.functions.ValueFromHeaderFunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.functions.OtherFunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.functions.StringFunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.functions.TimeFunctionExecutor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class FunctionFactoryTest {

    @Test
    public void getInstance() throws Exception {
        FunctionFactory functionFactory = FunctionFactory.getInstance();
        assertNotNull(functionFactory);
    }

    @Test
    public void getAddTimeFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("addTime");
        assertEquals(executor.getClass(), TimeFunctionExecutor.class);
    }

    @Test
    public void getCurrentTimeFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("currentTime");
        assertEquals(executor.getClass(), TimeFunctionExecutor.class);
    }

    @Test
    public void getMillisToTimeFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("millisToTime");
        assertEquals(executor.getClass(), TimeFunctionExecutor.class);
    }

    @Test
    public void getRandomFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("random");
        assertEquals(executor.getClass(), OtherFunctionExecutor.class);
    }

    @Test
    public void getTimeToMillisFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("timeToMillis");
        assertEquals(executor.getClass(), TimeFunctionExecutor.class);
    }

    @Test
    public void getUuidFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("uuid");
        assertEquals(executor.getClass(), OtherFunctionExecutor.class);
    }

    @Test
    public void getValueFromBodyUsingJsonPathFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromBodyUsingJsonPath");
        assertEquals(executor.getClass(), ValueFromBodyFunctionExecutor.class);
    }

    @Test
    public void getValueFromBodyUsingRegExpFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromBodyUsingRegExp");
        assertEquals(executor.getClass(), ValueFromBodyFunctionExecutor.class);
    }

    @Test
    public void getValueFromBodyUsingXpathFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromBodyUsingXpath");
        assertEquals(executor.getClass(), ValueFromBodyFunctionExecutor.class);
    }

    @Test
    public void getValueFromCookieUsingJsonPathFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromCookieUsingJsonPath");
        assertEquals(executor.getClass(), ValueFromCookieFunctionExecutor.class);
    }

    @Test
    public void getValueFromCookieUsingRegExpFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromCookieUsingRegExp");
        assertEquals(executor.getClass(), ValueFromCookieFunctionExecutor.class);
    }

    @Test
    public void getValueFromCookieUsingXpathFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromCookieUsingXpath");
        assertEquals(executor.getClass(), ValueFromCookieFunctionExecutor.class);
    }

    @Test
    public void getValueFromHeaderUsingJsonPathFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromHeaderUsingJsonPath");
        assertEquals(executor.getClass(), ValueFromHeaderFunctionExecutor.class);
    }

    @Test
    public void getValueFromHeaderUsingRegExpFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromHeaderUsingRegExp");
        assertEquals(executor.getClass(), ValueFromHeaderFunctionExecutor.class);
    }

    @Test
    public void getValueFromHeaderUsingXpathFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("valueFromHeaderUsingXpath");
        assertEquals(executor.getClass(), ValueFromHeaderFunctionExecutor.class);
    }

    @Test
    public void getAppendFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("append");
        assertEquals(executor.getClass(), StringFunctionExecutor.class);
    }

    @Test
    public void getSubstringFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("substring");
        assertEquals(executor.getClass(), StringFunctionExecutor.class);
    }

    @Test
    public void getIndexOfFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("indexOf");
        assertEquals(executor.getClass(), StringFunctionExecutor.class);
    }

    @Test
    public void getIndexOfFromIndexFunctionExecutor() throws Exception {
        FunctionExecutor executor = FunctionFactory.getInstance().getFunctionExecutor("indexOfFromIndex");
        assertEquals(executor.getClass(), StringFunctionExecutor.class);
    }



}