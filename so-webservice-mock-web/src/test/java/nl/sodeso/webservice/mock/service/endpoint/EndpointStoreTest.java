package nl.sodeso.webservice.mock.service.endpoint;

import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class EndpointStoreTest {

    @Before
    public void reset() {
        EndpointStore.getInstance().reset();
    }

    @Test
    public void testGetByPathFail() throws Exception {
        try {
            EndpointStore.getInstance().addEndpoint(new JsEndpoint("e1", "/contacts"));
            EndpointStore.getInstance().addEndpoint(new JsEndpoint("e2", "/contacts/api"));
        } catch (EndpointStoreException e) {
            assertEquals("An endpoint already exists that matches (a part of) the path '/contacts/api'.", e.getMessage());
        }
    }

    @Test
    public void testGetByPathOk() throws Exception {
        EndpointStore.getInstance().addEndpoint(new JsEndpoint("e1", "/contacts"));
        EndpointStore.getInstance().addEndpoint(new JsEndpoint("e2", "/contacts2"));
    }

}