package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Ronald Mathies
 */
public class ServletInputStreamMock extends ServletInputStream {

    private InputStream stream;

    public ServletInputStreamMock(InputStream stream) {
        this.stream = stream;
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void setReadListener(ReadListener readListener) {

    }

    @Override
    public int read() throws IOException {
        return stream.read();
    }
}
