package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsParameterCondition;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.model.generic.JsParameter;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.condition.BaseConditionExecutorTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class JsFunctionParameterConditionExecutorTest extends BaseConditionExecutorTest {

    @Test
    public void standard() throws Exception {
        Context context = new Context();
        context.setJsRequest(new JsRequest.Builder()
                .withMethod("POST")
                .withRequestUri("/service/company/10/customer/20")
                .build());

//        HttpServletRequestMock httpServletRequest = new HttpServletRequestMock();
//        httpServletRequest.setRequestURI("/service/company/10/customer/20");
//        httpServletRequest.setRequestInputStream(new ByteArrayInputStream("".getBytes()));

        JsParameterCondition condition = new JsParameterCondition();
        condition.setPath("/company/{companyId}/customer/{customerId}");
        condition.addParameter(new JsParameter("companyId", "10"));
        condition.addParameter(new JsParameter("customerId", "20"));

        assertTrue(executeCondition(context, condition));
    }
}