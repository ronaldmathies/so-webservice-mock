package nl.sodeso.webservice.mock.service.factory.action.actions;

import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsVariableAction;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsVariableValue;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsMessageResponseType;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.action.BaseActionExecutorTest;
import nl.sodeso.webservice.mock.service.variables.VariableManager;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class JsVariableJsActionExecutorTest extends BaseActionExecutorTest {

    private Context context = null;

    @Before
    public void before() {
        VariableManager.getInstance().initRequestContainer();

        context = new Context();
        JsResponse jsResponse = new JsResponse();
        JsMessageResponseType jsMessageResponse = new JsMessageResponseType();
        jsMessageResponse.setBody(
                "<a>$R{myplaceholder1}</a>" +
                "<b>$R{myplaceholder3}</b>");
        jsResponse.setJsResponseType(jsMessageResponse);
        context.setJsResponse(jsResponse);
    }

    @Test
    public void withFunctions() throws Exception {
//        JsVariableAction jsVariableAction = new JsVariableAction();
//        jsVariableAction.addVariable(new JsVariableValue("request", "var1", "_currentTime(dd-MM-yyyy)"));
//        jsVariableAction.addVariable(new JsVariableValue("global", "var3", "global_variable"));
//        jsVariableAction.addVariable(new JsVariableValue("request", "myplaceholder1", "_addTime($R{var1}, dd-MM-yyyy, 1, 1, 1)"));
//        jsVariableAction.addVariable(new JsVariableValue("request", "myplaceholder3", "$G{var3}"));
//        executeAction(context, jsVariableAction);
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(new Date());
//        calendar.roll(Calendar.DAY_OF_YEAR, 1);
//        calendar.roll(Calendar.MONTH, 1);
//        calendar.roll(Calendar.YEAR, 1);
//
//        String expectedResult = new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime());
//        assertEquals(String.format("<a>%s</a><b>global_variable</b>", expectedResult), ((JsMessageResponseType)context.getJsResponse().getJsResponseType()).getBody()); // <c>global_variable</c>
    }

}