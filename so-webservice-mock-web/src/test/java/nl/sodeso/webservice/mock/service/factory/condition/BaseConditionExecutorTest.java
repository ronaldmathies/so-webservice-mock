package nl.sodeso.webservice.mock.service.factory.condition;

import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.ConditionFactory;

/**
 * @author Ronald Mathies
 */
public class BaseConditionExecutorTest {

    @SuppressWarnings("unchecked")
    protected boolean executeCondition(Context context, JsCondition condition) throws Exception {
        context.setJsEndpoint(new JsEndpoint("endpoint", "/service"));

        ConditionExecutor executor = ConditionFactory.getInstance().getCondition(condition);
        executor.setContext(context);
        return executor.isApplicable(condition);
    }

}
