package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsParameterCondition;
import nl.sodeso.webservice.mock.model.generic.JsParameter;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.annotation.ConditionExecutorType;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ConditionExecutorType(
    condition = JsParameterCondition.class
)
public class ParameterConditionExecutor extends ConditionExecutor<JsParameterCondition> {

    public boolean isApplicable(JsParameterCondition condition) throws ConditionException {
        String[] pathParts = condition.getPath().split("/");
        String[] requestPathParts = ContextUtil.getRequestUri(getContext(), false).split("/");

        boolean matches = true;
        for (int index = 0; index < pathParts.length; index++) {
            String pathPart = pathParts[index];
            String requestPathPart = requestPathParts[index];

            if (!pathPart.startsWith("{")) {
                if (!pathPart.equals(requestPathPart)) {
                    matches = false;
                    break;
                }
            } else {
                String key = pathPart.substring(1, pathPart.length() - 1);
                JsParameter jsParameter = condition.parameterWithName(key);

                if (jsParameter != null) {

                    if (!jsParameter.getValue().equals(requestPathPart)) {
                        matches = false;
                        break;
                    }

                }
            }
        }

        return matches;
    }

}
