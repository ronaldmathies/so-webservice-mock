package nl.sodeso.webservice.mock.service.endpoint.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsCookieExpression;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutor;
import nl.sodeso.webservice.mock.service.endpoint.expression.annotations.ExpressionExecutorType;
import nl.sodeso.webservice.mock.service.util.*;

import java.util.Optional;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ExpressionExecutorType(
    name = CookieExpressionExecutor.NAME,
    expression = JsCookieExpression.class
)
public class CookieExpressionExecutor extends ExpressionExecutor<JsCookieExpression> {

    protected static final String NAME = "cookie";

    public String value(JsCookieExpression expression, JsRequest jsRequest) throws ExpressionException {
        try {
            String valueToMatch = "";

            Optional<JsCookie> jsCookie = jsRequest.getJsCookie(expression.getName());
            if (jsCookie.isPresent()) {
                if (expression.getJsonpath() != null) {
                    valueToMatch = JSonUtil.valueFromString(expression.getJsonpath(), jsCookie.get().getValue());
                }

                if (expression.getRegexp() != null) {
                    valueToMatch = RegExpUtil.valueFromString(expression.getRegexp(), jsCookie.get().getValue());
                }

                if (expression.getXpath() != null) {
                    valueToMatch = XPathUtil.valueFromString(expression.getXpath(), jsCookie.get().getValue());
                }
            }

            return valueToMatch;
        } catch (XPathUtilException | RegExpUtilException | JSonUtilException e) {
            throw new ExpressionException(e.getMessage());
        }
    }

}
