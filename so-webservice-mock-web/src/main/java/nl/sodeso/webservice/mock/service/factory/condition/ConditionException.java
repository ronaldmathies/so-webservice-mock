package nl.sodeso.webservice.mock.service.factory.condition;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class ConditionException extends ServletException {

    public ConditionException(String message, Object ... args) {
        super(String.format(message, args));
    }
}