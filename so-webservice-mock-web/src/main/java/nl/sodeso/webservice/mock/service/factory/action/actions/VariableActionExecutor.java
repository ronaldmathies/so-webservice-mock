package nl.sodeso.webservice.mock.service.factory.action.actions;

import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsVariableAction;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsVariableValue;
import nl.sodeso.webservice.mock.service.factory.action.ActionExecutor;
import nl.sodeso.webservice.mock.service.factory.action.annotation.ActionExecutorType;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import nl.sodeso.webservice.mock.service.variables.Scope;
import nl.sodeso.webservice.mock.service.variables.VariableManager;

import javax.servlet.ServletException;

/**
 * This action will update any placeholder in the body that are marked with a dollar sign and braces on both sides ( ${placeholder} )
 * with the value specified in the arguments. The key of the argument is the name of the placeholder the value is the
 * value that will update the placeholder.
 *
 * @author Ronald Mathies
 */
@ActionExecutorType(
    action = JsVariableAction.class
)
public class VariableActionExecutor extends ActionExecutor<JsVariableAction> {

    public void execute(JsVariableAction action) throws ServletException {
        for (JsVariableValue variable : action.getVariables()) {
            String result = FunctionUtil.evaluateAndExecuteFunctions(getContext(), variable.getValue());
            VariableManager.getInstance().getActiveVariableContainerByScope(Scope.getScope(variable.getScope()))
                    .setValue(variable.getName(), result);
        }
    }

}
