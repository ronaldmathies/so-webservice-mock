package nl.sodeso.webservice.mock.service.factory.response.responses;

import nl.sodeso.webservice.mock.model.endpoint.response.type.JsRedirectResponseType;
import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import nl.sodeso.webservice.mock.service.factory.response.ResponseException;
import nl.sodeso.webservice.mock.service.factory.response.ResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.annotation.ResponseExecutorType;

import java.io.IOException;

/**
 * @author Ronald Mathies
 */
@ResponseExecutorType(
    response = JsRedirectResponseType.class
)
public class RedirectResponseExecutor extends ResponseExecutor<JsRedirectResponseType> {

    @Override
    public void execute(JsRedirectResponseType response) throws ResponseException {
        addHeaders(response.getJsHeaders(), getContext().getServletResponse());
        addCookies(response.getJsCookies(), getContext().getServletResponse());

        getContext().getServletResponse().setStatus(response.getHttpStatus());

        try {
            getContext().getServletResponse().sendRedirect(FunctionUtil.evaluateAndExecuteFunctions(getContext(), response.getUrl()));
        } catch (IOException | FunctionException e) {
            throw new ResponseException("Failed to send redirect.");
        }
    }
}
