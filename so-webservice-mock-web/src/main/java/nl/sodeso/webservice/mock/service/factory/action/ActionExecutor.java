package nl.sodeso.webservice.mock.service.factory.action;

import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;
import nl.sodeso.webservice.mock.service.Executor;

import javax.servlet.ServletException;

/**
 * The JsAction interface.
 *
 * @author Ronald Mathies
 */
public abstract class ActionExecutor<A extends JsAction> extends Executor {

    /**
     * Executes the given action agains the context.
     *
     * @param action the action.
     *
     * @throws ServletException Thrown when a problem occurred during the execution.
     */
    public abstract void execute(A action) throws ServletException;

}
