package nl.sodeso.webservice.mock.service.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
public class RegExpUtil {

    public static String valueFromString(String expression, String value) throws RegExpUtilException {
        try {
            Matcher matcher = Pattern.compile(expression).matcher(value);
            if (matcher.find()) {
                if (matcher.groupCount() > 0) {
                    return matcher.group(1);
                }
            }
        } catch (Exception e) {
            throw new RegExpUtilException(e.getMessage());
        }

        return null;
    }

    public static boolean matches(String expression, String value) throws RegExpUtilException {
        return value.matches(expression);
    }
}