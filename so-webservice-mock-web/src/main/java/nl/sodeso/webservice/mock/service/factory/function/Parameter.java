package nl.sodeso.webservice.mock.service.factory.function;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Ronald Mathies
 */
public class Parameter {

    private String name;
    private String value;

    public Parameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public Integer toInteger() throws FunctionException {
        try {
            return new Integer(value);
        } catch (NumberFormatException e) {
            throw new FunctionException("Cannot parse string value '%s' to an integer.", value);
        }
    }

    public Float toFloat() throws FunctionException {
        try {
            return new Float(value);
        } catch (NumberFormatException e) {
            throw new FunctionException("Cannot parse string value '%s' to a float.", value);
        }
    }

    public String toString() {
        return value;
    }

    public Double toDouble() throws FunctionException {
        try {
            return new Double(value);
        } catch (NumberFormatException e) {
            throw new FunctionException("Cannot parse string value '%s' to a double.", value);
        }
    }
    public Long toLong() throws FunctionException {
        try {
            return new Long(value);
        } catch (NumberFormatException e) {
            throw new FunctionException("Cannot parse string value '%s' to a long.", value);
        }
    }

    public Date toDate(String format) throws FunctionException {
        try {
            return new SimpleDateFormat(format).parse(value);
        } catch (ParseException e) {
            throw new FunctionException("Unable to format date '%s' with format '%s'", value, format);
        }
    }

}
