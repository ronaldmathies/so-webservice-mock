package nl.sodeso.webservice.mock.service.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author Ronald Mathies
 */
public class RequestUtil {

    private static final String UTF_8 = "UTF-8";

    public static String getBody(HttpServletRequest servletRequest) throws IOException {
        return new Scanner(servletRequest.getInputStream(), UTF_8).useDelimiter("\\A").next();
    }

    public static String getCookieValue(HttpServletRequest servletRequest, String name) {
        for (Cookie cookie : servletRequest.getCookies()) {
            if (cookie.getName().equals(name)) {
                return cookie.getValue();
            }
        }

        return null;
    }
}
