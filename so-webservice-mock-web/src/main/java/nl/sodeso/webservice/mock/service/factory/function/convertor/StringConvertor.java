package nl.sodeso.webservice.mock.service.factory.function.convertor;

/**
 * @author Ronald Mathies
 */
public class StringConvertor implements Convertor<String> {

    public Class<String> getType() {
        return String.class;
    }

    @Override
    public String from(String value) throws ConvertorException {
        return value;
    }

}
