package nl.sodeso.webservice.mock.service.factory.action.actions;

import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsExceptionAction;
import nl.sodeso.webservice.mock.service.factory.action.ActionExecutor;
import nl.sodeso.webservice.mock.service.factory.action.annotation.ActionExecutorType;

import javax.servlet.ServletException;

/**
 * This action will throw an Exception to create a simulation of a 500 Internal Server Error.
 *
 * @author Ronald Mathies
 */
@ActionExecutorType(
    action = JsExceptionAction.class
)
public class ExceptionActionExecutor extends ActionExecutor<JsExceptionAction> {

    /**
     * {@inheritDoc}
     */
    public void execute(JsExceptionAction action) throws ServletException {
        throw new ServletException("Generate internal server error.");
    }

}
