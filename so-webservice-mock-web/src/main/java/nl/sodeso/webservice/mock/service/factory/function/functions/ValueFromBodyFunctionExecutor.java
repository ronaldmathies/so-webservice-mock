package nl.sodeso.webservice.mock.service.factory.function.functions;

import com.jayway.jsonpath.JsonPath;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.*;
import nl.sodeso.webservice.mock.service.factory.function.convertor.RegExpConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.StringConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.XpathConvertor;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
@FunctionsType (
    functions = {
        @FunctionType(
            name = "valueFromBodyUsingJsonPath",
            description = "valueFromBodyUsingJsonPath( jsonpath )",
            group = "Body",
            format = "_valueFromBodyUsingJsonPath( 'jsonpath' )",
            documentation = "Resolves the value by parsing the body using the JSON path, this would require the body to contain JSON data.",
            parameters = {
                @ParameterType(name = "jsonpath", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "jsonpath", value = "$.request.reference")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromBodyUsingRegExp",
            description = "valueFromBodyUsingRegExp( regexp )",
            group = "Body",
            format = "_valueFromBodyUsingRegExp( 'regexp' )",
            documentation = "Resolves the value by parsing the body using the regulair expression.",
            parameters = {
                @ParameterType(name = "regexp", type = RegExpConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "regexp", value = "<reference>(.*?)</reference>")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromBodyUsingXpath",
            description = "valueFromBodyUsingXpath( xpath )",
            group = "Body",
            format = "_valueFromBodyUsingXpath( 'xpath' )",
            documentation = "Resolves the value by parsing the body using the XPath expression, this would require the body to contain XML data.",
            parameters = {
                @ParameterType(name = "xpath", type = XpathConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "xpath", value = "//*[local-name()=\\'element\\']")
                    }
                )
            }
        )
    }
)
public class ValueFromBodyFunctionExecutor extends FunctionExecutor {

    public String valueFromBodyUsingJsonPath(String jsonpath) throws FunctionException {
        return JsonPath.read(getContext().getJsRequest().getBody(), jsonpath);
    }

    public String valueFromBodyUsingRegExp(Pattern pattern) throws FunctionException {
        Matcher matcher = pattern.matcher(getContext().getJsRequest().getBody());
        if (matcher.find()) {
            if (matcher.groupCount() > 0) {
                return matcher.group(1);
            }
        }

        return "";
    }

    public String valueFromBodyUsingXpath(XPathExpression xpath) throws FunctionException {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.parse(
                    new InputSource(
                            new StringReader(getContext().getJsRequest().getBody())));

            document.getDocumentElement().normalize();

            return (String) xpath.evaluate(document, XPathConstants.STRING);
        } catch (ParserConfigurationException | XPathExpressionException | SAXException | IOException e) {
            throw new FunctionException("Failed to parse the body due to an error '%s'.", e.getMessage());
        }
    }

}
