package nl.sodeso.webservice.mock.service.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.service.endpoint.EndpointStore;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 * @author Ronald Mathies
 */
@WebServlet(
    name = "Functions Demo Data Loader",
    loadOnStartup = 1
)
public class FunctionsDemoLoader extends HttpServlet {

    @Override
    public void init() throws ServletException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsEndpoint jsEndpoint = mapper.readValue(getClass().getResourceAsStream("/demo/functions/endpoint.json"), JsEndpoint.class);
            EndpointStore.getInstance().addEndpoint(jsEndpoint);
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/functions/value_from_header.json"), JsResponse.class));
        } catch (Exception e) {
            System.out.println("Yoho: " + e.getMessage());
        }
    }

}
