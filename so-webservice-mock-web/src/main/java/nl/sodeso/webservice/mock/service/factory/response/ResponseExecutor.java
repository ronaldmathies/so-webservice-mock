package nl.sodeso.webservice.mock.service.factory.response;

import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.model.generic.JsHeader;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsResponseType;
import nl.sodeso.webservice.mock.service.Executor;
import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;
import nl.sodeso.commons.general.ExceptionUtil;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpCookie;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public abstract class ResponseExecutor<R extends JsResponseType> extends Executor {

    /**
     * @throws ServletException Thrown when a problem occurred during the execution.
     */
    public abstract void execute(R response) throws ResponseException;

    protected void addBody(HttpServletResponse servletResponse, InputStream inputStream) throws ResponseException {
        try (ServletOutputStream outputStream = servletResponse.getOutputStream()) {
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException e) {
            throw new ResponseException("Failed to parse body of the response.");
        }
    }

    protected void addBody(HttpServletResponse servletResponse, String body) throws ResponseException {
        try (ServletOutputStream outputStream = servletResponse.getOutputStream()) {

            if (body != null) {
                outputStream.write(body.getBytes(Charset.forName("UTF-8")));
            }

        } catch (IOException e) {
            throw new ResponseException("Failed to parse body of the response.");
        }
    }

    protected void addHeaders(List<JsHeader> jsHeaders, HttpServletResponse servletResponse) throws ResponseException {
        if (jsHeaders != null) {
            try {
                jsHeaders.forEach(ExceptionUtil.rethrowConsumer(jsHeader -> servletResponse.addHeader(jsHeader.getName(), FunctionUtil.evaluateAndExecuteFunctions(getContext(), jsHeader.getValue()))));
            } catch (FunctionException e) {
                throw new ResponseException(e.getMessage());
            }
        }
    }

    protected void addCookies(List<JsCookie> jsCookies, HttpServletResponse servletResponse) throws ResponseException {
        if (jsCookies != null) {
            for (JsCookie jsCookie : jsCookies) {

                try {
                    Cookie httpCookie = new Cookie(jsCookie.getName(), jsCookie.getValue());

                    if (jsCookie.getAction().equals("create")) {
                        if (jsCookie.hasDomain()) {
                            httpCookie.setDomain(jsCookie.getDomain());
                        }

                        if (jsCookie.hasPath()) {
                            httpCookie.setPath(jsCookie.getPath());
                        }

                        if (jsCookie.hasVersion()) {
                            httpCookie.setVersion(Integer.parseInt(jsCookie.getVersion()));
                        }

                        if (jsCookie.hasMaxAge()) {
                            httpCookie.setMaxAge(Integer.parseInt(jsCookie.getMaxAge()));
                        }

                        if (jsCookie.hasComment()) {
                            httpCookie.setComment(jsCookie.getComment());
                        }

                        httpCookie.setSecure(jsCookie.isSecure());
                        httpCookie.setHttpOnly(jsCookie.isHttpOnly());

                        httpCookie.setValue(FunctionUtil.evaluateAndExecuteFunctions(getContext(), jsCookie.getValue()));
                    } else if (jsCookie.getAction().equals("delete")) {
                        httpCookie.setMaxAge(0);
                    }

                    servletResponse.addCookie(httpCookie);
                } catch (FunctionException e) {
                    throw new ResponseException("Failed to evaluate value of cookie for functions.");
                }
            }
        }
    }

}
