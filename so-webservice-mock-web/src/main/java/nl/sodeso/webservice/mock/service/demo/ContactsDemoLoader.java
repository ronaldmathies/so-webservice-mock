package nl.sodeso.webservice.mock.service.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.service.endpoint.EndpointStore;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 * @author Ronald Mathies
 */
@WebServlet(
    name = "Contacts Demo Data Loader",
    loadOnStartup = 1
)
public class ContactsDemoLoader extends HttpServlet {

    @Override
    public void init() throws ServletException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsEndpoint jsEndpoint = mapper.readValue(getClass().getResourceAsStream("/demo/contacts/endpoint.json"), JsEndpoint.class);
            EndpointStore.getInstance().addEndpoint(jsEndpoint);
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/login-contacts-service.json"), JsResponse.class));
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/get_export.json"), JsResponse.class));
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/get_all.json"), JsResponse.class));
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/get_contact_1.json"), JsResponse.class));
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/get_contact_2.json"), JsResponse.class));
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/logout-contacts-service.json"), JsResponse.class));
            EndpointStore.getInstance().addResponse(jsEndpoint.getName(),mapper.readValue(getClass().getResourceAsStream("/demo/contacts/not_logged_in.json"), JsResponse.class));
        } catch (Exception e) {
            System.out.println("Yoho: " + e.getMessage());
        }
    }

}
