package nl.sodeso.webservice.mock.service.endpoint.expression.annotations;

import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Documented
@Target({ TYPE })
@Retention(RUNTIME)
public @interface ExpressionExecutorType {

    String name();
    Class<? extends JsExpression> expression();

}