package nl.sodeso.webservice.mock.service.factory.response.responses;

import nl.sodeso.webservice.mock.model.endpoint.response.type.JsMessageResponseType;
import nl.sodeso.webservice.mock.service.factory.response.ResponseException;
import nl.sodeso.webservice.mock.service.factory.response.ResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.annotation.ResponseExecutorType;
import nl.sodeso.webservice.mock.service.variables.VariableManager;

/**
 * @author Ronald Mathies
 */
@ResponseExecutorType(
    response = JsMessageResponseType.class
)
public class MessageResponseExecutor extends ResponseExecutor<JsMessageResponseType> {

    @Override
    public void execute(JsMessageResponseType response) throws ResponseException {
        addHeaders(response.getJsHeaders(), getContext().getServletResponse());
        addCookies(response.getJsCookies(), getContext().getServletResponse());

        getContext().getServletResponse().setStatus(response.getHttpStatus());
        getContext().getServletResponse().setContentType(response.getContentType());

        addBody(getContext().getServletResponse(), VariableManager.getInstance().replaceVariablesForValues(response.getBody()));
    }

}
