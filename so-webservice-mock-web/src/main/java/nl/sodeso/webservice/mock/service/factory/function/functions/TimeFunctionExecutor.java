package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.*;
import nl.sodeso.webservice.mock.service.factory.function.convertor.IntConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.LongConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.StringConvertor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Ronald Mathies
 */
@FunctionsType(
    functions = {
        @FunctionType(
            name = "addTime",
            description = "addTime( date, format, days, months, years )",
            group = "Time",
            format = "_addTime( 'date', 'format', days, months, years )",
            documentation = "Adds a number of days, months, years to the date and returns the result.<br/><br/>The date should be expressed in the format given and will also be returned in the same format, the formatting rules can be found <a target='_blank' href='https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html'>here</a>.",
            parameters = {
                @ParameterType(name = "date", type = StringConvertor.class),
                @ParameterType(name = "format", type = StringConvertor.class),
                @ParameterType(name = "days", type = IntConvertor.class),
                @ParameterType(name = "months", type = IntConvertor.class),
                @ParameterType(name = "years", type = IntConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Add one year and five days to a fixed date",
                    parameters = {
                        @SampleParameterType(name = "date", value = "01-01-2016"),
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy"),
                        @SampleParameterType(name = "days", value = "5"),
                        @SampleParameterType(name = "months", value = "0"),
                        @SampleParameterType(name = "years", value = "1")
                    }
                ),
                @SampleType(
                    name = "Add five days to the current system date",
                    parameters = {
                        @SampleParameterType(name = "date", value = "_currentTime(dd-MM-yyyy)"),
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy"),
                        @SampleParameterType(name = "days", value = "5"),
                        @SampleParameterType(name = "months", value = "0"),
                        @SampleParameterType(name = "years", value = "0")
                    }
                )
            }
        ),
        @FunctionType(
            name = "currentTime",
            description = "currentTime( format )",
            group = "Time",
            format = "_currentTime( 'format' )",
            documentation = "Returns the current system date / time in the format specified. The formatting rules can be found <a target='_blank' href='https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html'>here</a>.",
            parameters = {
                @ParameterType(name = "format", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Current date in dd-MM-yyyy format",
                    parameters = {
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy")
                    }
                ),
                @SampleType(
                    name = "Current date / time in dd-MM-yyyy HH:mm:ss format",
                    parameters = {
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy HH:mm:ss")
                    }
                )
            }
        ),
        @FunctionType(
            name = "millisToTime",
            description = "millisToTime( millis, format )",
            group = "Time",
            format = "_millisToTime( millis, 'format' )",
            documentation = "Returns a formatted date based on the milliseconds given (the milliseconds is the the difference, measured in milliseconds, between the given time and midnight, January 1, 1970 UTC. The formatting rules can be found <a target='_blank' href='https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html'>here</a>.",
            parameters = {
                @ParameterType(name = "millis", type = LongConvertor.class),
                @ParameterType(name = "format", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Convert milliseconds to a date format",
                    parameters = {
                        @SampleParameterType(name = "millis", value = "1466767114089"),
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy")
                    }
                )
            }
        ),
        @FunctionType(
            name = "timeToMillis",
            description = "timeToMillis( 'date', 'format' )",
            group = "Time",
            format = "_timeToMillis( 'date', 'format' )",
            documentation = "Converts the given date to milliseconds (the difference, measured in milliseconds, between the date specified and midnight, January 1, 1970 UTC). The formatting rules can be found <a target='_blank' href='https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html'>here</a>.",
            parameters = {
                @ParameterType(name = "date", type = StringConvertor.class),
                @ParameterType(name = "format", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Fixed date and time to milliseconds",
                    parameters = {
                        @SampleParameterType(name = "date", value = "01-01-2016 12:20:25"),
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy HH:mm:ss")
                    }
                ),
                @SampleType(
                    name = "Current system date to milliseconds",
                    parameters = {
                        @SampleParameterType(name = "date", value = "_currentTime(dd-MM-yyyy)"),
                        @SampleParameterType(name = "format", value = "dd-MM-yyyy HH:mm:ss")
                    }
                )
            }
        )
    }
)
public class TimeFunctionExecutor extends FunctionExecutor {

    public String addTime(String date, String format, Integer days, Integer months, Integer years) throws FunctionException {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new SimpleDateFormat(format).parse(date));
            calendar.roll(Calendar.DAY_OF_YEAR, days);
            calendar.roll(Calendar.MONTH, months);
            calendar.roll(Calendar.YEAR, years);

            return new SimpleDateFormat(format).format(calendar.getTime());
        } catch (ParseException e) {
            throw new FunctionException(e.getMessage());
        }
    }

    public String currentTime(String format) throws FunctionException {
        return new SimpleDateFormat(format).format(new Date());
    }

    public String millisToTime(Long millis, String format) throws FunctionException {
        return new SimpleDateFormat(format).format(new Date(millis));
    }

    public String timeToMillis(String date, String format) throws FunctionException {
        try {
            Date _d = new SimpleDateFormat(format).parse(date);
            return String.valueOf(_d.getTime());
        } catch (ParseException e) {
            throw new FunctionException(e.getMessage());
        }
    }

}
