package nl.sodeso.webservice.mock.service.variables;

/**
 * @author Ronald Mathies
 */
public enum Scope {
    REQUEST("request"),
    SESSION("session"),
    GLOBAL("global");

    private String scope;

    Scope(String scope) {
        this.scope = scope;
    }

    public static Scope getScope(String scope) {
        for (Scope _scope : Scope.values()) {
            if (_scope.scope.equals(scope)) {
                return _scope;
            }
        }

        return null;
    }

    public String getScope() {
        return this.scope;
    }

}
