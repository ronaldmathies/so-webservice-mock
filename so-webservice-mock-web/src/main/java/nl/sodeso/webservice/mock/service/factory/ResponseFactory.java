package nl.sodeso.webservice.mock.service.factory;

import nl.sodeso.webservice.mock.model.endpoint.response.type.JsResponseType;
import nl.sodeso.webservice.mock.service.factory.action.ActionException;
import nl.sodeso.webservice.mock.service.factory.response.ResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.annotation.ResponseExecutorType;
import nl.sodeso.commons.general.AnnotationUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Factory for resolving Actions by their name. All the actions are resolved and cached when this factory is used for the
 * first time.
 *
 * @author Ronald Mathies
 */
public class ResponseFactory {

    private static final Logger log = Logger.getLogger(ResponseFactory.class.getName());

    private static final String MSG_FAILED_TO_INSTANTIATE_RESPONSE = "Failed to instantiate response '%s'";

    private static final Object lock = new Object();
    private static ResponseFactory INSTANCE = null;

    private Set<Class<? extends ResponseExecutor>> availableResponses = new HashSet<>();

    private ResponseFactory() {
        availableResponses = AnnotationUtils.findSubTypesWithAnnotation(ResponseExecutor.class, ResponseExecutorType.class);
    }

    /**
     * Returns the singleton instance of the action factory.
     *
     * @return the instance.
     */
    public static ResponseFactory getInstance() {
        if (INSTANCE == null) {
            synchronized (lock) {
                INSTANCE = new ResponseFactory();
            }
        }

        return INSTANCE;
    }

    /**
     * Returns the executor for the given action.
     *
     * @param jsResponseType the jsResponseType.
     *
     * @return the executor that can execute the given action.
     *
     * @throws ActionException thrown when there was a problemen creating the executor.
     */
    public ResponseExecutor getExecutor(JsResponseType jsResponseType) throws ActionException {
        log.info(String.format("Finding jsResponseType executor for jsResponseType '%s'", jsResponseType.getClass().getSimpleName()));

        for (Class<? extends ResponseExecutor> responseExecutor : availableResponses) {
            ResponseExecutorType responseExecutorType = responseExecutor.getAnnotation(ResponseExecutorType.class);
            if (responseExecutorType != null && responseExecutorType.response().getSimpleName().equals(jsResponseType.getClass().getSimpleName())) {

                try {
                    Constructor<? extends ResponseExecutor> constructor = responseExecutor.getConstructor();
                    return constructor.newInstance();
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new ActionException(MSG_FAILED_TO_INSTANTIATE_RESPONSE, responseExecutor.getSimpleName());
                }
            }
        }

        log.severe(String.format("JsResponseType executor for jsResponseType '%s' not found", jsResponseType.getClass().getSimpleName()));

        return null;
    }

}
