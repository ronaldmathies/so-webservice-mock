package nl.sodeso.webservice.mock.service.factory.function.convertor;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * @author Ronald Mathies
 */
public class XpathConvertor implements Convertor<XPathExpression> {

    public Class<XPathExpression> getType() {
        return XPathExpression.class;
    }

    @Override
    public XPathExpression from(String value) throws ConvertorException {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            return xPath.compile(value);
        } catch (XPathExpressionException e) {
            throw new ConvertorException("Invalid XPath expression: '%s'", e.getMessage());
        }
    }

}
