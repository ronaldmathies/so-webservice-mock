package nl.sodeso.webservice.mock.service.factory.function;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class FunctionException extends ServletException {

    public FunctionException(String message, Object ... args) {
        super(String.format(message, args));
    }
}