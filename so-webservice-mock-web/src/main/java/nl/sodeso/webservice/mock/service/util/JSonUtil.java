package nl.sodeso.webservice.mock.service.util;

import com.jayway.jsonpath.JsonPath;

/**
 * @author Ronald Mathies
 */
public class JSonUtil {

    public static String valueFromString(String path, String value) throws JSonUtilException {
        try {
            return JsonPath.read(value, path);
        } catch (Exception e) {
            throw new JSonUtilException(e.getMessage());
        }
    }

}