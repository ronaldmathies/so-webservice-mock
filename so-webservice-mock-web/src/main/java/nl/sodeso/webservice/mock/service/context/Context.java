package nl.sodeso.webservice.mock.service.context;

import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Ronald Mathies
 */
public class Context {

    private HttpServletResponse servletResponse;

    private JsEndpoint jsEndpoint;
    private JsRequest jsRequest;
    private JsResponse jsResponse;

    public Context() {
    }

    public void setJsRequest(JsRequest jsRequest) {
        this.jsRequest = jsRequest;
    }

    public JsRequest getJsRequest() {
        return this.jsRequest;
    }

    public void setServletResponse(HttpServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

    public HttpServletResponse getServletResponse() {
        return this.servletResponse;
    }

    public void setJsEndpoint(JsEndpoint jsEndpoint) {
        this.jsEndpoint = jsEndpoint;
    }

    public JsEndpoint getJsEndpoint() {
        return this.jsEndpoint;
    }

    public void setJsResponse(JsResponse jsResponse) {
        this.jsResponse = jsResponse;
    }

    public JsResponse getJsResponse() {
        return this.jsResponse;
    }

}
