package nl.sodeso.webservice.mock.service.variables;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Ronald Mathies
 */
public class VariableManager {

    public static final String COOKIE_SESSION_NAME = "WS-MOCK-SESSION";

    private static final String VAR_REQUEST_PREFIX = "$R{";
    private static final String VAR_SESSION_PREFIX = "$S{";
    private static final String VAR_GLOBAL_PREFIX = "$G{";
    private static final String VAR_SUFFIX = "}";

    private static final Object lock = new Object();
    private static VariableManager INSTANCE = null;

    private ThreadLocal<VariableContainer> request = new ThreadLocal<>();
    private ThreadLocal<VariableContainer> session = new ThreadLocal<>();

    private Map<String, VariableContainer> sessionContainers = new HashMap<>();
    private VariableContainer global = new VariableContainer();

    public static VariableManager getInstance() {
        if (INSTANCE == null) {
            synchronized (lock) {
                INSTANCE = new VariableManager();
            }
        }

        return INSTANCE;
    }

    public void initOrActivateSessionContainer(HttpServletRequest request, HttpServletResponse response) {
        String sessionId = null;
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals(COOKIE_SESSION_NAME)) {
                    sessionId = cookie.getValue();
                }
            }
        }

        if (sessionId == null) {
            sessionId = UUID.randomUUID().toString();

            Cookie cookie = new Cookie(COOKIE_SESSION_NAME, sessionId);
            cookie.setMaxAge(60 * 60); // One hour
            response.addCookie(cookie);

            this.sessionContainers.put(sessionId, new VariableContainer());
        }

        session.set(this.sessionContainers.get(sessionId));
    }

    public void initRequestContainer() {
        request.set(new VariableContainer());
    }

    public void resetSessionContainers() {
        this.sessionContainers.clear();
    }

    public VariableContainer getSessionVariableContainer(String sessionId) {
        return sessionContainers.get(sessionId);
    }

    public VariableContainer getActiveVariableContainerByScope(Scope scope) {
        VariableContainer variableContainer = null;

        switch (scope) {
            case REQUEST:
                variableContainer = request.get();
                break;
            case SESSION:
                variableContainer = session.get();
                break;
            case GLOBAL:
                variableContainer = global;
        }

        return variableContainer;
    }

    public String replaceVariablesForValues(String value) {
        VariableContainer requestVariableContainer = request.get();
        for (String variable : requestVariableContainer.getVariables().keySet()) {
            String name = VAR_REQUEST_PREFIX + variable + VAR_SUFFIX;
            value = value.replace(name, requestVariableContainer.getValue(variable));
        }

        VariableContainer sessionVariableContainer = session.get();
        for (String variable : sessionVariableContainer.getVariables().keySet()) {
            String name = VAR_SESSION_PREFIX + variable + VAR_SUFFIX;
            value = value.replace(name, sessionVariableContainer.getValue(variable));
        }

        VariableContainer globalVariableContainer = session.get();
        for (String variable : globalVariableContainer.getVariables().keySet()) {
            String name = VAR_GLOBAL_PREFIX + variable + VAR_SUFFIX;
            value = value.replace(name, globalVariableContainer.getValue(variable));
        }

        return value;
    }

    public String evaluatePotentialVariable(String potentialVariable) {
        Scope scope = null;
        if (potentialVariable.startsWith(VAR_REQUEST_PREFIX) && potentialVariable.endsWith(VAR_SUFFIX)) {
            scope = Scope.REQUEST;
        } else if (potentialVariable.startsWith(VAR_SESSION_PREFIX) && potentialVariable.endsWith(VAR_SUFFIX)) {
            scope = Scope.SESSION;
        } else if (potentialVariable.startsWith(VAR_GLOBAL_PREFIX) && potentialVariable.endsWith(VAR_SUFFIX)) {
            scope = Scope.GLOBAL;
        }

        if (scope != null) {
            return getActiveVariableContainerByScope(scope)
                    .getValue(potentialVariable.substring(3, potentialVariable.length() - 1));
        }

        return potentialVariable;
    }

}
