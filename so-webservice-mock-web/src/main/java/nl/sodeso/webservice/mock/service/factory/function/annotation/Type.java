package nl.sodeso.webservice.mock.service.factory.function.annotation;

import java.util.Date;

/**
 * @author Ronald Mathies
 */
public enum Type {

    Int(Integer.class),
    Double(Integer.class),
    Float(Float.class),
    Long(Long.class),
    String(String.class),
    Date(Date.class);

    Class<?> type = null;

    Type(Class<?> type) {
        this.type = type;
    }

    public Class<?> getType() {
        return type;
    }

}
