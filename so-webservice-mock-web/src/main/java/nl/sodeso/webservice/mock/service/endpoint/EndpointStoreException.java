package nl.sodeso.webservice.mock.service.endpoint;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class EndpointStoreException extends ServletException {

    public EndpointStoreException(String message, Object ... args) {
        super(String.format(message, args));
    }
}