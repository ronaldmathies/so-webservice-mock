package nl.sodeso.webservice.mock.service;

import nl.sodeso.commons.general.Base64Util;
import nl.sodeso.commons.restful.validation.range.DoubleRangeConstraint;
import nl.sodeso.commons.storage.FileEntry;
import nl.sodeso.commons.storage.StorageController;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.function.JsFunction;
import nl.sodeso.webservice.mock.model.function.JsFunctionParameter;
import nl.sodeso.webservice.mock.model.function.JsSample;
import nl.sodeso.webservice.mock.model.function.JsSampleParameter;
import nl.sodeso.webservice.mock.model.generic.JsStatus;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsVariableValue;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsDownloadResponseType;
import nl.sodeso.webservice.mock.model.service.JsService;
import nl.sodeso.webservice.mock.service.endpoint.EndpointStoreException;
import nl.sodeso.webservice.mock.service.endpoint.EndpointStore;
import nl.sodeso.webservice.mock.service.factory.FunctionFactory;
import nl.sodeso.webservice.mock.service.factory.function.annotation.ParameterType;
import nl.sodeso.webservice.mock.service.factory.function.annotation.SampleParameterType;
import nl.sodeso.webservice.mock.service.factory.function.annotation.SampleType;
import nl.sodeso.webservice.mock.service.variables.Scope;
import nl.sodeso.webservice.mock.service.variables.VariableContainer;
import nl.sodeso.webservice.mock.service.variables.VariableManager;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

/**
 * JSON service for controling the mock service.
 *
 * @author Ronald Mathies
 */
@Path("/")
public class MockControlService implements MockControlServiceInterface {
    private static final Logger log = Logger.getLogger(MockControlService.class.getName());

    private static final String MSG_RESET_SUCCESS = "All responses and endpoints have been deleted.";
    private static final String MSG_RESPONSE_ADDED = "Response has been added.";
    private static final String MSG_RESPONSE_MODIFIED = "Response has been modified.";
    private static final String MSG_ENDPOINT_ADDED = "Endpoint has been added.";
    private static final String MSG_ENDPOINT_UPDATED = "Endpoint has been updated.";
    private static final String MSG_RESPONSE_DELETED = "JsResponseType has been deleted.";
    private static final String MSG_RESPONSES_DELETED = "All responses have been deleted.";
    private static final String MSG_ENDPOINT_DELETED = "Endpoint has been deleted.";
    private static final String MSG_RESPONSE_NOT_FOUND = "No response found for the given endpoint / id combination.";
    private static final String MSG_REQUEST_NOT_FOUND = "No request found for the given endpoint / response / id combination.";
    private static final String MSG_RESPONSE_NOT_OF_TYPE_DOWNLOAD = "The response that was found for the given endpoint / id combination is not a download response type.";
    private static final String MSG_ENDPOINT_NOT_FOUND = "No endpoint found with the given name.";
    private static final String MSG_STORE_DOESNT_EXIST = "Store does not exist.";
    private static final String MSG_VAR_ALL_RESET_SUCCESS = "All variables in all scopes have been reset.";
    private static final String MSG_VAR_SESSION_RESET_SUCCESS = "All session variables have been reset.";
    private static final String MSG_VAR_GLOBAL_RESET_SUCCESS = "All global variables have been reset.";

    public MockControlService() {}

    public Response GetApi() {
        return status(OK)
            .header("Content-Disposition", "attachement; filename=\"api.json\"")
            .entity(getClass().getResourceAsStream("/openapi.json"))
            .build();
    }

    @Override
    public Response GetVersion() {
        return ok(new JsService("NAME", "VERSION")).build();
    }

    @Override
    public Response PostEndpoint(JsEndpoint jsEndpoint) {
        try {
            EndpointStore.getInstance().addEndpoint(jsEndpoint);
            return ok(new JsStatus(JsStatus.OK, MSG_ENDPOINT_ADDED)).build();
        } catch (EndpointStoreException e) {
            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, e.getMessage())).build();
        }
    }

    @Override
    public Response PutEndpoint(String endpointName, JsEndpoint jsEndpoint) {
        try {
            EndpointStore.getInstance().updateEndpoint(endpointName, jsEndpoint);
            return ok(new JsStatus(JsStatus.OK, MSG_ENDPOINT_UPDATED)).build();
        } catch (EndpointStoreException e) {
            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, e.getMessage())).build();
        }
    }

    @Override
    public Response DeleteEndpoint(String endpointName) {
        EndpointStore.getInstance().removeEndpoint(endpointName);
        return ok(new JsStatus(JsStatus.OK, MSG_ENDPOINT_DELETED)).build();
    }

    @Override
    public List<JsEndpoint> GetEndpoints() {
        return EndpointStore.getInstance().allEndpoints();
    }

    @Override
    public Response GetEndpoint(String endpointName, String endpointPath) {
        Optional<JsEndpoint> jsEndpoint = Optional.empty();
        if (endpointName != null) {
            jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        } else if (endpointPath != null) {
            jsEndpoint = EndpointStore.getInstance().getEndpointByPath(endpointPath);
        }

        if (jsEndpoint.isPresent()) {
            return ok(jsEndpoint.get()).build();
        } else {
            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_ENDPOINT_NOT_FOUND)).build();
        }
    }

    @Override
    public Response GetExportEndpoint(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName) {
        return GetEndpoint(endpointName, null);
    }

    @Override
    public Response GetExportEndpointWithRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName) {
        return GetEndpoint(endpointName, null);
    }

    @Override
    public Response GetExportEndpointWithResponses(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName) {
        return GetEndpoint(endpointName, null);
    }

    @Override
    public Response GetExportEndpointWithResponsesAndRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName) {
        return GetEndpoint(endpointName, null);
    }

    @Override
    public Response GetExportResponse(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName) {
        return GetResponse(endpointName, responseName);
    }

    @Override
    public Response GetExportResponseWithRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName) {
        return GetResponse(endpointName, responseName);
    }

    @Override
    public Response PostResponse(String endpointName, JsResponse jsResponse) {
        try {
            EndpointStore.getInstance().addResponse(endpointName, jsResponse);
            return ok(new JsStatus(JsStatus.OK, MSG_RESPONSE_ADDED)).build();
        } catch (EndpointStoreException e) {
            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, e.getMessage())).build();
        }
    }

    @Override
    public Response PutResponse(String endpointName, String responseName, JsResponse jsResponse) {
        try {
            EndpointStore.getInstance().updateResponse(endpointName, responseName, jsResponse);
            return ok(new JsStatus(JsStatus.OK, MSG_RESPONSE_MODIFIED)).build();
        } catch (EndpointStoreException e) {
            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, e.getMessage())).build();
        }
    }

    @Override
    public Response GetResponse(String endpointName, String responseName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> responseByName = jsEndpoint.get().getJsResponseByName(responseName);
            if (responseByName.isPresent()) {

                JsResponse jsResponse = responseByName.get();
                if (jsResponse.getJsResponseType() instanceof JsDownloadResponseType) {
                    JsDownloadResponseType jsDownloadResponseType = (JsDownloadResponseType)jsResponse.getJsResponseType();

                    try {
                        Optional<FileEntry> fileEntry = StorageController.getInstance().get(jsDownloadResponseType.getFileUuid());
                        if (fileEntry.isPresent() && fileEntry.get().getFile().exists()) {
                            StringWriter base64EncodedFileContents = Base64Util.encode(new FileInputStream(fileEntry.get().getFile()));
                            jsDownloadResponseType.setData(base64EncodedFileContents.toString());
                            jsDownloadResponseType.setSize(fileEntry.get().getFile().length());
                        }
                    } catch (FileNotFoundException e) {
                        // Should not occure.
                    }
                }

                return ok(responseByName.get()).build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_RESPONSE_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_STORE_DOESNT_EXIST)).build();
    }

    @Override
    public Response DeleteResponse(String endpointName, String responseName) {
        try {
            EndpointStore.getInstance().removeResponse(endpointName, responseName);
            return ok(new JsStatus(JsStatus.OK, MSG_RESPONSE_DELETED)).build();
        } catch (EndpointStoreException e) {
            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, e.getMessage())).build();
        }
    }

    @Override
    public Response GetResponses(String endpointName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            return ok(jsEndpoint.get().getJsResponses()).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_STORE_DOESNT_EXIST)).build();
    }

    @Override
    public Response DeleteResponses(String endpointName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            jsEndpoint.get().clearJsResponses();;
            return ok(new JsStatus(JsStatus.OK, MSG_RESPONSES_DELETED)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_STORE_DOESNT_EXIST)).build();
    }

    @Override
    public Response all() {
        EndpointStore.getInstance().reset();

        return ok(new JsStatus(JsStatus.OK, MSG_RESET_SUCCESS)).build();
    }

    @Override
    public Response GetRequest(String endpointName, String id) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsRequest> jsRequest = jsEndpoint.get().getRequestById(id);
            if (jsRequest.isPresent()) {
                return ok(jsRequest.get()).build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_REQUEST_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_ENDPOINT_NOT_FOUND)).build();
    }

    @Override
    public Response GetRequests(String endpointName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            return ok(jsEndpoint.get().getJsRequests()).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_ENDPOINT_NOT_FOUND)).build();
    }

    @Override
    public Response GetRequest(String endpointName, String responseName, String id) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> jsResponse = jsEndpoint.get().getJsResponseByName(responseName);
            if (jsResponse.isPresent()) {

                Optional<JsRequest> jsRequest = jsResponse.get().getJsRequestById(id);
                if (jsRequest.isPresent()) {
                    return ok(jsRequest.get()).build();

                }

                return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_REQUEST_NOT_FOUND)).build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_RESPONSE_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_STORE_DOESNT_EXIST)).build();
    }

    @Override
    public Response GetRequests(String endpointName, String responseName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {

            Optional<JsResponse> jsResponse = jsEndpoint.get().getJsResponseByName(responseName);
            if (jsResponse.isPresent()) {
                return ok(jsResponse.get().getJsRequests()).build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_RESPONSE_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_ENDPOINT_NOT_FOUND)).build();
    }

    @Override
    public Response DeleteRequest(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_ID) String id) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {

            if (jsEndpoint.get().removeJsRequest(id)) {
                return ok().build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_REQUEST_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_ENDPOINT_NOT_FOUND)).build();
    }

    @Override
    public Response DeleteRequestsAll(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            jsEndpoint.get().clearJsRequests();
            return ok().build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_ENDPOINT_NOT_FOUND)).build();
    }

    @Override
    public Response DeleteRequest(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName, @NotNull @PathParam(PARAM_ID) String id) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> jsResponse = jsEndpoint.get().getJsResponseByName(responseName);
            if (jsResponse.isPresent()) {

                if (jsResponse.get().removeJsRequest(id)) {
                    return ok().build();
                }

                return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_REQUEST_NOT_FOUND)).build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_RESPONSE_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_STORE_DOESNT_EXIST)).build();
    }

    @Override
    public Response DeleteRequestsAll(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName) {
        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> jsResponse = jsEndpoint.get().getJsResponseByName(responseName);
            if (jsResponse.isPresent()) {

                jsResponse.get().clearJsRequests();
                return ok().build();
            }

            return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_RESPONSE_NOT_FOUND)).build();
        }

        return status(BAD_REQUEST).entity(new JsStatus(JsStatus.ERR, MSG_STORE_DOESNT_EXIST)).build();
    }

    @Override
    public Response GetVariablesAll() {
        List<JsVariableValue> jsVariableValues = new ArrayList<>();
        fillWithVariables(Scope.GLOBAL, VariableManager.getInstance().getActiveVariableContainerByScope(Scope.GLOBAL), jsVariableValues);
//        fillWithVariables(Scope.SESSION, jsVariableValues);
        return ok(jsVariableValues).build();
    }

    @Override
    public Response GetVariablesSession(String sessionId) {
        List<JsVariableValue> jsVariableValues = new ArrayList<>();

        VariableContainer sessionVariableContainer = VariableManager.getInstance().getSessionVariableContainer(sessionId);
        if (sessionVariableContainer != null) {
            fillWithVariables(Scope.SESSION, sessionVariableContainer, jsVariableValues);
        }

        return ok(jsVariableValues).build();
    }

    @Override
    public Response GetVariablesGlobal() {
        List<JsVariableValue> jsVariableValues = new ArrayList<>();
        fillWithVariables(Scope.GLOBAL, VariableManager.getInstance().getActiveVariableContainerByScope(Scope.GLOBAL), jsVariableValues);
        return ok(jsVariableValues).build();
    }

    @Override
    public Response GetVariablesAllReset() {
        VariableManager.getInstance().getActiveVariableContainerByScope(Scope.GLOBAL).reset();
        VariableManager.getInstance().resetSessionContainers();

        return ok(new JsStatus(JsStatus.OK, MSG_VAR_ALL_RESET_SUCCESS)).build();
    }

    @Override
    public Response GetVariablesSessionReset(String sessionId) {
        VariableManager.getInstance().getSessionVariableContainer(sessionId).reset();
        return ok(new JsStatus(JsStatus.OK, MSG_VAR_SESSION_RESET_SUCCESS)).build();
    }

    @Override
    public Response GetVariablesGlobalReset() {
        VariableManager.getInstance().getActiveVariableContainerByScope(Scope.GLOBAL).reset();
        return ok(new JsStatus(JsStatus.OK, MSG_VAR_GLOBAL_RESET_SUCCESS)).build();
    }

    private void fillWithVariables(Scope scope, VariableContainer variableContainer, List<JsVariableValue> jsVariableValues) {
        variableContainer.getVariables()
            .entrySet()
            .forEach(entry -> jsVariableValues.add(new JsVariableValue(scope.getScope(), entry.getKey(), entry.getValue())));    }

    @Override
    public Response GetGroups() {
        return ok(FunctionFactory.getInstance().getFunctionGroups()).build();
    }

    @Override
    public Response GetGroupFunctions(String group) {
        ArrayList<JsFunction> jsFunctions = FunctionFactory.getInstance().getFunctions(group).stream().map(functionType -> {
            JsFunction jsFunction = new JsFunction(functionType.name(), functionType.description(), functionType.documentation(), functionType.format());

            for (ParameterType parameterType : functionType.parameters()) {
                jsFunction.addJsParameter(new JsFunctionParameter(parameterType.name(), "string"));
            }

            for (SampleType sampleType : functionType.samples()) {
                JsSample jsSample = new JsSample(sampleType.name());

                for (SampleParameterType sampleParameterType : sampleType.parameters()) {
                    jsSample.addJsSampleParameter(new JsSampleParameter(sampleParameterType.name(), sampleParameterType.value()));
                }

                jsFunction.addJsSample(jsSample);
            }

            return jsFunction;
        }).collect(Collectors.toCollection(ArrayList::new));

        return ok(jsFunctions).build();
    }
}