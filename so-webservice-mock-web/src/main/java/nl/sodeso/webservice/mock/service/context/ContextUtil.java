package nl.sodeso.webservice.mock.service.context;

/**
 * @author Ronald Mathies
 */
public class ContextUtil {

    public static String getRequestUri(Context context, boolean includeEndpointPath) {
        String requestURI = context.getJsRequest().getRequestUri();

        if (!includeEndpointPath) {
            boolean excludeSlash = context.getJsEndpoint().getPath().endsWith("/");
            return requestURI.substring(context.getJsEndpoint().getPath().length() + (excludeSlash ? -1 : 0));
        }

        return requestURI;
    }

}
