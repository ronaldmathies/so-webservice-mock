package nl.sodeso.webservice.mock.service.factory.function.convertor;

/**
 * @author Ronald Mathies
 */
public class LongConvertor implements Convertor<Long> {

    public Class<Long> getType() {
        return Long.class;
    }

    @Override
    public Long from(String value) throws ConvertorException {
        return Long.parseLong(value.trim());
    }

}
