package nl.sodeso.webservice.mock.service.endpoint.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsBodyExpression;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsUuidExpression;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutor;
import nl.sodeso.webservice.mock.service.endpoint.expression.annotations.ExpressionExecutorType;
import nl.sodeso.webservice.mock.service.util.*;

import java.util.UUID;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ExpressionExecutorType(
    name = UuidExpressionExecutor.NAME,
    expression = JsUuidExpression.class
)
public class UuidExpressionExecutor extends ExpressionExecutor<JsUuidExpression> {

    // TODO: Neccesary?
    protected static final String NAME = "uuid";

    public String value(JsUuidExpression expression, JsRequest jsRequest) throws ExpressionException {
        return UUID.randomUUID().toString();
    }

}
