package nl.sodeso.webservice.mock.service.factory.response.responses;

import nl.sodeso.commons.storage.FileEntry;
import nl.sodeso.commons.storage.StorageController;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsDownloadResponseType;
import nl.sodeso.webservice.mock.service.factory.response.ResponseException;
import nl.sodeso.webservice.mock.service.factory.response.ResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.annotation.ResponseExecutorType;

import java.io.*;
import java.util.Optional;

/**
 * @author Ronald Mathies
 */
@ResponseExecutorType(
    response = JsDownloadResponseType.class
)
public class DownloadResponseExecutor extends ResponseExecutor<JsDownloadResponseType> {

    private static final String HEADER_CONTENT_DISPOSITION = "Content-Disposition";
    private static final String HEADER_ATTACHEMENT_PREFIX = "attachment; filename=";
    @Override
    public void execute(JsDownloadResponseType response) throws ResponseException {
        addHeaders(response.getJsHeaders(), getContext().getServletResponse());
        addCookies(response.getJsCookies(), getContext().getServletResponse());

        getContext().getServletResponse().setStatus(response.getHttpStatus());
        getContext().getServletResponse().setContentType(response.getContentType());
        getContext().getServletResponse()
                .setHeader(HEADER_CONTENT_DISPOSITION, HEADER_ATTACHEMENT_PREFIX.concat(String.valueOf(response.getFilename())));

        try {
            Optional<FileEntry> fileEntry = StorageController.getInstance().get(response.getFileUuid());
            if (fileEntry.isPresent() && fileEntry.get().getFile().exists()) {
                addBody(getContext().getServletResponse(), new FileInputStream(fileEntry.get().getFile()));
            }
        } catch (IOException e) {
            throw new ResponseException("Failed to load data from temporarily file on disk.");
        }
    }
}
