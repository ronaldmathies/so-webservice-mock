package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.*;
import nl.sodeso.webservice.mock.service.factory.function.convertor.IntConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.StringConvertor;

/**
 * @author Ronald Mathies
 */

/*

@FunctionType(
    name = "append",
    documentation = "",
    format = "_append(%s, %s)",
    parameters = {
        @ParameterType(name = "value1", type = StringConvertor.class),
        @ParameterType(name = "value2", type = StringConvertor.class)
    },
    samples = {
        @sample(
            name = "Combine two values",
            parameters = {
                @SampleParameter(name = "value1", value = "Hello"),
                @SampleParameter(name = "value2", value = " 'World!'")
            }
        )
    }

)
 */

@FunctionsType(
    functions = {
        @FunctionType(
            name = "append",
            description = "append(value1, value2)",
            group = "String",
            documentation = "Appends the two given values to a single value.",
            format = "_append( 'value1', 'value2' )",
            parameters = {
                @ParameterType(name = "value1", type = StringConvertor.class),
                @ParameterType(name = "value2", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Combine two values",
                    parameters = {
                        @SampleParameterType(name = "value1", value = "Hello"),
                        @SampleParameterType(name = "value2", value = " 'World!'")
                    }
                )
            }
        ),
        @FunctionType(
            name = "indexOf",
            description = "indexOf(value, valueToFind)",
            group = "String",
            documentation = "Returns the starting index (zero based) within the string for which the value was found. If no occurrence was found it will return -1.",
            format = "_indexOf( 'value', 'valuetoFind' )",
            parameters = {
                @ParameterType(name = "value", type = StringConvertor.class),
                @ParameterType(name = "valueToFind", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Find the first occurrence",
                    parameters = {
                        @SampleParameterType(name = "value", value = "Hello World!"),
                        @SampleParameterType(name = "valueToFind", value = "World!")
                    }
                )
            }
        ),
        @FunctionType(
            name = "indexOfFromIndex",
            description = "indexOfFromIndex(value, valueToFind, fromIndex)",
            group = "String",
            documentation = "Returns the starting index (zero based) within the string for which the value was found. If no occurrence was found it will return -1, the starting point for the search is the given fromIndex (zero based) value.",
            format = "_indexOfFromIndex( 'value', 'valueToFind', fromIndex )",
            parameters = {
                @ParameterType(name = "value", type = StringConvertor.class),
                @ParameterType(name = "valueToFind", type = StringConvertor.class),
                @ParameterType(name = "fromIndex", type = IntConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Find the second occurrence of 'A'",
                    parameters = {
                        @SampleParameterType(name = "value", value = "ABC ABC"),
                        @SampleParameterType(name = "valueToFind", value = "A"),
                        @SampleParameterType(name = "fromIndex", value = "1")
                    }
                )
            }
        ),
        @FunctionType(
            name = "substring",
            description = "substring(value, beginIndex, endIndex)",
            group = "String",
            documentation = "Returns a part of the given value, starting from the begin index and ending with the end index.",
            format = "_substring( 'value', beginIndex, endIndex )",
            parameters = {
                @ParameterType(name = "value", type = StringConvertor.class),
                @ParameterType(name = "beginIndex", type = IntConvertor.class),
                @ParameterType(name = "endIndex", type = IntConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Returns 'Hello' from 'Hello world!",
                    parameters = {
                        @SampleParameterType(name = "value", value = "Hello World!"),
                        @SampleParameterType(name = "beginIndex", value = "0"),
                        @SampleParameterType(name = "endIndex", value = "5")
                    }
                ),
                @SampleType(
                    name = "Returns 'World' from 'Hello world!'",
                    parameters = {
                        @SampleParameterType(name = "value", value = "Hello World!"),
                        @SampleParameterType(name = "beginIndex", value = "6"),
                        @SampleParameterType(name = "endIndex", value = "11")
                    }
                )
            }
        )
    }
)
public class StringFunctionExecutor extends FunctionExecutor {

    public String append(String value1, String value2) throws FunctionException {
        String result = "";

        if (value1 != null) {
            result += value1;
        }

        if (value2 != null) {
            result += value2;
        }

        return result;
    }


    public String indexOf(String value, String valueToFind) throws FunctionException {
        if (value != null & valueToFind != null) {
            try {
                return String.valueOf(value.indexOf(valueToFind));
            } catch (IndexOutOfBoundsException e) {
                return "-1";
            }
        }

        return "-1";
    }

    public String indexOfFromIndex(String value, String valueToFind, Integer fromIndex) throws FunctionException {
        if (value != null & valueToFind != null) {

            if (fromIndex > value.length()) {
                return "-1";
            }

            try {
                return String.valueOf(value.indexOf(valueToFind, fromIndex));
            } catch (IndexOutOfBoundsException e) {
                return "-1";
            }
        }

        return "-1";
    }

    public String substring(String value, Integer beginIndex, Integer endIndex) throws FunctionException {
        if (value != null) {
            try {
                if (beginIndex != null && endIndex != null) {
                    return value.substring(beginIndex, endIndex);
                }
            } catch (IndexOutOfBoundsException e) {
                return value;
            }
        }

        return value;
    }

}
