package nl.sodeso.webservice.mock.service.endpoint.expression;

import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.annotations.ExpressionExecutorType;
import nl.sodeso.commons.general.AnnotationUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ExpressionExecutorFactory {

    private static final Logger log = Logger.getLogger(ExpressionExecutorFactory.class.getName());

    private static final String MSG_FAILED_TO_INSTANTIATE_CONDITION = "Failed to instantiate expression '%s'";

    private static ExpressionExecutorFactory INSTANCE = null;

    private Set<Class<? extends ExpressionExecutor>> availableExpressions = new HashSet<>();

    private ExpressionExecutorFactory() {
        availableExpressions = AnnotationUtils.findSubTypesWithAnnotation(ExpressionExecutor.class, ExpressionExecutorType.class);
    }

    /**
     * Returns the singleton instance of the condition factory.
     *
     * @return the instance.
     */
    public static ExpressionExecutorFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ExpressionExecutorFactory();
        }

        return INSTANCE;
    }

    /**
     * Returns the executor for the given condition.
     *
     * @param jsExpression the jsExpression.
     *
     * @return the executor that can execute the given condition.
     *
     * @throws ConditionException thrown when there was a problemen creating the executor.
     */
    public ExpressionExecutor getExpression(JsExpression jsExpression) throws ExpressionException {
        for (Class<? extends ExpressionExecutor> expressionExecutor : availableExpressions) {
            ExpressionExecutorType expressionExecutorType = expressionExecutor.getAnnotation(ExpressionExecutorType.class);
            if (expressionExecutorType != null && expressionExecutorType.expression().getSimpleName().equals(jsExpression.getClass().getSimpleName())) {

                try {
                    Constructor<? extends ExpressionExecutor> executor = expressionExecutor.getConstructor();
                    return executor.newInstance();
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new ExpressionException(MSG_FAILED_TO_INSTANTIATE_CONDITION, expressionExecutor.getSimpleName());
                }
            }
        }

        return null;
    }

}
