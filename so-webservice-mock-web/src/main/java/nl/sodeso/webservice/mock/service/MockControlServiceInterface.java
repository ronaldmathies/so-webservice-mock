package nl.sodeso.webservice.mock.service;

import com.fasterxml.jackson.annotation.JsonView;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpointView;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequestView;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponseView;
import nl.sodeso.webservice.mock.model.service.JsService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public interface MockControlServiceInterface {

    String PARAM_RESPONSE_NAME = "responseName";
    String PARAM_ENDPOINT_NAME = "endpointName";
    String PARAM_ID = "id";
    String PARAM_SESSION_ID = "sessionId";
    String PARAM_GROUP = "group";

    String QUERY_NAME = "name";
    String QUERY_PATH = "path";


    String GET_VERSION = "version";
    String GET_API = "api";
    String DELETE_ALL = "all";

    String GET_GROUPS =  "groups";
    String GET_GROUP_FUNCTIONS = "{group}/functions";

    String POST_RESPONSE = "response/{endpointName}";
    String PUT_RESPONSE = "response/{endpointName}/{responseName}";
    String GET_RESPONSE = "response/{endpointName}/{responseName}";
    String DELETE_RESPONSE = "response/{endpointName}/{responseName}";
    String GET_RESPONSES = "responses/{endpointName}";
    String DELETE_RESPONSES = "responses/{endpointName}";

    String DELETE_ENDPOINT = "endpoint/{endpointName}";
    String POST_ENDPOINT = "endpoint";
    String PUT_ENDPOINT = "endpoint/{endpointName}";
    String GET_ENDPOINTS = "endpoints";
    String GET_ENDPOINT = "endpoint";

    String GET_EXPORT_ENDPOINT = "export/{endpointName}";
    String GET_EXPORT_ENDPOINT_WITH_REQUESTS = "export/{endpointName}/requests";
    String GET_EXPORT_ENDPOINT_WITH_RESPONSES = "export/{endpointName}/responses";
    String GET_EXPORT_ENDPOINT_WITH_RESPONSES_AND_REQUESTS = "export/{endpointName}/responses/requests";
    String GET_EXPORT_RESPONSE = "export/{endpointName}/{responseName}";
    String GET_EXPORT_RESPONSE_WITH_REQUESTS = "export/{endpointName}/{responseName}/requests";

    String GET_REQUEST = "request/{endpointName}/{id}";
    String GET_REQUESTS = "requests/{endpointName}";
    String GET_REQUEST_FOR_RESPONSE = "requests/{endpointName}/{responseName}/{id}";
    String GET_REQUESTS_FOR_RESPONSE = "requests/{endpointName}/{responseName}";

    String DELETE_REQUEST = "request/{endpointName}/{id}";
    String DELETE_REQUESTS_ALL = "requests/{endpointName}";
    String DELETE_REQUEST_FOR_RESPONSE = "requests/{endpointName}/{responseName}/{id}";
    String DELETE_REQUESTS_FOR_RESPONSE_ALL = "requests/{endpointName}/{responseName}";

    String GET_VARIABLES_ALL = "variables/all";
    String GET_VARIABLES_SESSION = "variables/session/{sessionId}";
    String GET_VARIABLES_GLOBAL = "variables/global";

    String GET_VARIABLES_ALL_RESET = "variables/all/reset";
    String GET_VARIABLES_SESSION_RESET = "variables/session/{sessionId}";
    String GET_VARIABLES_GLOBAL_RESET = "variables/global/reset";

    /**
     * Returns an OpenAPI document describing the interaction of
     * this service.
     * @return an JSON document in OpenAPI format describing this service.
     */
    @GET
    @Path(GET_API)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetApi();

    /**
     * Returns the version number of this service.
     *
     * @return A <code>JsService</code> instance containing the version number.
     *
     * @see JsService
     */
    @GET
    @Path(GET_VERSION)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVersion();

    @POST
    @Path(POST_ENDPOINT)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response PostEndpoint(@Valid @JsonView({JsEndpointView.Responses.class}) JsEndpoint jsEndpoint);

    @PUT
    @Path(PUT_ENDPOINT)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response PutEndpoint(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @Valid @JsonView({JsEndpointView.Responses.class}) JsEndpoint jsEndpoint);

    @DELETE
    @Path(DELETE_ENDPOINT)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteEndpoint(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @GET
    @Path(GET_ENDPOINTS)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsEndpointView.Summary.class})
    List<JsEndpoint> GetEndpoints();

    @GET
    @Path(GET_ENDPOINT)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsEndpointView.Detailed.class})
    Response GetEndpoint(@QueryParam(QUERY_NAME) String endpointName, @QueryParam(QUERY_PATH) String endpointPath);

    @GET
    @Path(GET_EXPORT_ENDPOINT)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsEndpointView.Detailed.class})
    Response GetExportEndpoint(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @GET
    @Path(GET_EXPORT_ENDPOINT_WITH_REQUESTS)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsEndpointView.Requests.class})
    Response GetExportEndpointWithRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @GET
    @Path(GET_EXPORT_ENDPOINT_WITH_RESPONSES)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsEndpointView.Responses.class})
    Response GetExportEndpointWithResponses(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @GET
    @Path(GET_EXPORT_ENDPOINT_WITH_RESPONSES_AND_REQUESTS)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsEndpointView.ResponsesAndRequests.class})
    Response GetExportEndpointWithResponsesAndRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @GET
    @Path(GET_EXPORT_RESPONSE)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsResponseView.Detailed.class})
    Response GetExportResponse(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName);

    @GET
    @Path(GET_EXPORT_RESPONSE_WITH_REQUESTS)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsResponseView.Requests.class})
    Response GetExportResponseWithRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName);

    @POST
    @Path(POST_RESPONSE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response PostResponse(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @JsonView({JsResponseView.Detailed.class}) @Valid JsResponse jsResponse);

    @PUT
    @Path(PUT_RESPONSE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response PutResponse(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName, @JsonView({JsResponseView.Detailed.class}) @Valid JsResponse jsResponse);

    @GET
    @Path(GET_RESPONSE)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsResponseView.Detailed.class})
    Response GetResponse(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName);

    @DELETE
    @Path(DELETE_RESPONSE)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteResponse(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName);

    @GET
    @Path(GET_RESPONSES)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsResponseView.Summary.class})
    Response GetResponses(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @DELETE
    @Path(DELETE_RESPONSES)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteResponses(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @DELETE
    @Path(DELETE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    Response all();

    @GET
    @Path(GET_REQUEST)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsRequestView.Detailed.class})
    Response GetRequest(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_ID) String id);

    @GET
    @Path(GET_REQUESTS)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsRequestView.Summary.class})
    Response GetRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @GET
    @Path(GET_REQUEST_FOR_RESPONSE)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsRequestView.Detailed.class})
    Response GetRequest(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName, @NotNull @PathParam(PARAM_ID) String id);

    @GET
    @Path(GET_REQUESTS_FOR_RESPONSE)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView({JsRequestView.Summary.class})
    Response GetRequests(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName);

    @DELETE
    @Path(DELETE_REQUEST)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteRequest(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_ID) String id);

    @DELETE
    @Path(DELETE_REQUESTS_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteRequestsAll(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName);

    @DELETE
    @Path(DELETE_REQUEST_FOR_RESPONSE)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteRequest(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName, @NotNull @PathParam(PARAM_ID) String id);

    @DELETE
    @Path(DELETE_REQUESTS_FOR_RESPONSE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    Response DeleteRequestsAll(@NotNull @PathParam(PARAM_ENDPOINT_NAME) String endpointName, @NotNull @PathParam(PARAM_RESPONSE_NAME) String responseName);

    @GET
    @Path(GET_VARIABLES_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVariablesAll();

    @GET
    @Path(GET_VARIABLES_SESSION)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVariablesSession(@NotNull @PathParam(PARAM_SESSION_ID) String sessionId);

    @GET
    @Path(GET_VARIABLES_GLOBAL)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVariablesGlobal();

    @GET
    @Path(GET_VARIABLES_ALL_RESET)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVariablesAllReset();

    @GET
    @Path(GET_VARIABLES_SESSION_RESET)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVariablesSessionReset(@NotNull @PathParam(PARAM_SESSION_ID) String sessionId);

    @GET
    @Path(GET_VARIABLES_GLOBAL_RESET)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetVariablesGlobalReset();

    @GET
    @Path(GET_GROUPS)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetGroups();

    @GET
    @Path(GET_GROUP_FUNCTIONS)
    @Produces(MediaType.APPLICATION_JSON)
    Response GetGroupFunctions(@NotNull @PathParam(PARAM_GROUP) String group);
}
