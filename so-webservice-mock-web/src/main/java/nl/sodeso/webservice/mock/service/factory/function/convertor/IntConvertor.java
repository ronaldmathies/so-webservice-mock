package nl.sodeso.webservice.mock.service.factory.function.convertor;

/**
 * @author Ronald Mathies
 */
public class IntConvertor implements Convertor<Integer> {

    public Class<Integer> getType() {
        return Integer.class;
    }

    @Override
    public Integer from(String value) throws ConvertorException {
        return Integer.parseInt(value.trim());
    }

}
