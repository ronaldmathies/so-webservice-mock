package nl.sodeso.webservice.mock.service.factory.function.functions;

import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.*;
import nl.sodeso.webservice.mock.service.factory.function.convertor.IntConvertor;

import java.util.Random;
import java.util.UUID;

/**
 * @author Ronald Mathies
 */
@FunctionsType(
        functions = {
            @FunctionType(
                name = "random",
                description = "random( from, to )",
                group = "Other",
                format = "_random ( from, to )",
                documentation = "Generates a random nummer between the from / to specified.",
                parameters = {
                    @ParameterType(name = "from", type = IntConvertor.class),
                    @ParameterType(name = "to", type = IntConvertor.class)
                },
                samples = {
                    @SampleType(
                        name = "Random number between 0 and 100",
                        parameters = {
                            @SampleParameterType(name = "from", value = "0"),
                            @SampleParameterType(name = "to", value = "100")
                        }
                    )
                }
            ),
            @FunctionType(
                name = "uuid",
                group = "Other",
                description = "uuid()",
                format = "_uuid()",
                documentation = "Generates a random unique identifier."
            )
        }
)
public class OtherFunctionExecutor extends FunctionExecutor {

    public String random(Integer from, Integer to) throws FunctionException {
        return String.valueOf(new Random().nextInt(to - from + 1) + from);
    }

    public String uuid() throws FunctionException {
        return UUID.randomUUID().toString();
    }

}
