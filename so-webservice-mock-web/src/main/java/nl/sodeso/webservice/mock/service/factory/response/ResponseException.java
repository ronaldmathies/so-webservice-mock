package nl.sodeso.webservice.mock.service.factory.response;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class ResponseException extends ServletException {

    public ResponseException(String message, Object ... args) {
        super(String.format(message, args));
    }
}