package nl.sodeso.webservice.mock.service.endpoint;

import nl.sodeso.commons.storage.FileEntry;
import nl.sodeso.commons.storage.StorageController;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.settings.JsLogging;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsDownloadResponseType;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsResponseType;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutor;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutorFactory;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class EndpointStore {

    private static final Logger log = Logger.getLogger(EndpointStore.class.getName());

    private static EndpointStore INSTANCE = null;

    private List<JsEndpoint> endpoints = new ArrayList<>();

    private EndpointStore() {
    }

    public static EndpointStore getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EndpointStore();
        }

        return INSTANCE;
    }

    public void updateEndpoint(String name, JsEndpoint jsEndpoint) throws EndpointStoreException {
        boolean isReplaced = false;

        int index = 0;
        JsEndpoint existingJsEndpoint = null;
        for (; index < endpoints.size(); index++) {
            if (endpoints.get(index).getName().equals(name)) {
                existingJsEndpoint = endpoints.get(index);
                break;
            }
        }

        if (existingJsEndpoint == null) {
            throw new EndpointStoreException("No endpoint found with name '%s'.", name);
        }

        endpoints.remove(index);

        // Copy over the responses since we can only edit the endpoint itself.
        jsEndpoint.setJsResponses(existingJsEndpoint.getJsResponses());
        jsEndpoint.setJsRequests(existingJsEndpoint.getJsRequests());

        endpoints.add(jsEndpoint);
    }

    public void addEndpoint(JsEndpoint jsEndpoint) throws EndpointStoreException {
        if (getEndpointByName(jsEndpoint.getName()).isPresent()) {
            throw new EndpointStoreException("An endpoint already exists with the name '%s'.", jsEndpoint.getName());
        }

        if (getEndpointByPath(jsEndpoint.getPath()).isPresent()) {
            throw new EndpointStoreException("An endpoint already exists that matches (a part of) the path '%s'.", jsEndpoint.getPath());
        }

        this.endpoints.add(jsEndpoint);
    }

    public Optional<JsEndpoint> getEndpointByPath(String path) {
        return endpoints.stream().filter(jsEndpoint -> {
            if (path.startsWith(jsEndpoint.getPath())) {

                String subpath = path.substring(jsEndpoint.getPath().length());
                if (subpath.isEmpty()) {
                    return true;
                }

                if (subpath.charAt(0) != '/') {
                    return false;
                }

                return path.startsWith(jsEndpoint.getPath());
            }

            return false;
        }).findFirst();
    }

    public Optional<JsEndpoint> getEndpointByName(String endpointName) {
        return endpoints.stream().filter(endpoint -> endpoint.getName().equals(endpointName)).findFirst();
    }

    public List<JsEndpoint> allEndpoints() {
        return this.endpoints;
    }

    public void removeEndpoint(String endpointName) {
        endpoints.removeIf(endpoint -> endpoint.getName().equals(endpointName));
    }

    public void removeResponse(String endpointName, String responseName) throws EndpointStoreException {
        Optional<JsEndpoint> jsEndpoint = getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> foundResponse = jsEndpoint.get().getJsResponseByName(responseName);
            if (foundResponse.isPresent()) {
                JsResponseType jsResponseType = foundResponse.get().getJsResponseType();
                if (jsResponseType instanceof JsDownloadResponseType) {
                    StorageController.getInstance().remove(((JsDownloadResponseType) jsResponseType).getFileUuid());
                }

                jsEndpoint.get().removeJsResponse(responseName);
            } else {
                throw new EndpointStoreException("Response with name '%s' does not exists.", responseName);
            }
        } else {
            throw new EndpointStoreException("Endpoint with name '%s' does not exists.", endpointName);
        }
    }

    public void updateResponse(String endpointName, String responseName, JsResponse jsResponse) throws EndpointStoreException {
        Optional<JsEndpoint> jsEndpoint = getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> foundResponse = jsEndpoint.get().getJsResponseByName(responseName);
            if (foundResponse.isPresent()) {
                removeResponse(endpointName, responseName);
                addResponse(endpointName, jsResponse);
            }
        } else {
            throw new EndpointStoreException("Endpoint with name '%s' does not exists.", endpointName);
        }
    }

    public void addResponse(String endpointName, JsResponse jsResponse) throws EndpointStoreException {
        Optional<JsEndpoint> jsEndpoint = getEndpointByName(endpointName);
        if (jsEndpoint.isPresent()) {
            Optional<JsResponse> foundResponse = jsEndpoint.get().getJsResponseByName(jsResponse.getName());
            if (foundResponse.isPresent()) {
                throw new EndpointStoreException("Response with name '%s' already exists.", jsResponse.getName());
            }

            if (jsResponse.getJsResponseType() instanceof JsDownloadResponseType) {
                JsDownloadResponseType jsDownloadResponseType = (JsDownloadResponseType)jsResponse.getJsResponseType();

                FileEntry fileEntry = new FileEntry(UUID.randomUUID().toString());
                jsDownloadResponseType.setFileUuid(fileEntry.getUuid());

                if (jsDownloadResponseType.getData() != null && !jsDownloadResponseType.getData().isEmpty()) {
                    StorageController.getInstance().add(fileEntry);

                    try {
                        IOUtils.copy(new Base64InputStream(new ByteArrayInputStream(jsDownloadResponseType.getData().getBytes("UTF-8")), false), new FileOutputStream(fileEntry.getFile()));
                    } catch (IOException e) {
                        throw new EndpointStoreException("Failed to store data in a temporarily file on disk.");
                    } finally {
                        jsDownloadResponseType.setData(null);
                        jsDownloadResponseType.setSize(fileEntry.getFile().length());
                    }
                }
            }

            jsEndpoint.get().addJsResponse(jsResponse);
        }
    }

    public void reset() {
        endpoints.clear();
    }

    public void log(String endpointName, String responseName, JsRequest jsRequest) {
        try {
            Optional<JsEndpoint> jsEndpoint = getEndpointByName(endpointName);
            if (jsEndpoint.isPresent()) {
                Optional<JsResponse> jsResponse = jsEndpoint.get().getJsResponseByName(responseName);
                if (jsResponse.isPresent()) {
                    JsLogging jsLogging = jsResponse.get().getJsLogging();
                    if (jsLogging != null && jsLogging.isEnableLogging()) {

                        ExpressionExecutor executor =
                                ExpressionExecutorFactory.getInstance().getExpression(jsLogging.getJsExpression());
                        String id = executor.value(jsLogging.getJsExpression(), jsRequest);
                        if (id == null) {
                            id = UUID.randomUUID().toString();
                        }

                        jsRequest.setId(id);

                        List<JsRequest> jsRequests = jsResponse.get().getJsRequests();

                        if (jsRequests.size() > jsEndpoint.get().getJsLogging().getNumberOfRequestsToStore()) {
                            jsRequests.remove(0);
                        }

                        jsRequests.add(jsRequest);
                    }
                }
            }
        } catch (ExpressionException e) {
            // do nothing... logging is not a first priority.
            log.warning("Request has not been logged, unable to ");
        }
    }

    @SuppressWarnings("unchecked")
    public void log(String endpointName, JsRequest jsRequest) {
        try {
            Optional<JsEndpoint> jsEndpoint = getEndpointByName(endpointName);
            if (jsEndpoint.isPresent()) {
                JsLogging jsLogging = jsEndpoint.get().getJsLogging();
                if (jsLogging != null && jsLogging.isEnableLogging()) {

                    ExpressionExecutor executor =
                            ExpressionExecutorFactory.getInstance().getExpression(jsLogging.getJsExpression());
                    String id = executor.value(jsLogging.getJsExpression(), jsRequest);
                    if (id == null) {
                        id = UUID.randomUUID().toString();
                    }

                    jsRequest.setId(id);

                    List<JsRequest> jsRequests = jsEndpoint.get().getJsRequests();

                    if (jsRequests.size() > jsEndpoint.get().getJsLogging().getNumberOfRequestsToStore()) {
                        jsRequests.remove(0);
                    }

                    jsRequests.add(jsRequest);
                }
            }
        } catch (ExpressionException e) {
            // do nothing... logging is not a first priority.
            log.warning("Request has not been logged, unable to ");
        }
    }

}
