package nl.sodeso.webservice.mock.service.util;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;

/**
 * @author Ronald Mathies
 */
public class XPathUtil {

    public static String valueFromString(String path, String value) throws XPathUtilException {

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(new InputSource(new StringReader(value)));
            document.getDocumentElement().normalize();

            return (String) XPathFactory.newInstance().newXPath().compile(path).evaluate(document, XPathConstants.STRING);
        } catch (SAXException e) {
            // Suppress SAX exception.
        } catch (ParserConfigurationException | IOException | XPathExpressionException e) {
            throw new XPathUtilException(e.getMessage());
        }

        return null;
    }

}
