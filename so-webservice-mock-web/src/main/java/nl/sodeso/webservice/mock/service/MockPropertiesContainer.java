package nl.sodeso.webservice.mock.service;

import nl.sodeso.commons.properties.annotations.EnvironmentResource;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.environment.EnvironmentContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = MockPropertiesContainer.DOMAIN
)
@EnvironmentResource
public class MockPropertiesContainer extends EnvironmentContainer {

    public static final String DOMAIN = "mock";

    public static final String DISCOVERY_SERVICE_ID = "SERVICE_ID";
    public static final String DISCOVERY_EXPOSED_PORT = "EXPOSED_PORT";

    public static final String DISCOVERY_MULTICAST_ENABLED = "MULTICAST_ENABLED";
    public static final String DISCOVERY_MULTICAST_GROUP = "MULTICAST_GROUP";
    public static final String DISCOVERY_MULTICAST_PORT = "MULTICAST_PORT";
    public static final String DISCOVERY_MULTICAST_INTERVAL = "MULTICAST_INTERVAL";

    public static final String DISCOVERY_BROADCAST_ENABLED = "BROADCAST_ENABLED";
    public static final String DISCOVERY_BROADCAST_PORT = "BROADCAST_PORT";
    public static final String DISCOVERY_BROADCAST_INTERVAL = "BROADCAST_INTERVAL";

}
