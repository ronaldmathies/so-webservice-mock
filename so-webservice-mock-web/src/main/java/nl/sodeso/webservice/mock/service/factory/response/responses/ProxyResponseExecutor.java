package nl.sodeso.webservice.mock.service.factory.response.responses;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.type.JsProxyResponseType;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.factory.response.ResponseException;
import nl.sodeso.webservice.mock.service.factory.response.ResponseExecutor;
import nl.sodeso.webservice.mock.service.factory.response.annotation.ResponseExecutorType;
import nl.sodeso.webservice.mock.service.util.Method;
import nl.sodeso.webservice.mock.service.variables.VariableManager;
import org.apache.commons.io.IOUtils;

import javax.json.JsonObject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

/**
 * @author Ronald Mathies
 */
@ResponseExecutorType(
    response = JsProxyResponseType.class
)
public class ProxyResponseExecutor extends ResponseExecutor<JsProxyResponseType> {

    private static final String COOKIE_MAPPING_NAME = "WS-MOCK-COOKIE-MAPPING";

    @Override
    public void execute(JsProxyResponseType jsProxyResponseType) throws ResponseException {
        JsRequest jsRequest = getContext().getJsRequest();

        try {
            URL requestUrl = new URL(getContext().getJsRequest().getRequestUrl());
            String path = requestUrl.getPath().replaceAll(getContext().getJsEndpoint().getPath(), "");

            StringBuilder urlBuilder = new StringBuilder(requestUrl.getProtocol())
                    .append("://")
                    .append(jsProxyResponseType.getDomain())
                    .append(path);

            if (!jsRequest.getJsQueries().isEmpty()) {
                urlBuilder.append(jsRequest.getFullJsQuery());
            }

            URL url = new URL(urlBuilder.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(jsRequest.getMethod());

            jsRequest.getJsHeaders().forEach(jsHeader -> connection.setRequestProperty(jsHeader.getName(), jsHeader.getValue()));

            final Map<String, String> domainMapping = new HashMap<>();
            Optional<JsCookie> jsCookieMapping = jsRequest.getJsCookie(COOKIE_MAPPING_NAME);
            if (jsCookieMapping.isPresent()) {
                domainMapping.putAll(new ObjectMapper().readValue(jsCookieMapping.get().getValue(), HashMap.class));
            }

            CookieManager cookieManager = new CookieManager();
            jsRequest.getJsCookies().forEach(jsCookie -> {
                if (!jsCookie.getName().equals(COOKIE_MAPPING_NAME) && !jsCookie.getName().equals(VariableManager.COOKIE_SESSION_NAME)) {
                    HttpCookie httpCookie = new HttpCookie(jsCookie.getName(), jsCookie.getValue());
                    if (jsCookie.getDomain() != null) {
                        httpCookie.setDomain(jsCookie.getDomain());
                    }

                    if (jsCookie.hasMaxAge()) {
                        httpCookie.setMaxAge(Integer.valueOf(jsCookie.getMaxAge()));
                    }

                    if (jsCookie.hasVersion()) {
                        httpCookie.setVersion(Integer.valueOf(jsCookie.getVersion()));
                    }

                    if (jsCookie.hasPath()) {
                        httpCookie.setPath(jsCookie.getPath());
                    }

                    if (jsCookie.hasComment()) {
                        httpCookie.setComment(jsCookie.getComment());
                    }

                    httpCookie.setSecure(jsCookie.isSecure());
                    httpCookie.setHttpOnly(jsCookie.isHttpOnly());

                    if (domainMapping.containsKey(jsCookie.getName())) {
                        httpCookie.setDomain(domainMapping.get(jsCookie.getName()));
                    }

                    try {
                        cookieManager.getCookieStore().add(url.toURI(), httpCookie);
                    } catch (URISyntaxException e) {
                        System.out.println(e);
                    }
                }

            });

            connection.setConnectTimeout(jsProxyResponseType.getConnectionTimeoutInSeconds() * 1000);
            connection.setReadTimeout(jsProxyResponseType.getReadTimeoutInSeconds() * 1000);

            if (Method.valueOf(jsRequest.getMethod()).isBodyAllowed()) {
                connection.setDoOutput(true);
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                IOUtils.copy(new ByteArrayInputStream(jsRequest.getBody().getBytes()), dataOutputStream);
                dataOutputStream.flush();
                dataOutputStream.close();
            }

            HttpServletResponse servletResponse = getContext().getServletResponse();
            servletResponse.setStatus(connection.getResponseCode());

            // Handle response.

            addHeaders(jsProxyResponseType.getJsHeaders(), servletResponse);
            addCookies(jsProxyResponseType.getJsCookies(), servletResponse);

            processHeadersFromProxyConnection(jsProxyResponseType, connection);
            processBodyFromProxyConnection(connection);

        } catch (IOException e) {
            throw new ResponseException("Failed to proxy request.");
        }
    }

    private void processHeadersFromProxyConnection(JsProxyResponseType jsProxyResponseType, HttpURLConnection connection) {
        connection.getHeaderFields().forEach((name, values) -> {
            if (name != null) {
                if (name.equals("Set-Cookie")) {
                    if (!jsProxyResponseType.isIgnoreCookies()) {
                        processCookiesFromProxyConnection(jsProxyResponseType, values);
                    }
                } else if (!jsProxyResponseType.isIgnoreHeaders()) {
                    values.forEach(value -> getContext().getServletResponse().addHeader(name, value));
                }
            }
        });
    }

    private void processCookiesFromProxyConnection(JsProxyResponseType jsProxyResponseType, List<String> values) {
        try {
            URL requestUrl = new URL(getContext().getJsRequest().getRequestUrl());

            Map<String, String> domainMapping = new HashMap<>();

            values.forEach(value -> {

                List<HttpCookie> httpCookies = HttpCookie.parse(value);
                httpCookies.forEach(httpCookie -> {

                    domainMapping.put(httpCookie.getName(), httpCookie.getDomain());

                    Cookie cookie = new Cookie(httpCookie.getName(), httpCookie.getValue());
                    cookie.setDomain(requestUrl.getHost());
                    cookie.setMaxAge((int) httpCookie.getMaxAge());
                    cookie.setVersion(httpCookie.getVersion());
                    cookie.setPath(httpCookie.getPath());
                    cookie.setComment(httpCookie.getComment());
                    cookie.setSecure(httpCookie.getSecure());
                    cookie.setHttpOnly(httpCookie.isHttpOnly());
                    getContext().getServletResponse().addCookie(cookie);

                    // TODO: process cookies, watch out for rewriting the path / domain
                });
            });

            Cookie domainMappingCookie = new Cookie(COOKIE_MAPPING_NAME, new ObjectMapper().writeValueAsString(domainMapping));
            getContext().getServletResponse().addCookie(domainMappingCookie);

        } catch (MalformedURLException | JsonProcessingException e) {
            //throw new ResponseException("Failed to proxy request: " + e.getMessage());
            System.out.println(e.getMessage());
        }
    }

    private void processBodyFromProxyConnection(HttpURLConnection connection) throws IOException {
        IOUtils.copy(connection.getInputStream(), getContext().getServletResponse().getOutputStream());
    }
}
