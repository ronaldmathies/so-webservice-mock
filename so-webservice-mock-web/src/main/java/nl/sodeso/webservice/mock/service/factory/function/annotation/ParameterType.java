package nl.sodeso.webservice.mock.service.factory.function.annotation;

import nl.sodeso.webservice.mock.service.factory.function.convertor.Convertor;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Documented
@Target({ TYPE })
@Retention(RUNTIME)
public @interface ParameterType {

    String name();
    Class<? extends Convertor> type();

}