package nl.sodeso.webservice.mock.service.factory.condition;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.service.Executor;

/**
 * @author Ronald Mathies
 */
public abstract class ConditionExecutor<C extends JsCondition> extends Executor {

    /**
     * Checks the condition agains the context to see if the context meets the conditions requirements.
     *
     * @param condition the condition.
     *
     * @return true if the meets the requirements, false if not.
     *
     * @throws ConditionException Thrown when a problem occurred during the execution.
     */
    public abstract boolean isApplicable(C condition) throws ConditionException;

}
