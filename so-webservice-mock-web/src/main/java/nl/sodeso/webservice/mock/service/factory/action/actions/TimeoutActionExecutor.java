package nl.sodeso.webservice.mock.service.factory.action.actions;

import nl.sodeso.webservice.mock.model.endpoint.response.action.actions.JsTimeoutAction;
import nl.sodeso.webservice.mock.service.factory.action.ActionException;
import nl.sodeso.webservice.mock.service.factory.action.ActionExecutor;
import nl.sodeso.webservice.mock.service.factory.action.annotation.ActionExecutorType;
import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionUtil;

import javax.servlet.ServletException;

/**
 * This action will delay the request by the amount specified in the arguments.
 *
 * @author Ronald Mathies
 */
@ActionExecutorType(
        action = JsTimeoutAction.class
)
public class TimeoutActionExecutor extends ActionExecutor<JsTimeoutAction> {

    public void execute(JsTimeoutAction action) throws ServletException {
        try {
            int seconds = Integer.parseInt(FunctionUtil.evaluateAndExecuteFunctions(getContext(), action.getSeconds()));
            Thread.sleep(seconds * 1000l);
        } catch (FunctionException | InterruptedException e) {
            throw new ActionException(e.getMessage());
        }
    }

}
