package nl.sodeso.webservice.mock.service.endpoint.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsQueryExpression;
import nl.sodeso.webservice.mock.model.generic.JsQuery;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutor;
import nl.sodeso.webservice.mock.service.endpoint.expression.annotations.ExpressionExecutorType;
import nl.sodeso.webservice.mock.service.util.*;

import java.util.Optional;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ExpressionExecutorType(
    name = QueryExpressionExecutor.NAME,
    expression = JsQueryExpression.class
)
public class QueryExpressionExecutor extends ExpressionExecutor<JsQueryExpression> {

    // TODO: Neccesary?
    protected static final String NAME = "body";

    public String value(JsQueryExpression expression, JsRequest jsRequest) throws ExpressionException {
        try {
            String valueToMatch = "";

            Optional<JsQuery> jsQuery = jsRequest.getJsQuery(expression.getName());
            if (jsQuery.isPresent()) {

                if (expression.getJsonpath() != null) {
                    valueToMatch = JSonUtil.valueFromString(expression.getJsonpath(), jsQuery.get().getValue());
                }

                if (expression.getRegexp() != null) {
                    valueToMatch = RegExpUtil.valueFromString(expression.getRegexp(), jsQuery.get().getValue());
                }

                if (expression.getXpath() != null) {
                    valueToMatch = XPathUtil.valueFromString(expression.getXpath(), jsQuery.get().getValue());
                }
            }

            return valueToMatch;
        } catch (XPathUtilException | RegExpUtilException | JSonUtilException e) {
            throw new ExpressionException(e.getMessage());
        }
    }

}
