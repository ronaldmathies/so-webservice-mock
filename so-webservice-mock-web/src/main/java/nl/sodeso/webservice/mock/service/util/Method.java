package nl.sodeso.webservice.mock.service.util;

/**
 * @author Ronald Mathies
 */
public enum Method {

    GET(false),
    HEAD(false),
    POST(true),
    PUT(true),
    DELETE(false),
    TRACE(false),
    CONNECT(false);

    private boolean isBodyAllowed;

    Method(boolean isBodyAllowed) {
        this.isBodyAllowed = isBodyAllowed;
    }

    public boolean isBodyAllowed() {
        return this.isBodyAllowed;
    }

    public Method fromValue(String value) {
        for (Method method : Method.values()) {
            if (method.name().equals(value)) {
                return method;
            }
        }

        return null;
    }
}
