package nl.sodeso.webservice.mock.service.factory.response.annotation;

import nl.sodeso.webservice.mock.model.endpoint.response.type.JsResponseType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Documented
@Target({ TYPE })
@Retention(RUNTIME)
public @interface ResponseExecutorType {

    Class<? extends JsResponseType> response();

}