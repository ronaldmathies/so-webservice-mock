package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsMethodCondition;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.annotation.ConditionExecutorType;

/**
 * JsCondition that tests if the request method matches the given value.
 *
 * @author Ronald Mathies
 */
@ConditionExecutorType(
    condition = JsMethodCondition.class
)
public class MethodConditionExecutor extends ConditionExecutor<JsMethodCondition> {

    public boolean isApplicable(JsMethodCondition condition) throws ConditionException {
        return getContext().getJsRequest().getMethod().equals(condition.getType());
    }

}
