package nl.sodeso.webservice.mock.service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Ronald Mathies
 */
public class MockControlServiceApplication extends javax.ws.rs.core.Application {

    private Set<Object> singletons = new HashSet<>();

    public MockControlServiceApplication() {
        this.singletons.add(new MockControlService());
    }

    @Override
    public Set<Object> getSingletons() {
        return this.singletons;
    }
}
