package nl.sodeso.webservice.mock.service.factory.function.convertor;

/**
 * @author Ronald Mathies
 */
public class BooleanConvertor implements Convertor<Boolean> {

    public Class<Boolean> getType() {
        return Boolean.class;
    }

    @Override
    public Boolean from(String value) throws ConvertorException {
        return Boolean.valueOf(value.trim());
    }

}
