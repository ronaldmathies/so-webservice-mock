package nl.sodeso.webservice.mock.service;

import nl.sodeso.webservice.mock.service.context.Context;

/**
 * @author Ronald Mathies
 */
public abstract class Executor {

    private Context context = null;

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return this.context;
    }

}
