package nl.sodeso.webservice.mock.service.factory.function.convertor;

/**
 * @author Ronald Mathies
 */
public class DoubleConvertor implements Convertor<Double> {

    public Class<Double> getType() {
        return Double.class;
    }

    @Override
    public Double from(String value) throws ConvertorException {
        return Double.parseDouble(value.trim());
    }

}
