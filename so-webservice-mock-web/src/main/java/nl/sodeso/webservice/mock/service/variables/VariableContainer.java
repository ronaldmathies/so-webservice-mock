package nl.sodeso.webservice.mock.service.variables;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class VariableContainer {

    private Map<String, String> variables = new HashMap<>();

    public void setValue(String variableName, String value) {
        variables.put(variableName, value);
    }

    public String getValue(String variableName) {
        return variables.get(variableName);
    }

    public Map<String, String> getVariables() {
        return this.variables;
    }

    public void reset() {
        this.variables.clear();
    }
}
