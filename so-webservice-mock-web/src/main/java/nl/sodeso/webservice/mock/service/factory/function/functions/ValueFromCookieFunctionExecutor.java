package nl.sodeso.webservice.mock.service.factory.function.functions;

import com.jayway.jsonpath.JsonPath;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.*;
import nl.sodeso.webservice.mock.service.factory.function.convertor.RegExpConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.StringConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.XpathConvertor;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
@FunctionsType(
    functions = {
        @FunctionType(
            name = "valueFromCookie",
            description = "valueFromCookie( name )",
            group = "Cookie",
            format = "_valueFromCookie( 'name' )",
            documentation = "Resolves the value of the cookie with the specified name from the incoming request.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Resolve value of cookie",
                    parameters = {
                        @SampleParameterType(name = "name", value = "mycookie")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromCookieUsingJsonPath",
            description = "valueFromCookieUsingJsonPath( name, jsonpath )",
            group = "Cookie",
            format = "_valueFromCookieUsingJsonPath( 'name', 'jsonpath' )",
            documentation = "Resolves the value of the cookie with the specified name from the incoming request, the value is then parsed using the JSON path, this would require the value of the cookie to contain JSON data.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class),
                @ParameterType(name = "jsonpath", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "name", value = "mycookie"),
                        @SampleParameterType(name = "jsonpath", value = "$.request.reference")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromCookieUsingRegExp",
            description = "valueFromCookieUsingRegExp( name, regexp )",
            group = "Cookie",
            format = "_valueFromCookieUsingRegExp( 'name', 'regexp' )",
            documentation = "Resolves the value of the cookie with the specified name from the incoming request, the value is then parsed using the regular expression.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class),
                @ParameterType(name = "regexp", type = RegExpConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "name", value = "mycookie"),
                        @SampleParameterType(name = "regexp", value = "<reference>(.*?)</reference>")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromCookieUsingXpath",
            description = "valueFromCookieUsingXpath( name, xpath )",
            group = "Cookie",
            format = "_valueFromCookieUsingXpath( 'name', 'xpath' )",
            documentation = "Resolves the value of the cookie with the specified name from the incoming request, the value is then parsed using the XPath expression, this would require the value of the cookie to contain XML data.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class),
                @ParameterType(name = "xpath", type = XpathConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "name", value = "mycookie"),
                        @SampleParameterType(name = "xpath", value = "//*[local-name()=\\'element\\']")
                    }
                )
            }
        )
    }
)
public class ValueFromCookieFunctionExecutor extends FunctionExecutor {

    public String valueFromCookie(String name) throws FunctionException {
        Optional<JsCookie> jsCookieOptional = getContext().getJsRequest().getJsCookie(name);
        return jsCookieOptional.map(JsCookie::getValue).orElse("");

    }

    public String valueFromCookieUsingJsonPath(String name, String jsonpath) throws FunctionException {
        Optional<JsCookie> jsCookieOptional = getContext().getJsRequest().getJsCookie(name);
        return jsCookieOptional.map(jsCookie -> (String)JsonPath.read(jsCookie.getValue(), jsonpath)).orElse("");
    }

    public String valueFromCookieUsingRegExp(String name, Pattern pattern) throws FunctionException {
        Optional<JsCookie> jsCookieOptional = getContext().getJsRequest().getJsCookie(name);
        if (jsCookieOptional.isPresent()) {
            Matcher matcher = pattern.matcher(jsCookieOptional.get().getValue());
            if (matcher.find()) {
                if (matcher.groupCount() > 0) {
                    return matcher.group(1);
                }
            }
        }
        return "";
    }

    public String valueFromCookieUsingXpath(String name, XPathExpression xpath) throws FunctionException {
        try {
            Optional<JsCookie> jsCookieOptional = getContext().getJsRequest().getJsCookie(name);
            if (jsCookieOptional.isPresent()) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                Document document = documentBuilder.parse(
                        new InputSource(
                                new StringReader(jsCookieOptional.get().getValue())));

                document.getDocumentElement().normalize();

                return (String) xpath.evaluate(document, XPathConstants.STRING);
            }
        } catch (ParserConfigurationException | XPathExpressionException | SAXException | IOException e) {
            throw new FunctionException("Failed to parse the cookie value with name '%s' due to an error '%s'.", name, e.getMessage());
        }

        return "";
    }

}
