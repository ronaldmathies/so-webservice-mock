package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsPathCondition;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.annotation.ConditionExecutorType;
import nl.sodeso.webservice.mock.service.util.RegExpUtil;
import nl.sodeso.webservice.mock.service.util.RegExpUtilException;

import java.util.logging.Logger;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ConditionExecutorType(
    condition = JsPathCondition.class
)
public class PathConditionExecutor extends ConditionExecutor<JsPathCondition> {

    private static final Logger log = Logger.getLogger(PathConditionExecutor.class.getName());

    /**
     * TODO: Matching should be switched arround? requestPathPArts leading?
     * Add regexp?
     */
    public boolean isApplicable(JsPathCondition condition) throws ConditionException {

        log.info(String.format(
            "\n\t\tPathConditionExecutor -> (\n" +
            "\t\t   path: '%s'\n" +
            "\t\t )\n",
            condition.getPath()));

        String requestUri = ContextUtil.getRequestUri(getContext(), false);

        if (condition.isRegexp()) {
            try {
                return RegExpUtil.matches(condition.getPath(), requestUri);
            } catch (RegExpUtilException e) {
                throw new ConditionException(e.getMessage());
            }
        }

        return condition.getPath().equals(requestUri);
    }

}
