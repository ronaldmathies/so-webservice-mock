package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsBodyCondition;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.annotation.ConditionExecutorType;
import nl.sodeso.webservice.mock.service.util.*;

import java.io.IOException;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ConditionExecutorType(
    condition = JsBodyCondition.class
)
public class BodyConditionExecutor extends ConditionExecutor<JsBodyCondition> {

    public boolean isApplicable(JsBodyCondition condition) throws ConditionException {
        try {
            String valueToMatch = getContext().getJsRequest().getBody();

            if (condition.getJsonpath() != null) {
                valueToMatch = JSonUtil.valueFromString(condition.getJsonpath(), valueToMatch);
            }

            if (condition.getRegexp() != null) {
                valueToMatch = RegExpUtil.valueFromString(condition.getRegexp(), valueToMatch);
            }

            if (condition.getXpath() != null) {
                valueToMatch = XPathUtil.valueFromString(condition.getXpath(), valueToMatch);
            }

            if (condition.getMatches().equals(valueToMatch)) {
                return true;
            }

        } catch (XPathUtilException | RegExpUtilException | JSonUtilException e) {
            throw new ConditionException(e.getMessage());
        }

        return false;

    }

}
