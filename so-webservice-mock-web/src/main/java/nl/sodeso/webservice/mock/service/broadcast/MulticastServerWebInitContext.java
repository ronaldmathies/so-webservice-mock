package nl.sodeso.webservice.mock.service.broadcast;

import nl.sodeso.commons.network.discovery.multicast.MulticastServerConfiguration;
import nl.sodeso.commons.network.discovery.multicast.AbstractMulticastServerWebInitContext;
import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.webservice.mock.service.MockPropertiesContainer;

/**
 * @author Ronald Mathies
 */
public class MulticastServerWebInitContext extends AbstractMulticastServerWebInitContext {

    public MulticastServerConfiguration getConfiguration() {
        return new MulticastServerConfiguration() {

            @Override
            public boolean isEnabled() {
                return PropertyConfiguration.getInstance().getBooleanProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_MULTICAST_ENABLED, false);
            }

            @Override
            public String getMulticastGroup() {
                return PropertyConfiguration.getInstance().getStringProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_MULTICAST_GROUP, null);
            }

            @Override
            public int getMulticastPort() {
                return PropertyConfiguration.getInstance().getIntegerProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_MULTICAST_PORT, 0);
            }

            @Override
            public int getExposedPort() {
                return PropertyConfiguration.getInstance().getIntegerProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_EXPOSED_PORT, 0);
            }

            @Override
            public String getServiceId() {
                return PropertyConfiguration.getInstance().getStringProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_SERVICE_ID, null);
            }

            @Override
            public int getMulticastInterval() {
                return PropertyConfiguration.getInstance().getIntegerProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_MULTICAST_INTERVAL, 0);
            }
        };
    }
}
