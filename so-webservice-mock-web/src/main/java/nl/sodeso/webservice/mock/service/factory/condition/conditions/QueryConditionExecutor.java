package nl.sodeso.webservice.mock.service.factory.condition.conditions;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.conditions.JsQueryCondition;
import nl.sodeso.webservice.mock.model.generic.JsQuery;
import nl.sodeso.webservice.mock.service.context.ContextUtil;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.annotation.ConditionExecutorType;

import java.util.Optional;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ConditionExecutorType(
    condition = JsQueryCondition.class
)
public class QueryConditionExecutor extends ConditionExecutor<JsQueryCondition> {

    public boolean isApplicable(JsQueryCondition condition) throws ConditionException {
        for (JsQuery jsQuery : condition.getQueries()) {
            Optional<JsQuery> jsQueryOptional = getContext().getJsRequest().getJsQuery(jsQuery.getName());
            if (jsQueryOptional.isPresent()) {
                String queryValue = jsQueryOptional.get().getValue();
                if (!jsQuery.getValue().equals(queryValue)) {
                    return false;
                }
            }
        }

        return true;
    }

}
