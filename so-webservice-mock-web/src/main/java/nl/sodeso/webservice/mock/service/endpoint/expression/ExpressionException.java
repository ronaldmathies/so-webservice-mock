package nl.sodeso.webservice.mock.service.endpoint.expression;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class ExpressionException extends ServletException {

    public ExpressionException(String message, Object ... args) {
        super(String.format(message, args));
    }
}