package nl.sodeso.webservice.mock.service.endpoint.expression;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.JsExpression;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Ronald Mathies
 */
public abstract class ExpressionExecutor<E extends JsExpression> {

    /**
     * Checks the condition agains the context to see if the context meets the conditions requirements.
     *
     * @param expression the expression.
     *
     * @return true if the meets the requirements, false if not.
     *
     * @throws ConditionException Thrown when a problem occurred during the execution.
     */
    public abstract String value(E expression, JsRequest jsRequest) throws ExpressionException;

}
