package nl.sodeso.webservice.mock.service.endpoint.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsHeaderExpression;
import nl.sodeso.webservice.mock.model.generic.JsHeader;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutor;
import nl.sodeso.webservice.mock.service.endpoint.expression.annotations.ExpressionExecutorType;
import nl.sodeso.webservice.mock.service.util.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ExpressionExecutorType(
    name = HeaderExpressionExecutor.NAME,
    expression = JsHeaderExpression.class
)
public class HeaderExpressionExecutor extends ExpressionExecutor<JsHeaderExpression> {

    protected static final String NAME = "header";

    public String value(JsHeaderExpression expression, JsRequest jsRequest) throws ExpressionException {
        try {
            String valueToMatch = "";

            Optional<JsHeader> jsHeader = jsRequest.getJsHeader(expression.getName());
            if (jsHeader.isPresent()) {
                if (expression.getJsonpath() != null) {
                    valueToMatch = JSonUtil.valueFromString(expression.getJsonpath(), jsHeader.get().getValue());
                }

                if (expression.getRegexp() != null) {
                    valueToMatch = RegExpUtil.valueFromString(expression.getRegexp(), jsHeader.get().getValue());
                }

                if (expression.getXpath() != null) {
                    valueToMatch = XPathUtil.valueFromString(expression.getXpath(), jsHeader.get().getValue());
                }
            }
            return valueToMatch;
        } catch (XPathUtilException | RegExpUtilException | JSonUtilException e) {
            throw new ExpressionException(e.getMessage());
        }
    }

}
