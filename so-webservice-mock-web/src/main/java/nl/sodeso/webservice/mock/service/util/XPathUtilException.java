package nl.sodeso.webservice.mock.service.util;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class XPathUtilException extends ServletException {

    public XPathUtilException(String message, Object ... args) {
        super(String.format(message, args));
    }

}