package nl.sodeso.webservice.mock.service.endpoint.expression.expressions;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.settings.expression.expressions.JsBodyExpression;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionException;
import nl.sodeso.webservice.mock.service.endpoint.expression.ExpressionExecutor;
import nl.sodeso.webservice.mock.service.endpoint.expression.annotations.ExpressionExecutorType;
import nl.sodeso.webservice.mock.service.util.*;

/**
 * JsCondition that tests the incomming response using a JSon JsPath construction. This would only work if the
 * incomming response is a JSon response.
 *
 * @author Ronald Mathies
 */
@ExpressionExecutorType(
    name = BodyExpressionExecutor.NAME,
    expression = JsBodyExpression.class
)
public class BodyExpressionExecutor extends ExpressionExecutor<JsBodyExpression> {

    // TODO: Neccesary?
    protected static final String NAME = "body";

    public String value(JsBodyExpression expression, JsRequest jsRequest) throws ExpressionException {
        try {
            String valueToMatch = "";
            if (expression.getJsonpath() != null) {
                valueToMatch = JSonUtil.valueFromString(expression.getJsonpath(), jsRequest.getBody());
            }

            if (expression.getRegexp() != null) {
                valueToMatch = RegExpUtil.valueFromString(expression.getRegexp(), jsRequest.getBody());
            }

            if (expression.getXpath() != null) {
                valueToMatch = XPathUtil.valueFromString(expression.getXpath(), jsRequest.getBody());
            }

            return valueToMatch;
        } catch (XPathUtilException | RegExpUtilException | JSonUtilException e) {
            throw new ExpressionException(e.getMessage());
        }
    }

}
