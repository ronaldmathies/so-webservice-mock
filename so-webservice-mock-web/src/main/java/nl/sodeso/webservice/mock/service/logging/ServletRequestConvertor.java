package nl.sodeso.webservice.mock.service.logging;

import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.generic.JsCookie;
import nl.sodeso.webservice.mock.service.util.Method;
import nl.sodeso.webservice.mock.service.util.RequestUtil;

import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

/**
 * @author Ronald Mathies
 */
public class ServletRequestConvertor {

    public static JsRequest toService(HttpServletRequest servletRequest) {
        JsRequest.Builder builder = new JsRequest.Builder()
                .withMethod(servletRequest.getMethod())
                .withRequestUrl(toRequestUrl(servletRequest))
                .withRequestUri(servletRequest.getRequestURI());

        try {
            // Add request headers
            Enumeration<String> headerNames = servletRequest.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                builder.withHeader(name, servletRequest.getHeader(name));
            }

            // Add request cookies
            if (servletRequest.getCookies() != null) {
                for (Cookie cookie : servletRequest.getCookies()) {
                    builder.withCookie(
                            new JsCookie.Builder(cookie.getName(), cookie.getValue())
                                .withDomain(cookie.getDomain())
                            .withPath(cookie.getPath())
                            .withVersion(String.valueOf(cookie.getVersion()))
                            .withMaxAge(String.valueOf(cookie.getMaxAge()))
                            .withSecure(cookie.getSecure())
                            .withHttpOnly(cookie.isHttpOnly())
                            .build());
                }
            }

            // Add request query parameters
            Enumeration<String> parameterNames = servletRequest.getParameterNames();
            while (parameterNames.hasMoreElements()) {
                String name = parameterNames.nextElement();
                builder.withQuery(name, servletRequest.getParameter(name));
            }

            if (Method.valueOf(servletRequest.getMethod()).isBodyAllowed()) {
                builder.withBody(RequestUtil.getBody(servletRequest));
            }

            if (Method.valueOf(servletRequest.getMethod()).isBodyAllowed()) {
                builder.withBody(RequestUtil.getBody(servletRequest));
            }
        } catch (IOException e) {

        }

        return builder.build();
    }

    public static  String toRequestUrl(HttpServletRequest servletRequest) {
        StringBuffer requestUrl = servletRequest.getRequestURL();
        if (requestUrl == null) {
            requestUrl = new StringBuffer();
        }

        String queryString = servletRequest.getQueryString();
        if (queryString != null) {
            requestUrl.append('?').append(queryString);
        }
        return requestUrl.toString();
    }
}
