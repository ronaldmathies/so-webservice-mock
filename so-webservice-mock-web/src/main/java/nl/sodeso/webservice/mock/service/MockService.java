package nl.sodeso.webservice.mock.service;

import nl.sodeso.commons.general.CloneUtil;
import nl.sodeso.webservice.mock.model.endpoint.JsEndpoint;
import nl.sodeso.webservice.mock.model.endpoint.request.JsRequest;
import nl.sodeso.webservice.mock.model.endpoint.response.JsResponse;
import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;
import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.endpoint.EndpointStore;
import nl.sodeso.webservice.mock.service.factory.ActionFactory;
import nl.sodeso.webservice.mock.service.factory.ConditionFactory;
import nl.sodeso.webservice.mock.service.factory.ResponseFactory;
import nl.sodeso.webservice.mock.service.factory.action.ActionException;
import nl.sodeso.webservice.mock.service.factory.action.ActionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.response.ResponseExecutor;
import nl.sodeso.webservice.mock.service.logging.ServletRequestConvertor;
import nl.sodeso.webservice.mock.service.variables.VariableManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * @author Ronald Mathies
 */
@WebServlet (
    name = "Mock Service",
        urlPatterns = {"/"}
)
public class MockService extends HttpServlet {

    @Override
    protected void service(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        VariableManager.getInstance().initOrActivateSessionContainer(servletRequest, servletResponse);
        VariableManager.getInstance().initRequestContainer();

        Optional<JsEndpoint> jsEndpoint = EndpointStore.getInstance().getEndpointByPath(servletRequest.getRequestURI());

        try {
            JsRequest jsRequest = ServletRequestConvertor.toService(servletRequest);

            if (jsEndpoint.isPresent()) {
                Optional<JsResponse> jsResponse = findAndProcessResponse(jsEndpoint.get(), jsRequest, servletResponse);
                if (jsResponse.isPresent()) {
                    EndpointStore.getInstance().log(jsEndpoint.get().getName(), jsResponse.get().getName(), jsRequest);

                    if (jsResponse.get().getJsSettings().isDeleteAfterUse()) {
                        EndpointStore.getInstance().removeResponse(jsEndpoint.get().getName(), jsResponse.get().getName());
                    }
                } else {
                    EndpointStore.getInstance().log(jsEndpoint.get().getName(), jsRequest);
                    throw new MockServiceException("No matching response found based on the incoming request on path '%s'", servletRequest.getRequestURI());
                }

            } else {
                throw new MockServiceException("No endpoint found with path '%s'", jsRequest.getRequestUri());
            }

        } catch (MockServiceException e) {
            if (!servletResponse.isCommitted()) {
                servletResponse.sendError(400, e.getMessage());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private Optional<JsResponse> findAndProcessResponse(JsEndpoint jsEndpoint, JsRequest jsRequest, HttpServletResponse servletResponse) throws ServletException, IOException, MockServiceException {
        JsCondition currentJsCondition = null;
        JsAction currentJsAction = null;

        try {
            Context context = new Context();
            context.setJsRequest(jsRequest);
            context.setServletResponse(servletResponse);
            context.setJsEndpoint(jsEndpoint);

            for (JsResponse jsResponse : jsEndpoint.getJsResponses()) {

                // Reset the response every time because conditions should not make any changes.
                context.setJsResponse(CloneUtil.clone(jsResponse));

                boolean isApplicable = true;
                if (jsResponse.getJsConditions().isEmpty()) {
                    isApplicable = true;
                } else {
                    for (JsCondition jsCondition : jsResponse.getJsConditions()) {
                        currentJsCondition = jsCondition;

                        ConditionExecutor conditionExecutor = ConditionFactory.getInstance().getCondition(jsCondition);
                        conditionExecutor.setContext(context);
                        if (!conditionExecutor.isApplicable(jsCondition)) {
                            isApplicable = false;
                            break;
                        }
                    }
                }

                if (isApplicable) {

                    // Reset the response since conditions should not make any changes to the response.
                    JsResponse jsResponseClone = CloneUtil.clone(jsResponse);
                    context.setJsResponse(jsResponseClone);

                    for (JsAction jsAction : jsResponse.getJsActions()) {
                        currentJsAction = jsAction;

                        ActionExecutor actionExecutor = ActionFactory.getInstance().getAction(jsAction);
                        actionExecutor.setContext(context);
                        actionExecutor.execute(jsAction);
                    }

                    ResponseExecutor responseExecutor = ResponseFactory.getInstance().getExecutor(jsResponse.getJsResponseType());
                    responseExecutor.setContext(context);

                    // TODO: Why pass in the response type when we have the context?
                    responseExecutor.execute(jsResponseClone.getJsResponseType());

                    return Optional.of(context.getJsResponse());
                }
            }
        } catch (ActionException e) {
            throw new MockServiceException("Failed to execute action '%s' due to '%s'", currentJsAction.getClass().getSimpleName(), e.getMessage());
        } catch (ConditionException e) {
            throw new MockServiceException("Failed to execute condition '%s' due to '%s'", currentJsCondition.getClass().getSimpleName(), e.getMessage());
        }

        return Optional.empty();
    }

}
