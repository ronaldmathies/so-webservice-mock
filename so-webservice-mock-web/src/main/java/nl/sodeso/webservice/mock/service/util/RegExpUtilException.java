package nl.sodeso.webservice.mock.service.util;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class RegExpUtilException extends ServletException {

    public RegExpUtilException(String message, Object ... args) {
        super(String.format(message, args));
    }

}