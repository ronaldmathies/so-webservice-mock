package nl.sodeso.webservice.mock.service.factory.function.convertor;

/**
 * @author Ronald Mathies
 */
public interface Convertor<T> {

    Class<T> getType();
    T from(String value) throws ConvertorException;

}
