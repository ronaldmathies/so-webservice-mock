package nl.sodeso.webservice.mock.service.factory;

import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.FunctionType;
import nl.sodeso.commons.general.AnnotationUtils;
import nl.sodeso.webservice.mock.service.factory.function.annotation.FunctionsType;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class FunctionFactory {

    private static final Logger log = Logger.getLogger(FunctionFactory.class.getName());

    private static final String MSG_FAILED_TO_INSTANTIATE_FUNCTION = "Failed to instantiate function '%s'";

    private static final Object lock = new Object();
    private static FunctionFactory INSTANCE = null;

    private Set<Class<? extends FunctionExecutor>> availableFunctions = new HashSet<>();

    private FunctionFactory() {
        availableFunctions = AnnotationUtils.findSubTypesWithAnnotation(FunctionExecutor.class, FunctionsType.class);
    }

    /**
     * Returns the singleton instance of the format factory.
     *
     * @return the instance.
     */
    public static FunctionFactory getInstance() {
        if (INSTANCE == null) {
            synchronized (lock) {
                INSTANCE = new FunctionFactory();
            }
        }

        return INSTANCE;
    }

    /**
     * Returns all group functions.
     *
     * @return all group functions.
     */
    public Set<String> getFunctionGroups() {
        Set<String> functionGroups = new HashSet<>();
        for (Class<? extends FunctionExecutor> functionExecutor : availableFunctions) {
            FunctionsType functionsType = functionExecutor.getAnnotation(FunctionsType.class);
            if (functionsType != null ) {
                for (FunctionType functionType : functionsType.functions()) {
                    functionGroups.add(functionType.group());
                }
            }
        }
        return functionGroups;
    }

    /**
     * Returns all functions within the given group.
     *
     * @param group the group to look for.
     * @return all functions within the given group.
     */
    public List<FunctionType> getFunctions(String group) {
        List<FunctionType> functionTypes = new ArrayList<>();
        for (Class<? extends FunctionExecutor> functionExecutor : availableFunctions) {
            FunctionsType functionsType = functionExecutor.getAnnotation(FunctionsType.class);
            if (functionsType != null ) {
                for (FunctionType functionType : functionsType.functions()) {
                    if (functionType.group().equals(group)) {
                        functionTypes.add(functionType);
                    }
                }
            }
        }
        return functionTypes;
    }

    /**
     * Returns the executor for the given format name.
     *
     * @param name the name of the format.
     *
     * @return the executor that can execute the given format.
     *
     * @throws FunctionException thrown when there was a problemen creating the executor.
     */
    public FunctionExecutor getFunctionExecutor(String name) throws FunctionException {
        log.info(String.format("Finding function executor for function '%s'", name));

        for (Class<? extends FunctionExecutor> functionExecutor : availableFunctions) {
            FunctionsType functionsType = functionExecutor.getAnnotation(FunctionsType.class);
            if (functionsType != null ) {
                for (FunctionType functionType : functionsType.functions()) {
                    if (functionType.name().equals(name)) {

                        try {
                            Constructor<? extends FunctionExecutor> constructor = functionExecutor.getConstructor();
                            return constructor.newInstance();
                        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                            throw new FunctionException(MSG_FAILED_TO_INSTANTIATE_FUNCTION, functionExecutor.getSimpleName());
                        }
                    }
                }
            }
        }

        log.severe(String.format("Functon executor for function '%s' not found", name));

        return null;
    }

}
