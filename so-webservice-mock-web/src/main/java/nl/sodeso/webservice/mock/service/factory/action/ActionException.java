package nl.sodeso.webservice.mock.service.factory.action;

import javax.servlet.ServletException;

/**
 * Exception that can be used by actions during their processing.
 *
 * @author Ronald Mathies
 */
public class ActionException extends ServletException {

    public ActionException(String message, Object ... args) {
        super(args != null && args.length > 0 ? String.format(message, args) : message);
    }
}