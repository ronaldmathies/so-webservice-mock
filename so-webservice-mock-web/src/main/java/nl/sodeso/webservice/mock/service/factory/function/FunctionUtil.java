package nl.sodeso.webservice.mock.service.factory.function;

import nl.sodeso.webservice.mock.service.context.Context;
import nl.sodeso.webservice.mock.service.factory.FunctionFactory;
import nl.sodeso.webservice.mock.service.factory.function.annotation.FunctionType;
import nl.sodeso.webservice.mock.service.factory.function.annotation.FunctionsType;
import nl.sodeso.webservice.mock.service.factory.function.annotation.ParameterType;
import nl.sodeso.webservice.mock.service.factory.function.convertor.ConvertorException;
import nl.sodeso.webservice.mock.service.variables.VariableManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
public class FunctionUtil {

    public static String evaluateAndExecuteFunctions(Context context, String value) throws FunctionException {
        Matcher matcher = Pattern.compile("_(.*?)\\b[^\\(]*\\((.*)\\)").matcher(value);
        if (matcher.find()) {
            String name = matcher.group(1);
            int replaceStart = matcher.start();
            int replaceEnd = matcher.end();

            FunctionExecutor function = FunctionFactory.getInstance().getFunctionExecutor(name);
            if (function == null)
                throw new FunctionException("No function found with name '%s'", name);

            function.setContext(context);

            List<Parameter> parameters = parameters(function, name, value.substring(replaceStart, replaceEnd));

            String evaluatedResult = "";
            try {
                Method cMethod = findMethod(function, name);
                if (cMethod == null)
                    throw new FunctionException("No method found with the name '%s' and matching arguments for function '%s'.", name, name);

                Object[] cParameters = constructMethodParameters(function, name, parameters);
                evaluatedResult = (String)cMethod.invoke(function, cParameters);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FunctionException("Error while accessing / invoking function '%s'.", name);
            }

            return value.substring(0, replaceStart) + evaluatedResult + value.substring(replaceEnd);
        }

        return VariableManager.getInstance().evaluatePotentialVariable(value);
    }

    public static FunctionType getFunctionType(FunctionExecutor executor, String name) {
        FunctionsType functionsType = executor.getClass().getAnnotation(FunctionsType.class);
        if (functionsType != null) {
            for (FunctionType functionType : functionsType.functions()) {
                if (functionType.name().equals(name)) {
                    return functionType;
                }
            }
        }

        return null;
    }

    private static Method findMethod(FunctionExecutor executor, String name) throws FunctionException {
        try {
            FunctionType functionType = getFunctionType(executor, name);
            ParameterType[] fParamTypes = functionType.parameters();

            Method foundMethod = null;
            Method[] cMethods = executor.getClass().getMethods();
            for (Method cMethod : cMethods) {

                if (!cMethod.getName().equals(functionType.name())) {
                    continue;
                }

                boolean matches = true;
                Class<?>[] cParamTypes = cMethod.getParameterTypes();
                for (int index = 0; index < cParamTypes.length; index++) {

                    Class<?> type = fParamTypes[index].type().newInstance().getType();
                    if (!cParamTypes[index].equals(type)) {
                        matches = false;
                        break;
                    }
                }

                if (matches) {
                    foundMethod = cMethod;
                    break;
                }
            }

            return foundMethod;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new FunctionException("Error while instantiating / accessing a convertor for '%s'.", executor.getClass().getSimpleName());
        }
    }

    private static Object[] constructMethodParameters(FunctionExecutor executor, String name, List<Parameter> parameters) throws FunctionException {
        try {
            FunctionType functionType = getFunctionType(executor, name);
            ParameterType[] fParamTypes = functionType.parameters();

            List<Object> p = new ArrayList<>();
            for (int fParamTypeIndex = 0; fParamTypeIndex < fParamTypes.length; fParamTypeIndex++) {
                p.add(fParamTypes[fParamTypeIndex].type().newInstance().from(parameters.get(fParamTypeIndex).toString()));
            }

            return p.toArray(new Object[p.size()]);
        } catch (InstantiationException | IllegalAccessException | ConvertorException e) {
            throw new FunctionException("Error while instantiating / accessing a convertor for '%s'.", executor.getClass().getSimpleName());
        }
    }

    public static List<Parameter> parameters(FunctionExecutor executor, String name, String function) throws FunctionException{
        FunctionType functionType = getFunctionType(executor, name);
        ParameterType[] parameterTypes = functionType.parameters();

        List<Parameter> parameters = new ArrayList<>();

        if (parameterTypes.length == 0)
            return parameters;

        StringBuilder value = new StringBuilder();

        int index = 0;
        int level = 0;
        boolean quote = false, wasQuoted = false;

        char[] chars = function.toCharArray();
        for (int cPos = 0; cPos < chars.length; cPos++) {
            char pChar = cPos > 0 ? chars[cPos-1] : 0;
            char cChar = chars[cPos];
            char nChar = cPos+1 < chars.length ? chars[cPos+1] : 0;

            if (cChar == '(') {
                level++;

                if (level == 1)
                    continue;

            } else if (cChar == ')') {
                level--;

                if (level == 0)
                    continue;
            } else if (cChar == '\\' && nChar == '\'') {
                continue;
            } else if (cChar == '\'' && pChar != '\\') {
                quote = !quote;
                wasQuoted = true;
                continue;
            }

            if (level == 0)
                continue;

            if (level == 1 && cChar == ',' && !quote) {
                parameters.add(new Parameter(parameterTypes[index++].name(), VariableManager.getInstance().evaluatePotentialVariable(FunctionUtil.evaluateAndExecuteFunctions(executor.getContext(), wasQuoted ? value.toString() : value.toString().trim()))));
                value = new StringBuilder();

            } else {
                value.append(cChar);
            }

        }

//        int index = 0;
//        int level = 0;
//        boolean quote = false, wasQuoted = false;
//        for (char c : format.toCharArray()) {
//            if (c == '(') {
//                level++;
//
//                if (level == 1)
//                    continue;
//
//            } else if (c == ')') {
//                level--;
//
//                if (level == 0)
//                    continue;
//            } else if (c == '\'') {
//                quote = !quote;
//                wasQuoted = true;
//                continue;
//            }
//
//            if (level == 0)
//                continue;
//
//            if (level == 1 && c == ',' && !quote) {
//                parameters.add(new Parameter(parameterTypes[index++].name(), VariableManager.getInstance().evaluatePotentialVariable(FunctionUtil.evaluateAndExecuteFunctions(executor.getContext(), wasQuoted ? value.toString() : value.toString().trim()))));
//                value = new StringBuilder();
//            } else {
//                value.append(c);
//            }
//
//        }

        parameters.add(new Parameter(parameterTypes[index].name(), VariableManager.getInstance().evaluatePotentialVariable(FunctionUtil.evaluateAndExecuteFunctions(executor.getContext(), value.toString().trim()))));

        if (parameters.size() != parameterTypes.length) {
            throw new FunctionException("Number of parameters in function (%d) does not match number of parameters expected (%d)", parameters.size(), parameterTypes.length);
        }

        return parameters;
    }
}
