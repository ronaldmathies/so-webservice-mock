package nl.sodeso.webservice.mock.service;

/**
 * @author Ronald Mathies
 */
public class MockServiceException extends Exception {

    public MockServiceException(String message, Object ... args) {
        super(String.format(message, args));
    }

}
