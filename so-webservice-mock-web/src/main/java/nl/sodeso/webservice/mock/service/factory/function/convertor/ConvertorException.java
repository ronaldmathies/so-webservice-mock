package nl.sodeso.webservice.mock.service.factory.function.convertor;

import javax.servlet.ServletException;

/**
 * @author Ronald Mathies
 */
public class ConvertorException extends ServletException {

    public ConvertorException(String message, Object ... args) {
        super(String.format(message, args));
    }
}