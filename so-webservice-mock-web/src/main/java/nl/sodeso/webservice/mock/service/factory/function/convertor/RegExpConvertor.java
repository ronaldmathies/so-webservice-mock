package nl.sodeso.webservice.mock.service.factory.function.convertor;

import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
public class RegExpConvertor implements Convertor<Pattern> {

    public Class<Pattern> getType() {
        return Pattern.class;
    }

    @Override
    public Pattern from(String value) throws ConvertorException {
        return Pattern.compile(value);
    }

}
