package nl.sodeso.webservice.mock.service.factory.function.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Documented
@Target({ TYPE })
@Retention(RUNTIME)
public @interface FunctionType {

    String name();
    String group() default "Other";
    String description();
    String documentation() default "";
    String format() default "";
    ParameterType[] parameters() default {};
    SampleType[] samples() default {};
}