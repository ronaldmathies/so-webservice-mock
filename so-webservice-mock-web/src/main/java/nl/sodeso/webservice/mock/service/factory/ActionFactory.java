package nl.sodeso.webservice.mock.service.factory;

import nl.sodeso.webservice.mock.model.endpoint.response.action.JsAction;
import nl.sodeso.webservice.mock.service.factory.action.ActionException;
import nl.sodeso.webservice.mock.service.factory.action.ActionExecutor;
import nl.sodeso.webservice.mock.service.factory.action.annotation.ActionExecutorType;
import nl.sodeso.commons.general.AnnotationUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Factory for resolving Actions by their name. All the actions are resolved and cached when this factory is used for the
 * first time.
 *
 * @author Ronald Mathies
 */
public class ActionFactory {

    private static final Logger log = Logger.getLogger(ActionFactory.class.getName());

    private static final String MSG_FAILED_TO_INSTANTIATE_ACTION = "Failed to instantiate action '%s'";

    private static final Object lock = new Object();
    private static ActionFactory INSTANCE = null;

    private Set<Class<? extends ActionExecutor>> availableActions = new HashSet<>();

    private ActionFactory() {
        availableActions = AnnotationUtils.findSubTypesWithAnnotation(ActionExecutor.class, ActionExecutorType.class);
    }

    /**
     * Returns the singleton instance of the action factory.
     *
     * @return the instance.
     */
    public static ActionFactory getInstance() {
        if (INSTANCE == null) {
            synchronized (lock) {
                INSTANCE = new ActionFactory();
            }
        }

        return INSTANCE;
    }

    /**
     * Returns the executor for the given jsAction.
     *
     * @param jsAction the jsAction.
     *
     * @return the executor that can execute the given jsAction.
     *
     * @throws ActionException thrown when there was a problemen creating the executor.
     */
    public ActionExecutor getAction(JsAction jsAction) throws ActionException {
        log.info(String.format("Finding jsAction executor for jsAction '%s'", jsAction.getClass().getSimpleName()));

        for (Class<? extends ActionExecutor> actionExecutor : availableActions) {
            ActionExecutorType actionExecutorType = actionExecutor.getAnnotation(ActionExecutorType.class);
            if (actionExecutorType != null && actionExecutorType.action().getSimpleName().equals(jsAction.getClass().getSimpleName())) {

                try {
                    Constructor<? extends ActionExecutor> constructor = actionExecutor.getConstructor();
                    return constructor.newInstance();
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new ActionException(MSG_FAILED_TO_INSTANTIATE_ACTION, actionExecutor.getSimpleName());
                }

            }
        }

        log.severe(String.format("JsAction executor for jsAction '%s' not found", jsAction.getClass().getSimpleName()));

        return null;
    }

}
