package nl.sodeso.webservice.mock.service.factory;

import nl.sodeso.webservice.mock.model.endpoint.response.condition.JsCondition;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionException;
import nl.sodeso.webservice.mock.service.factory.condition.ConditionExecutor;
import nl.sodeso.webservice.mock.service.factory.condition.annotation.ConditionExecutorType;
import nl.sodeso.commons.general.AnnotationUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ConditionFactory {

    private static final Logger log = Logger.getLogger(ConditionFactory.class.getName());

    private static final String MSG_FAILED_TO_INSTANTIATE_CONDITION = "Failed to instantiate condition '%s'";

    private static final Object lock = new Object();
    private static ConditionFactory INSTANCE = null;

    private Set<Class<? extends ConditionExecutor>> availableConditions = new HashSet<>();

    private ConditionFactory() {
        availableConditions = AnnotationUtils.findSubTypesWithAnnotation(ConditionExecutor.class, ConditionExecutorType.class);
    }

    /**
     * Returns the singleton instance of the condition factory.
     *
     * @return the instance.
     */
    public static ConditionFactory getInstance() {
        if (INSTANCE == null) {
            synchronized (lock) {
                INSTANCE = new ConditionFactory();
            }
        }

        return INSTANCE;
    }

    /**
     * Returns the executor for the given jsCondition.
     *
     * @param jsCondition the jsCondition.
     *
     * @return the executor that can execute the given jsCondition.
     *
     * @throws ConditionException thrown when there was a problemen creating the executor.
     */
    public ConditionExecutor getCondition(JsCondition jsCondition) throws ConditionException {
        log.info(String.format("Finding jsCondition executor for jsCondition '%s'", jsCondition.getClass().getSimpleName()));

        for (Class<? extends ConditionExecutor> conditionExecutor : availableConditions) {
            ConditionExecutorType conditionExecutorType = conditionExecutor.getAnnotation(ConditionExecutorType.class);
            if (conditionExecutorType != null && conditionExecutorType.condition().getSimpleName().equals(jsCondition.getClass().getSimpleName())) {

                try {
                    Constructor<? extends ConditionExecutor> constructor = conditionExecutor.getConstructor();
                    return constructor.newInstance();
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new ConditionException(MSG_FAILED_TO_INSTANTIATE_CONDITION, conditionExecutor.getSimpleName());
                }
            }
        }

        log.severe(String.format("JsCondition executor for jsCondition '%s' not found", jsCondition.getClass().getSimpleName()));

        return null;
    }

}
