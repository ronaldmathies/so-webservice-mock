package nl.sodeso.webservice.mock.service.broadcast;

import nl.sodeso.commons.network.discovery.broadcast.BroadcastServerConfiguration;
import nl.sodeso.commons.network.discovery.broadcast.AbstractBroadcastServerWebInitContext;
import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.webservice.mock.service.MockPropertiesContainer;

/**
 * @author Ronald Mathies
 */
public class BroadcastServerWebInitContext extends AbstractBroadcastServerWebInitContext {

    public BroadcastServerConfiguration getConfiguration() {
        return new BroadcastServerConfiguration() {

            @Override
            public boolean isEnabled() {
                return PropertyConfiguration.getInstance().getBooleanProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_BROADCAST_ENABLED, false);
            }

            @Override
            public int getLocalBindPort() {
                return PropertyConfiguration.getInstance().getIntegerProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_BROADCAST_PORT, 0);
            }

            @Override
            public int getExposedPort() {
                return PropertyConfiguration.getInstance().getIntegerProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_EXPOSED_PORT, 0);
            }

            @Override
            public String getServiceId() {
                return PropertyConfiguration.getInstance().getStringProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_SERVICE_ID, null);
            }

            @Override
            public int getBroadcastInterval() {
                return PropertyConfiguration.getInstance().getIntegerProperty(MockPropertiesContainer.DOMAIN, MockPropertiesContainer.DISCOVERY_BROADCAST_INTERVAL, 0);
            }
        };
    }
}
