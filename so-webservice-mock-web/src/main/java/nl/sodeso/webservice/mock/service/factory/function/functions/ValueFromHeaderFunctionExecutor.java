package nl.sodeso.webservice.mock.service.factory.function.functions;

import com.jayway.jsonpath.JsonPath;
import nl.sodeso.webservice.mock.model.generic.JsHeader;
import nl.sodeso.webservice.mock.service.factory.function.FunctionException;
import nl.sodeso.webservice.mock.service.factory.function.FunctionExecutor;
import nl.sodeso.webservice.mock.service.factory.function.annotation.*;
import nl.sodeso.webservice.mock.service.factory.function.convertor.RegExpConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.StringConvertor;
import nl.sodeso.webservice.mock.service.factory.function.convertor.XpathConvertor;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ronald Mathies
 */
@FunctionsType(
    functions = {
        @FunctionType(
            name = "valueFromHeader",
            description = "valueFromHeader( name )",
            group = "Header",
            format = "_valueFromHeader( 'name' )",
            documentation = "Resolves the value of the header with the specified name from the incoming request.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Resolve value of header",
                    parameters = {
                        @SampleParameterType(name = "name", value = "myheader"),
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromHeaderUsingJsonPath",
            description = "valueFromHeaderUsingJsonPath( name, jsonpath )",
            group = "Header",
            format = "_valueFromHeaderUsingJsonPath( 'name', 'jsonpath' )",
            documentation = "Resolves the value of the header with the specified name from the incoming request, the value is then parsed using the JSON path, this would require the value of the cookie to contain JSON data.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class),
                @ParameterType(name = "jsonpath", type = StringConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "name", value = "myheader"),
                        @SampleParameterType(name = "jsonpath", value = "$.request.reference")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromHeaderUsingRegExp",
            description = "valueFromHeaderUsingRegExp( name, regexp )",
            group = "Header",
            format = "_valueFromHeaderUsingRegExp( 'name', 'regexp' )",
            documentation = "Resolves the value of the header with the specified name from the incoming request, the value is then parsed using the regular expression.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class),
                @ParameterType(name = "regexp", type = RegExpConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "name", value = "myheader"),
                        @SampleParameterType(name = "regexp", value = "<reference>(.*?)</reference>")
                    }
                )
            }
        ),
        @FunctionType(
            name = "valueFromHeaderUsingXpath",
            description = "valueFromHeaderUsingXpath( name, xpath )",
            group = "Header",
            format = "_valueFromHeaderUsingXpath( 'name', 'xpath' )",
            documentation = "Resolves the value of the header with the specified name from the incoming request, the value is then parsed using the XPath expression, this would require the value of the header to contain XML data.",
            parameters = {
                @ParameterType(name = "name", type = StringConvertor.class),
                @ParameterType(name = "xpath", type = XpathConvertor.class)
            },
            samples = {
                @SampleType(
                    name = "Extract value using a simple expression",
                    parameters = {
                        @SampleParameterType(name = "name", value = "myheader"),
                        @SampleParameterType(name = "xpath", value = "//*[local-name()=\'element\']")
                    }
                )
            }
        )
    }
)
public class ValueFromHeaderFunctionExecutor extends FunctionExecutor {

    public String valueFromHeader(String name) throws FunctionException {
        Optional<JsHeader> jsHeaderOptional = getContext().getJsRequest().getJsHeader(name);
        return jsHeaderOptional.map(JsHeader::getValue).orElse("");

    }

    public String valueFromHeaderUsingJsonPath(String name, String jsonpath) throws FunctionException {
        Optional<JsHeader> jsHeaderOptional = getContext().getJsRequest().getJsHeader(name);
        return jsHeaderOptional.map(jsHeader -> (String)JsonPath.read(jsHeader.getValue(), jsonpath)).orElse("");

    }

    public String valueFromHeaderUsingRegExp(String name, Pattern pattern) throws FunctionException {
        Optional<JsHeader> jsHeaderOptional = getContext().getJsRequest().getJsHeader(name);
        if (jsHeaderOptional.isPresent()) {
            Matcher matcher = pattern.matcher(jsHeaderOptional.get().getValue());
            if (matcher.find()) {
                if (matcher.groupCount() > 0) {
                    return matcher.group(1);
                }
            }
        }

        return "";
    }

    public String valueFromHeaderUsingXpath(String name, XPathExpression xpath) throws FunctionException {
        try {
            Optional<JsHeader> jsHeaderOptional = getContext().getJsRequest().getJsHeader(name);
            if (jsHeaderOptional.isPresent()) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.parse(new InputSource(new StringReader(jsHeaderOptional.get().getValue())));
                document.getDocumentElement().normalize();

                return (String) xpath.evaluate(document, XPathConstants.STRING);
            }
        } catch (ParserConfigurationException | XPathExpressionException | SAXException | IOException e) {
            throw new FunctionException("Failed to parse the header with name '%s' due to an error '%s'.", name, e.getMessage());
        }

        return "";
    }

}
